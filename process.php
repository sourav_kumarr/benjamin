<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 6/28/2017
 * Time: 3:43 PM
 */
include("header1.php");
?>
    <link rel="stylesheet" href="css/pricing.css">
    <link rel="stylesheet" href="css/financing.css">
    <div class="container-fluid pricing-back">
        <div class="row no-gutter process-div" >
            <div class="col-md-12 process-txt">
                <ul class="list-unstyled">
                    <li><img src="images/process1.jpeg"></li>
                    <li>1: SCANNING > MEASURING > CUSTOMIZING</li>
                    <li ><span class="process-li">During your first appointment, you will peruse fabrics, discuss your preferences for fit and style and answer any questions you have.</span></li>
                    <li ><span class="process-li">Once you have selected fabric, we will scan, measure you and have you slip on some try on garments we keep in house for reference.</span></li>
                    <li><img src="images/process2.jpg"></li>
                    <li>2: FITTING > ALTERATIONS</li>
                    <li><span class="process-li">At your first fitting, we will slip on the unfinished garment, make all sizing adjustments and finish tailoring your order.</span></li>
                    <li><img src="images/process3.jpg"></li>
                    <li>3: ORDER COMPLETE</li>
                    <li><span class="process-li">Once we have perfected your fit, the order is complete and you can reorder with minimal to no future alterations.</span></li>

                </ul>
            </div>

            <div class="col-md-12 pricing-txt" style="margin-top: 4%">
                <a href="customization.php"><button type="button" class="custom-btn">GO CUSTOM</button></a>
            </div>
        </div>
    </div>
<?php
include ("footer1.php");
?>