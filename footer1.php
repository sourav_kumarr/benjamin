


    <div class="col-md-12 footer-back">
        <div class="header-txt main-footer col-md-2 col-lg-2 col-xs-12 col-sm-12"></div>
        <div class="header-txt main-footer col-md-2 col-lg-2 col-xs-12 col-sm-12">BOOK APPOINTMENT</div>
        <div class="header-txt main-footer col-md-2 col-lg-2 col-xs-12 col-sm-12">GIFT CARD</div>
        <div class="header-txt main-footer col-md-2 col-lg-2 col-xs-12 col-sm-12">CONTACT</div>
        <div class="header-txt main-footer col-md-2 col-lg-2 col-xs-12 col-sm-12">TERM OF SALE</div>
        <div class="header-txt main-footer col-md-2 col-lg-2 col-xs-12 col-sm-12">FAQ</div>
    </div>

    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 footer-panel">
        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12" style="padding-top: 10px;text-align: center">
            <div class="col-md-12">
                <h4 class="footer-title">BENJAMIN’S CUSTOM</h4>
            </div>
            <div class="col-md-12">
                <span style="color: grey">
                501 Fifth Avenue, Suite 712
                <br/>(on the corner of 42nd St)
                    New York, NY 10017</span>
            </div>

        </div>

        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12" style="padding-top: 10px;text-align: center ">
            <div class="col-md-12">
                <h4 class="footer-title">STAY CONNECTED</h4>
            </div>
            <div class="col-md-12">
              <input type="text" placeholder="Enter Email Address" class="email-box"/>
            </div>
            <div class="col-md-12" style="margin-top: 10px"><span class="footer-title" >Copyright 2017 © Benjamin’s Custom. All Rights Reserved </span></div>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12" style="padding-top: 10px;text-align: center ">
            <div class="col-md-12">
                <h6 class="footer-title">BY APPOINTMENT</h6>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" >
                <span style="color: grey;">Hours : 10 – 7</span>
            </div>
            <div class="col-md-12" >
                <span style="color: grey;">7 days a week</span>
            </div>
            <div class="col-md-12" >
                <h6 class="footer-title" style="text-decoration: underline;cursor: pointer">BOOK A FITTING</h6>
            </div>
        </div>
        </div>

<!--        <script src="js/jquery-3.1.1.min.js" type="application/javascript"></script>-->
<!--        <script src="js/bootstrap.min.js" type="application/javascript"></script>-->
        <script src="js/header1.js" type="application/javascript"></script>
</body>
</html>
