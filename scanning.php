<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 6/28/2017
 * Time: 3:43 PM
 */
 include("header1.php");
 ?>
 <link rel="stylesheet" href="css/pricing.css">
 <link rel="stylesheet" href="css/financing.css">
 <div class="container-fluid pricing-back back">
  <div class="row no-gutter back2" >
   <div class="col-md-12 ">
    <div class="col-md-4 scanning-div">

    </div>
       <div class="col-md-8 scanning-txt">
           <ul class=" list-unstyled">
               <li>IF THE SUIT DOESN’T FIT NOTHING ELSE MATTERS</li>
               <li>Every man’s posture and shoulder line is unique and any suit that doesn’t incorporate your unique shoulder and upper back angles will drape less elegantly than one that does.</li>
               <li>This sounds simple but even the most distinguished master tailors can get this wrong.</li>
               <li>Our Custom 3D Body Scanner ensures that we have the exact scientific measurements needed to achieve the best cut for the clients body.</li>
               <li style="font-weight: bold">3D SCAN SYSTEM SPECIFICATIONS</li>
               <li>Captures 2,844 measurements Performs 3 full scans in 1 second <0.03% error margin</li>
           </ul>
       </div>
   </div>
   <div class="col-md-12 financing-txt financing-txt-margin" style="margin-left: 10px">
       <span style="font-weight: bold;word-spacing: 2px;letter-spacing: 2px">3D PRECISION BODY SCANNING</span>
   </div>

      <div class="col-md-12 pricing-txt" style="margin-top: 2%">
          <a href="customization.php"><button type="button" class="custom-btn">GO CUSTOM</button></a>
      </div>
 </div>
 </div>
<?php
 include ("footer1.php");
?>