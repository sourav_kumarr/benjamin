<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 6/28/2017
 * Time: 3:43 PM
 */
 include("header1.php");
 ?>
 <link rel="stylesheet" href="css/pricing.css">
 <link rel="stylesheet" href="css/financing.css">
 <div class="container-fluid pricing-back back">
  <div class="row no-gutter back2" >
   <div class="col-md-12 ">
    <div class="col-md-4 tux-div">

    </div>
       <div class="col-md-8 scanning-txt">
           <ul class=" list-unstyled">
               <li>OUR FEATURED MIDNIGHT LORO PIANA TUXEDO | $995</li>
               <li>There are few moments more exciting for a man to showcase his style than a formal affair. As is the case with all black tie design, the details make all the difference.</li>
               <li>Each tuxedo is custom monogrammed </li>
               <li>Turnaround time is 3-5 weeks with a 2 week rush service available</li>
               <li >Starting at $895</li>
               <li><a href="mentrouser.php"><button type="button" class="custom-btn" style="padding: 3%;width: auto">CUSTOM MY TUXEDO</button></a></li>
           </ul>
       </div>
   </div>
   <div class="col-md-12 financing-txt financing-txt-margin" style="margin-left: 10px">
       <span style="font-weight: bold;word-spacing: 2px;letter-spacing: 2px">CUSTOM TUXEDOS</span>
   </div>


 </div>
 </div>
<?php
 include ("footer1.php");
?>