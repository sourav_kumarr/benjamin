<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 6/29/2017
 * Time: 6:54 PM
 */
 include("header1.php");
 ?>
    <link rel="stylesheet" href="css/pricing.css">
    <link rel="stylesheet" href="css/financing.css">
    <style>
        .back{
            background-image: url("images/slide2_blur.png") ;
            background-repeat: no-repeat;
            width: 100%;
            height: 1070px;
            background-size:cover ;
            padding: 5%;
        }
    </style>
    <div class="container-fluid pricing-back">
        <div class="row no-gutter custom-div" >
            <div class="col-md-12 process-txt">
                <ul class="list-unstyled">
                    <li><img src="images/custom_suit.jpg"></li>
                    <li style="text-align: center;margin-bottom: 10%">CUSTOM SUITING</li>
                    <li ><span class="process-li">We begin with top tier fabrics as they tailor distinctly better. The result is a more elegant suit with a notable difference in ease of movement, durability and a cleaner silhouette.</span></li>
                    <li ><span class="process-li">Our suits offer a sharp silhouette and elegant style at best in class pricing. We only use genuine and high-quality interlinings, linings, canvassing, buttons and stitching. </span></li>
                    <li ><span class="process-li">Each suitt comes with a complementary custom monogram. </span></li>
                    <li ><span class="process-li">Turnaround time is 3-5 weeks</span></li>
                    <li ><span class="process-li">2 week rush service available</span></li>
                    <li ><span class="process-li">Starting at $695</span></li>



                </ul>
            </div>

            <div class="col-md-12 pricing-txt" style="margin-top: 4%">
                <a href="mensuit.php" ><button type="button" class="custom-btn">GO CUSTOM</button></a>
            </div>
        </div>
    </div>
 <?php
 include("footer1.php");

?>