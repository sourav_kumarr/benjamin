<?php
namespace Modals;
require_once "CONNECT.php";
class ORDER
{
    private $link = null;
    private $link2 = null;
    public $CurrentDateTime = null;
    function __construct()
    {
        $this->link = new CONNECT();
        $this->link2 = new CONNECT();
        $this->CurrentDateTime = date("Y-m-d h:i:s");
    }
    public function addToCart_tr($product_type,$product_name,$product_price,$user_id,$quantity,$total_amount,$tr_type,
        $tr_fabric,$tr_fit,$tr_sidepocket,$tr_backpocket,$tr_seam, $tr_coinpocket,$tr_button_color,$tr_thread_color){
        if($this->link->Connect()){
            $query = "select * from benj_cart where product_type='$product_type' and product_price='$product_price'
            and user_id='$user_id' and tr_type='$tr_type' and tr_fabric='$tr_fabric' and tr_fit='$tr_fit' and
            tr_sidepocket='$tr_sidepocket' and tr_backpocket='$tr_backpocket' and tr_seam='$tr_seam' and 
            tr_coinpocket='$tr_coinpocket' and tr_button_color='$tr_button_color' and tr_thread_color='$tr_thread_color'";
            $result = mysqli_query($this->link->Connect(), $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $rows = mysqli_fetch_array($result);
                    $cart_id = $rows['cart_id'];
                    //update query here////
                    $query = "update benj_cart set quantity = quantity+1 where cart_id = '$cart_id'";
                    $result = mysqli_query($this->link->Connect(),$query);
                    if($result){
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Item Added to Cart";
                    }
                    else{
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                } else {
                    //insert query here/////
                    $query = "INSERT INTO benj_cart(product_type,product_name,product_price,user_id,quantity,
                    total_amount,tr_type,tr_fabric,tr_fit,tr_sidepocket,tr_backpocket,tr_seam, tr_coinpocket,
                    tr_button_color,tr_thread_color) VALUES ('$product_type','$product_name','$product_price',
                    '$user_id','$quantity','$total_amount','$tr_type','$tr_fabric','$tr_fit','$tr_sidepocket',
                    '$tr_backpocket','$tr_seam','$tr_coinpocket','$tr_button_color','$tr_thread_color')";
                    $result = mysqli_query($this->link->Connect(),$query);
                    if($result){
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Item Added to Cart";
                    }
                    else{
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function addToCart_sh($product_type,$product_name,$product_price,$user_id,$quantity,$total_amount,$sh_fit,
    $sh_fabric,$sh_sleeve,$sh_collar,$sh_cuff,$sh_placket, $sh_bottomcut,$sh_pocket,$sh_button_color,$sh_thread_color){
        if($this->link->Connect()){
            $query = "select * from benj_cart where product_type='$product_type' and product_price='$product_price'
            and user_id='$user_id' and sh_fit='$sh_fit' and sh_fabric='$sh_fabric' and sh_sleeve='$sh_sleeve' and
            sh_collar='$sh_collar' and sh_cuff='$sh_cuff' and sh_placket='$sh_placket' and sh_bottomcut='$sh_bottomcut'
            and sh_pocket='$sh_pocket' and sh_button_color='$sh_button_color' and sh_thread_color='$sh_thread_color'";
            $result = mysqli_query($this->link->Connect(), $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $rows = mysqli_fetch_array($result);
                    $cart_id = $rows['cart_id'];
                    //update query here////
                    $query = "update benj_cart set quantity = quantity+1 where cart_id = '$cart_id'";
                    $result = mysqli_query($this->link->Connect(),$query);
                    if($result){
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Item Added to Cart";
                    }
                    else{
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                }else{
                    //insert query here/////
                    $query = "INSERT INTO benj_cart(product_type,product_name,product_price,user_id,quantity,
                    total_amount,sh_fit,sh_fabric,sh_sleeve,sh_collar,sh_cuff,sh_placket,sh_bottomcut,sh_pocket,
                    sh_button_color,sh_thread_color) VALUES ('$product_type','$product_name','$product_price',
                    '$user_id','$quantity','$total_amount','$sh_fit','$sh_fabric','$sh_sleeve','$sh_collar',
                    '$sh_cuff','$sh_placket','$sh_bottomcut','$sh_pocket','$sh_button_color','$sh_thread_color')";
                    $result = mysqli_query($this->link->Connect(),$query);
                    if($result){
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Item Added to Cart";
                    }
                    else{
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function addToCart_su($product_type,$product_name,$product_price,$user_id,$quantity,$total_amount,$su_fabric,
    $su_fit,$su_button,$su_lapel,$su_sleeve_button,$su_lapel_stich, $su_pocket,$su_ticket_pocket,$su_vent,
    $su_elbow_patch,$su_elbow_fabric,$su_suit_linning,$su_button_color,$su_thread_color){
        if($this->link->Connect()){
            $query = "select * from benj_cart where product_type='$product_type' and product_price='$product_price'
            and user_id='$user_id' and su_fabric='$su_fabric' and su_fit='$su_fit' and su_button='$su_button' and
            su_lapel='$su_lapel' and su_sleeve_button='$su_sleeve_button' and su_lapel_stich='$su_lapel_stich' 
            and su_pocket='$su_pocket' and su_ticket_pocket='$su_ticket_pocket' and su_vent='$su_vent' and 
            su_elbow_patch='$su_elbow_patch' and su_elbow_fabric='$su_elbow_fabric' and su_suit_linning='$su_suit_linning'
             and su_button_color = '$su_button_color' and su_thread_color = '$su_thread_color'";
            $result = mysqli_query($this->link->Connect(), $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $rows = mysqli_fetch_array($result);
                    $cart_id = $rows['cart_id'];
                    //update query here////
                    $query = "update benj_cart set quantity = quantity+1 where cart_id = '$cart_id'";
                    $result = mysqli_query($this->link->Connect(),$query);
                    if($result){
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Item Added to Cart";
                    }
                    else{
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                }else{
                    //insert query here/////
                    $query = "INSERT INTO benj_cart(product_type,product_name,product_price,user_id,quantity,
                    total_amount,su_fabric,su_fit,su_button,su_lapel,su_sleeve_button,su_lapel_stich,su_pocket,
                    su_ticket_pocket,su_vent,su_elbow_patch,su_elbow_fabric,su_suit_linning,su_button_color,
                    su_thread_color) VALUES ('$product_type','$product_name','$product_price','$user_id','$quantity',
                    '$total_amount','$su_fabric','$su_fit','$su_button','$su_lapel','$su_sleeve_button','$su_lapel_stich',
                    '$su_pocket','$su_ticket_pocket','$su_vent','$su_elbow_patch','$su_elbow_fabric','$su_suit_linning',
                    '$su_button_color','$su_thread_color')";
                    $result = mysqli_query($this->link->Connect(),$query);
                    if($result){
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Item Added to Cart";
                    }
                    else{
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function getCartCount($user_id){
        if($this->link->Connect()) {
            $query = "select * from benj_cart where user_id='$user_id'";
            $result = mysqli_query($this->link->Connect(), $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "Cart Count Found";
                    $this->link->response['count'] = $num;
                }else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "No Data Found";
                }
            } else {
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function getCart($user_id){
        if($this->link->Connect()) {
            $query = "select * from benj_cart where user_id='$user_id'";
            $result = mysqli_query($this->link->Connect(), $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $cartData = array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $cartData[] = $rows;
                    }
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "Cart Data Found";
                    $this->link->response['cartData'] = $cartData;
                }else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "No Data Found";
                }
            } else {
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function createOrder($json)
    {
        if($this->link->Connect())
        {
            $json = json_decode($json);
            $userid = $json->userid;
            $total = $json->total;
            $items = $json->items;
            $order_number = mt_rand(000000,999999);
            $order_date = date("M d, Y H:i");
            $query = "insert into benj_orders(order_state,order_payment_status,order_number,order_total,order_user_id,
            order_date,paid_amount) values ('open','Pending','$order_number','$total','$userid','$order_date','0.00')";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $order_id = $this->link->lastId();
                $total_amount = 0;
                for($i=0;$i<sizeof($items);$i++)
                {
                    $product_type = $items[$i]->product_type;
                    if($product_type == "men_shirt"){
                        $price = $items[$i]->product_price;
                        $quantity = $items[$i]->product_quantity;
                        $fit = $items[$i]->sh_fit;
                        $fabric = $items[$i]->sh_fabric;
                        $sleeve = $items[$i]->sh_sleeve;
                        $collar = $items[$i]->sh_collar;
                        $cuff = $items[$i]->sh_cuff;
                        $placket = $items[$i]->sh_placket;
                        $bottom_cut = $items[$i]->sh_bottom_cut;
                        $pocket = $items[$i]->sh_pocket;
                        $button_color = $items[$i]->sh_button_color;
                        $thread_color = $items[$i]->sh_thread_color;
                        $total_amount = $total_amount + $price * $quantity;
                        $query="insert into benj_orders_detail(det_order_id,det_price,det_quantity,det_user_id,det_product_type,
                        sh_fit,sh_fabric,sh_sleeve,sh_collar,sh_cuff,sh_placket,sh_bottomcut,sh_pocket,sh_button_color,
                        sh_thread_color) values('$order_id','$price','$quantity','$userid','$product_type','$fit','$fabric',
                        '$sleeve','$collar','$cuff','$placket','$bottom_cut','$pocket','$button_color','$thread_color')";
                        $result = mysqli_query($this->link->Connect(),$query);
                        if ($result) {
                            $this->link->response[Status] = Success;
                            $this->link->response[Message] = "Order Placed Successfully";
                        } else {
                            $this->link->response[Status] = Error;
                            $this->link->response[Message] = $this->link->sqlError();
                        }
                    }
                    else if($product_type == "men_trouser"){
                        $price = $items[$i]->product_price;
                        $quantity = $items[$i]->product_quantity;
                        $tr_type = $items[$i]->tr_type;
                        $tr_fit = $items[$i]->tr_fit;
                        $tr_fabric = $items[$i]->tr_fabric;
                        $tr_sidepocket = $items[$i]->tr_sidepocket;
                        $tr_backpocket = $items[$i]->tr_backpocket;
                        $tr_seam = $items[$i]->tr_seam;
                        $tr_coinpocket = $items[$i]->tr_coinpocket;
                        $tr_button_color = $items[$i]->tr_button_color;
                        $tr_thread_color = $items[$i]->tr_thread_color;
                        $total_amount = $total_amount + $price * $quantity;
                        $query="insert into benj_orders_detail(det_order_id,det_price,det_quantity,det_user_id,det_product_type,
                        tr_type,tr_fabric,tr_fit,tr_sidepocket,tr_backpocket,tr_seam,tr_coinpocket,tr_button_color,
                        tr_thread_color) values('$order_id','$price','$quantity','$userid','$product_type','$tr_type','$tr_fabric',
                        '$tr_fit','$tr_sidepocket','$tr_backpocket','$tr_seam','$tr_coinpocket','$tr_button_color','$tr_thread_color')";
                        $result = mysqli_query($this->link->Connect(),$query);
                        if ($result) {
                            $this->link->response[Status] = Success;
                            $this->link->response[Message] = "Order Placed Successfully";
                        } else {
                            $this->link->response[Status] = Error;
                            $this->link->response[Message] = $this->link->sqlError();
                        }
                    }
                    else if($product_type == "men_suit"){
                        $price = $items[$i]->product_price;
                        $quantity = $items[$i]->product_quantity;
                        $su_fabric = $items[$i]->su_fabric;
                        $su_fit = $items[$i]->su_fit;
                        $su_button = $items[$i]->su_button;
                        $su_lapel = $items[$i]->su_lapel;
                        $su_sleeve_button = $items[$i]->su_sleeve_button;
                        $su_lapel_stich = $items[$i]->su_lapel_stich;
                        $su_pocket = $items[$i]->su_pocket;
                        $su_ticket_pocket = $items[$i]->su_ticket_pocket;
                        $su_vent = $items[$i]->su_vent;
                        $su_elbow_patch = $items[$i]->su_elbow_patch;
                        $su_elbow_fabric = $items[$i]->su_elbow_fabric;
                        $su_suit_linning = $items[$i]->su_suit_linning;
                        $su_button_color = $items[$i]->su_button_color;
                        $su_thread_color = $items[$i]->su_thread_color;
                        $total_amount = $total_amount + $price * $quantity;
                        $query="insert into benj_orders_detail(det_order_id,det_price,det_quantity,det_user_id,det_product_type,
                        su_fabric,su_fit,su_button,su_lapel,su_sleeve_button,su_lapel_stich,su_pocket,su_ticket_pocket,
                        su_vent,su_elbow_patch,su_elbow_fabric,su_suit_linning,su_button_color,su_thread_color) values
                        ('$order_id','$price','$quantity','$userid','$product_type','$su_fabric','$su_fit','$su_button',
                        '$su_lapel','$su_sleeve_button','$su_lapel_stich','$su_pocket','$su_ticket_pocket','$su_vent',
                        '$su_elbow_patch','$su_elbow_fabric','$su_suit_linning','$su_button_color','$su_thread_color')";
                        $result = mysqli_query($this->link->Connect(),$query);
                        if ($result) {
                            $this->link->response[Status] = Success;
                            $this->link->response[Message] = "Order Placed Successfully";
                            $this->link->response['order_id'] = $order_id;
                        } else {
                            $this->link->response[Status] = Error;
                            $this->link->response[Message] = $this->link->sqlError();
                        }
                    }
                }
                session_start();
                $total_amount = $total_amount+6;
                $_SESSION['total_amnt'] = $total_amount;
                $_SESSION['order_id'] = $order_id;
            }
            else {
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else {
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function updateCart($pid,$pprice,$prod_quan,$prod_total){
        if($this->link->Connect()) {
            $query = "update benj_cart set product_price='$pprice', quantity='$prod_quan', total_amount='$prod_total' 
            where cart_id='$pid'";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $this->link->response[Status] = Success;
                $this->link->response[Message] = "Cart Updated Successfully";
            }else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else {
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function removeItem($cart_id){
        if($this->link->Connect()){
            $query = "delete from benj_cart where cart_id='$cart_id'";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $this->link->response[Status] = Success;
                $this->link->response[Message] = "Cart Item Removed Successfully";
            }else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else {
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function clearCart($user_id){
        if($this->link->Connect()) {
            $query = "delete from benj_cart where user_id='$user_id'";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $this->link->response[Status] = Success;
                $this->link->response[Message] = "Cart Cleared Successfully";
            }else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else {
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function apiResponse($response){
        header("Content-Type:application/json");
        echo json_encode($response);
    }
}