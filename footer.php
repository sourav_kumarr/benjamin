    <div id="shopify-section-footer" class="shopify-section">
        <footer class="site-footer small--text-center has-content" role="contentinfo">
            <div class="footer-wrapper">
                <div class="grid small--text-left">
                    <div class="footer-blocks-container">
                        <div class="grid__item large--one-quarter footer-content-item ">
                            <p class="header">About Benjamin</p>
                            <p>Photographs provided by Bogart Menswear - Ryan T Crown / Simon Hutchinson. Bogart stocks
                                modern classics for men. Check them out at bogart.co.uk.</p>
                        </div>
                        <div class="grid__item large--one-tenth footer-content-item list-column ">
                            <p class="header">Quick Links</p>
                            <ul class="no-bullets">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="aboutus.php">About Us</a></li>
                                <li><a href="contactus.php">Contact Us </a></li>
                                <?php
                                if(!isset($_SESSION['benj_user_id'])) {
                                    ?>
                                    <li><a href="login/index.php">Order Now </a></li>
                                    <li><a href="login/index.php">Login/Register</a></li>
                                    <?php
                                }else{
                                    ?>
                                    <li><a href="customization.php">Order Now </a></li>
                                    <li><a href="logout.php">Logout</a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="grid__item large--one-tenth footer-content-item list-column ">
                            <p class="header">Connect</p>
                            <ul class="no-bullets">
                                <li>
                                    <a href="#">
                                        <span class="icon icon-facebook" aria-hidden="true">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon icon-twitter" aria-hidden="true">Twitter</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon icon-pinterest" aria-hidden="true">Pinterest</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon icon-instagram" aria-hidden="true">Instagram</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="grid__item large--one-quarter footer-content-item floatR">
                            <p class="header">Join our Newsletter</p>
                            <p>We only send emails once a month, and only about good things.</p>
                            <button class="btn btn--small show-exit-intent">Subscribe Now</button>
                        </div>
                    </div>
                    <hr class="full-width"/>
                    <div class="grid__item small--text-center muted">
                        <div class="large--left medium--left">
                            <a href="index.php" itemprop="url">
                                <h1 class='shop-name'>Benjamin</h1>
                            </a>
                        </div>
                        <div class='large--text-right medium--text-right small--text-center'>
                            <small>
                                <a target="_blank" rel="nofollow" href="#">Powered by Scan2Tailor</a> /
                                <a href="#" title="Benjamin">Designed by Benjamin</a>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <style>
            .site-footer {
                background-color: #1e2921;
            }

            .social h1,
            .social a
            .header,
            .shop-name {
                color: #ffffff;
            }

            .shop-name {
                fill: #ffffff;
            }

            .site-footer,
            .site-footer .muted small,
            .site-footer .muted a,
            .site-footer .list-column a,
            .payment-icons li,
            .social-icons li a,
            .site-header__logo {
                color: #ffffff;
            }

            .site-footer hr,
            .site-footer .grid__item {
                border-color: #27372c;
            }

            .footer-wrapper hr.full-width {
                width: 200%;
                margin-left: -50%;
                margin-top: 0;
            }
        </style>
    </div>
</body>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/libraries.min.js" type="text/javascript"></script>
<script src="js/moment.js" type="text/javascript"></script>
<script src="js/daterangepicker.js" type="text/javascript"></script>
<script src="js/ira.js" type="text/javascript"></script>
<script src="js/ajax-cart.js" type="text/javascript"></script>
<script>
function getCartCount(){
    var user_id = $("#user_id").val();
    if(user_id != ""){
        var url = "admin/api/orderProcess.php";
        $.post(url,{"type":"getCartCount","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                showCartCount(data.count);
            }
        }).fail(function(){
            alert("error occured on url");
        });
    }
}
function showCartCount(count){
    $("#cartCount").html(count);
}
function opencart(){
    window.location="cart.php";
}
getCartCount();
</script>