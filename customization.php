<?php
//session_start();
include ("header1.php");
/*if(!isset($_SESSION['benj_user_id'])){
    */?><!--
    <script>
        window.location = "login/index.php";
    </script>
    --><?php
/*}
echo "<input type='hidden' value='".$_SESSION['benj_user_id']."' id='user_id' />";*/
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="login/css/login.css" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="//fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i" rel="stylesheet" type="text/css" media="all"/>
<script src="js/libs/jquery.min.js"></script>
<style>
    .loadimage{
        display: none;
        height: 31px;
        left: 140px;
        position: absolute;
        top: 530px;
    }
    .cartbtn {
        position: fixed;
        right: 10px;
        top: 300px;
        width: 200px;
        z-index: 999999;
    }
    .site-footer {
        background-color: #1e2921;
    }

    .social h1,
    .social a
    .header,
    .shop-name {
        color: #ffffff;
    }

    .shop-name {
        fill: #ffffff;
    }

    .site-footer,
    .site-footer .muted small,
    .site-footer .muted a,
    .site-footer .list-column a,
    .payment-icons li,
    .social-icons li a,
    .site-header__logo {
        color: #ffffff;
    }
    .site-footer hr,
    .site-footer .grid__item {
        border-color: #27372c;
    }
    .footer-wrapper hr.full-width {
        width: 200%;
        margin-left: -50%;
        margin-top: 0;
    }
</style>
<body class="main" style="overflow-x:hidden">
<!--<div class="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand text-uppercase" href="index.php"><img src="images/logo/logo.png" style="width: 200px; float: left ! important; margin-left: -65px; margin-top: 5px;"></a>
            </div>

            <div class="collapse navbar-collapse" id="navigation">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php">Home</a></li>
                    <?php
/*                    if(isset($_SESSION['benj_user_id'])) {
                        */?>
                        <li class="has-dropdown">
                            <a href="customization.php" data-navigation-top-level>Order Now</a>
                        </li>
                        <?php
/*                    }else{
                        */?>
                        <li class="has-dropdown">
                            <a href="login/index.php" data-navigation-top-level>Order Now</a>
                        </li>
                        <?php
/*                    }
                    */?>
                    <li><a href="contactus.php">Contact us</a></li>
                    <li><a href="aboutus.php">About us</a></li>
                    <?php
/*                    if(isset($_SESSION['benj_user_id'])){
                        */?>
                        <li class="">
                            <a data-navigation-top-level>My Account</a>
                        </li>

                        <li class="">
                            <a href="logout.php" data-navigation-top-level>Logout</a>
                        </li>
                        <li onclick="opencart()"><span class="fa fa-shopping-cart fa-2x" style="cursor: pointer;color:white;margin-top:29px"></span>
                            <label class="badge" id="cartCount">0</label>
                        </li>
                        <?php
/*                    }else{
                        */?>
                        <li class="has-dropdown">
                            <a href="login/" data-navigation-top-level>Login/Register</a>
                        </li>
                        <?php
/*                    }
                    */?>
                </ul>
            </div>
        </div>
    </nav>
</div>
-->
<style>
    .badge {
        background: red none repeat scroll 0 0;
        margin-left: -15px;
        margin-top: -27px;
    }
    .mainContainer {
        margin-top: 90px;
        margin-bottom: 50px;
    }
    .heading{
        border-bottom: 2px solid #ccc;
        font-size: 43px;
        margin: 20px 0;
        padding-bottom: 23px;
        text-align: center;
    }
    .switches {
        border: 2px solid #ccc;
        border-radius: 10px;
        margin-bottom: 10px;
        padding: 10px;
        width: 66%;
        cursor:pointer;
    }
    .tophead{
        font-size:22px;
        border-bottom: 2px solid #ccc;
        padding-bottom: 14px;
    }
</style>
<div class="mainContainer col-md-12">
    <p class="heading">Customize Your Style With<br>Benjamin</p>
    <div class="col-md-8">
        <p class="heading" style="border-bottom: 0px;margin-top:120px;">Select An Option</p>
    </div>
    <div class="col-md-4">
        <a href="menshirt.php"><div class="switches" style="margin-top:10px"><p class="tophead">Shirts</p><p>Customize Shirts<p></div></a>
        <a href="mentrouser.php"><div class="switches"><p class="tophead">Trousers</p><p>Customize Trousers</p></div></a>
        <a href="mensuit.php"><div class="switches"><p class="tophead">Suits</p><p>Customize Suits</p></div></a>
    </div>
</div>
<div style="clear:both"></div>
<!--<div id="" class="shopify-section">
    <footer class="site-footer small--text-center has-content" role="contentinfo">
        <div class="footer-wrapper">
            <div class="grid small--text-left">
                <div class="footer-blocks-container">
                    <div class="grid__item large--one-quarter footer-content-item ">
                        <p class="header">About Benjamin</p>
                        <p>Photographs provided by Bogart Menswear - Ryan T Crown / Simon Hutchinson. Bogart stocks
                            modern classics for men. Check them out at bogart.co.uk.</p>
                    </div>
                    <div class="grid__item large--one-tenth footer-content-item list-column ">
                        <p class="header">Quick Links</p>
                        <ul class="no-bullets">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="aboutus.php">About Us</a></li>
                            <li><a href="contactus.php">Contact Us </a></li>
                            <?php
/*                            if(!isset($_SESSION['benj_user_id'])) {
                                */?>
                                <li><a href="login/index.php">Order Now </a></li>
                                <li><a href="login/index.php">Login/Register</a></li>
                                <?php
/*                            }else{
                                */?>
                                <li><a href="customization.php">Order Now </a></li>
                                <li><a href="logout.php">Logout</a></li>
                                <?php
/*                            }
                            */?>
                        </ul>
                    </div>
                    <div class="grid__item large--one-tenth footer-content-item list-column ">
                        <p class="header">Connect</p>
                        <ul class="no-bullets">
                            <li>
                                <a href="#">
                                    <span class="icon icon-facebook" aria-hidden="true">Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="icon icon-twitter" aria-hidden="true">Twitter</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="icon icon-pinterest" aria-hidden="true">Pinterest</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="icon icon-instagram" aria-hidden="true">Instagram</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="grid__item large--one-quarter footer-content-item floatR">
                        <p class="header">Join our Newsletter</p>
                        <p>We only send emails once a month, and only about good things.</p>
                        <button class="btn btn--small show-exit-intent">Subscribe Now</button>
                    </div>
                </div>
                <hr class="full-width"/>
                <div class="grid__item small--text-center muted">
                    <div class="large--left medium--left">
                        <a href="index.php" itemprop="url">
                            <h1 class='shop-name'>Benjamin</h1>
                        </a>
                    </div>
                    <div class='large--text-right medium--text-right small--text-center'>
                        <small>
                            <a target="_blank" rel="nofollow" href="#">Powered by Scan2Tailor</a> /
                            <a href="#" title="Benjamin">Designed by Benjamin</a>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>-->
<?php
 include ("footer1.php");
?>
</body>
<script>
    function getCartCount(){
        var user_id = $("#user_id").val();
        var url = "admin/api/orderProcess.php";
        $.post(url,{"type":"getCartCount","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                showCartCount(data.count);
            }else{
                // alert("cd"+data.Message);
            }
        }).fail(function(){
            alert("error occured on url");
        });
    }
    function showCartCount(count){
        $("#cartCount").html(count);
    }
    function opencart(){
        window.location="cart.php";
    }
    getCartCount();
</script>