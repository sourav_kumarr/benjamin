<?php
//session_start();
include("header1.php");

if(!isset($_SESSION['benj_user_id'])){
    ?>
    <script>
        window.location = "login/index.php";
    </script>
    <?php
}
echo "<input type='hidden' value='".$_SESSION['benj_user_id']."' id='user_id' />";
$paypal_url = 'https://www.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypal_id = 'kapillikes@gmail.com';
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link rel="stylesheet" href="login/css/login.css" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="//fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i" rel="stylesheet" type="text/css" media="all"/>
<script src="js/libs/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
<body class="main" style="overflow-x:hidden">
<!--<div class="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand text-uppercase" href="index.php"><img src="images/logo/logo.png" style="width: 200px; float: left ! important; margin-left: -65px; margin-top: 5px;"></a>
            </div>

            <div class="collapse navbar-collapse" id="navigation">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php">Home</a></li>
                    <?php
/*                    if(isset($_SESSION['benj_user_id'])) {
                        */?>
                        <li class="has-dropdown">
                            <a href="customization.php" data-navigation-top-level>Order Now</a>
                        </li>
                        <?php
/*                    }else{
                        */?>
                        <li class="has-dropdown">
                            <a href="login/index.php" data-navigation-top-level>Order Now</a>
                        </li>
                        <?php
/*                    }
                    */?>
                    <li><a href="contactus.php">Contact us</a></li>
                    <li><a href="aboutus.php">About us</a></li>
                    <?php
/*                    if(isset($_SESSION['benj_user_id'])){
                        */?>
                        <li class="">
                            <a data-navigation-top-level>My Account</a>
                        </li>
                        <li class="">
                            <a href="logout.php" data-navigation-top-level>Logout</a>
                        </li>
                        <li onclick="opencart()"><span class="fa fa-shopping-cart fa-2x" style="color:white;
                            margin-top:29px;cursor:pointer"></span>
                            <label class="badge" id="cartCount">0</label>
                        </li>
                        <?php
/*                    }else{
                        */?>
                        <li class="has-dropdown">
                            <a href="login/" data-navigation-top-level>Login/Register</a>
                        </li>
                        <?php
/*                    }
                    */?>
                </ul>
            </div>
        </div>
    </nav>
</div>-->
<style>
    .badge {
        background: red none repeat scroll 0 0;
        margin-left: -15px;
        margin-top: -27px;
    }
    .loader{
        background: rgba(0, 0, 0, 0.6) none repeat scroll 0 0;
        display:none;
        height: 100%;
        position: absolute;
        width: 100%;
        z-index: 5000;
    }
    .spinloader {
        height: 50px;
        margin: 25% 49%;
    }
    .cartcaption {
        font-size: 45px;
        margin: 0 0 30px 0;
        text-align: center;
    }
    .optionbox {
        color: #555;
        float: left;
        font-size: 19px;
        text-align: center;
        width: 31%;
    }

    .optionnumbox {
        background: white none repeat scroll 0 0;
        border: 3px solid #777;
        line-height: 40px;
        padding: 2px 8px;
    }
    .optionnumboxactive
    {
        background:#002856;
        color:white;
        border:3px solid #002856;
    }
    #cart {
        margin-top: 40px;
    }
    .completecart {
        margin-bottom: 25px;
    }
    .cartdelicon {
        font-size: 19px;
        margin-top: 159px;
        position: absolute;
    }
    .productinfo
    {
        border:1px solid #ccc;
        padding: 10px 0;
    }
    .quantity {
        border: 1px solid #ccc;
        color: #555;
        font-size: 18px;
        font-weight: normal;
        height: 202px;
        line-height: 200px;
        text-align: center;
    }
    .productpricing {
        border: 1px solid #ccc;
        color: red;
        font-size: 18px;
        font-weight: normal;
        height: 202px;
        line-height: 200px;
        text-align: right;
    }
    .finalpaymentcart {
        border: 1px solid #ccc;
        height: 90px;
        margin-left: 1%;
        width: 98%;
    }
    .finalpaymentcartlabel1
    {
        font-size: 19px;
        margin-top: 10px;
    }
    .finalpaymentcartlabel2
    {
        font-size: 19px;
        margin-top: 10px;
    }
    .totalpaymentcart {
        border: 1px solid #ccc;
        height: 60px;
        margin-left: 1%;
        width: 98%;
    }
    .totalpaymentcartlabel1
    {
        float: right;
        font-size: 19px;
        margin-top: 15px;
    }
    .custombottomcartbtn {
        background: #002856 none repeat scroll 0 0;
        border: medium none;
        color: white;
        font-size: 22px;
        margin-top: 50px;
        padding: 12px 62px;
    }
    th, td{
        border:0;
    }
    .increment_quantity {
        height: 25px;
        margin-top: 120px;
        position: absolute;
        width: 24px;
    }
    .decrement_quantity {
        height: 25px;
        margin-left: -26px;
        margin-top: 120px;
        position: absolute;
        width: 24px;
    }
    .trash {
        cursor: pointer;
        height: 24px;
    }
    .site-footer {
        background-color: #1e2921;
    }

    .social h1,
    .social a
    .header,
    .shop-name {
        color: #ffffff;
    }

    .shop-name {
        fill: #ffffff;
    }
    .site-footer,
    .site-footer .muted small,
    .site-footer .muted a,
    .site-footer .list-column a,
    .payment-icons li,
    .social-icons li a,
    .site-header__logo {
        color: #ffffff;
    }
    .site-footer hr,
    .site-footer .grid__item {
        border-color: #27372c;
    }
    .footer-wrapper hr.full-width {
        width: 200%;
        margin-left: -50%;
        margin-top: 0;
    }
</style>
<div class="loader">
    <img src="images/default.gif" class="spinloader" />
</div>
<div class="col-md-12" style="margin-top:100px">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <p class="cartcaption">CHECKOUT CART</p>
        <div class="col-md-12" style="margin-bottom: 20px">
            <div class="col-md-4 optionbox">CART<br><span class="optionnumbox optionnumboxactive" style="cursor:pointer"  onclick="backon_1()" id="optioncart">1</span></div>
            <div class="col-md-4 optionbox">SHIPPING & BILLING<br><span class="optionnumbox" style="cursor:pointer"  onclick="backon_2()" id="shippingcart" >2</span></div>
            <div class="col-md-4 optionbox">PAYMENT CONFIRMATION<br><span class="optionnumbox"  id="paymentcart">3</span></div>
        </div>
        <div class="clear"></div>
        <div id="cart"></div>
        <div class='col-md-12 finalpaymentcart' id="finalpaymentcart">
            <div class='col-md-9' style="padding:0;text-align:right">
                <p class="finalpaymentcartlabel1" >Cart subtotal:</p>
                <p class="finalpaymentcartlabel2">Tax :</p>
            </div>
            <div class='col-md-1'></div>
            <div class='col-md-2'>
                <!--<label class="finalpaymentcartlabel1" id="finalcartsubtotal" ></label><br><br>-->
                <input class="finalpaymentcartlabel1 " type="text" id="finalcartsubtotal" style="border:none;
                height:36px" readonly />
                <p class="finalpaymentcartlabel2" style="margin:0 0 0 10px">$6.00</p>
            </div>
        </div>
        <div class='col-md-12 totalpaymentcart' id="totalpaymentcart">
            <div class='col-md-9' style="padding:0">
                <label class="totalpaymentcartlabel1" >ORDER TOTAL:</label>
            </div>
            <div class='col-md-1'></div>
            <div class='col-md-2'>
                <input class=" totalpaymentcartlabel1 " type="text" id="totalcartsubtotal" style="border:none;
                height:35px;margin-top:6px" readonly /><br><br>
            </div>
        </div>
        <div class='col-md-12' id='stp1_buttons'>
            <input type="button" class="custombottomcartbtn" onclick="continue_shoping()"  style="float:left;background:white;color:black;border:1px solid #ccc" value="CONTINUE SHOPPING"/>
            <input type="button" class="custombottomcartbtn" onclick="step_2_task()" id="placeorder" style="float:right" value="Shipping & Billing"/>
        </div>
        <div id="shipping"></div>
        <div id="payment"></div>
        <!------------------------------step 2functionality start here------------------------------------------------>
        <div class="col-md-12" id="step2_div" style="display:none">
            <div class="row" id="other_div" style="border:2px #eee dotted;display:block;margin-top:10px;margin-bottom:20px">
                <div class="col-md-12" style="padding:20px">
                    <h4 class="addressdataheading">Ship to this address</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="otherradio_label">First name(*)</label>
                            <input type="text" class="form-control" id="first_name" placeholder="first name">
                        </div>
                        <div class="col-md-6">
                            <label class="otherradio_label">Last name(*)</label>
                            <input type="text" class="form-control" id="last_name" placeholder="last name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="otherradio_label">Address line 1:(*)</label>
                            <input type="text" class="form-control" id="adressline1" placeholder="address">
                        </div>
                        <div class="col-md-6">
                            <label class="otherradio_label">Address line 2</label>
                            <input type="text" class="form-control" id="adressline2" placeholder="sub address">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="otherradio_label">Country(*)</label>
                            <select class="form-control countries" name="country" id="countryId" ></select>
                        </div>
                        <div class="col-md-4 ">
                            <label class="otherradio_label">State(*)</label>
                            <select class="form-control states" name="state" id="stateId" ></select>
                        </div>
                        <div class="col-md-4">
                            <label class="otherradio_label">City(*)</label>
                            <div id="cityfeild">
                                <select class="form-control cities" name="city" id="cityId" ></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="otherradio_label">Postal code(*)</label>
                            <input type="text" class="form-control" id="postalcode" placeholder="postal code">
                        </div>
                        <div class="col-md-4">
                            <label class="otherradio_label">Phone number(*)</label>
                            <input type="text" class="form-control" id="phonenumber" placeholder="contact number">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" >
                <input id="make_payment" type="button" value="Make Payment" onclick="payment_confirmation()" class="custommpbtn pull-right">
            </div>
        </div>
        <!------------------------------step 2 end here--------------------------------------------------------------->
        <!------------------------------payment confirmation div start here--------------------------------------------------------------->
        <div class="row" id="payment_confirmation_div" style="display:none">
            <div class="col-md-12 ">
                <label class="pull-right ;text-align:right" style="padding:10px;border-bottom:1px solid #eee">Benjamin Custom Suit: <span id="paymentconfirm"></span></label><br><br><br>
                <label class="pull-right text-align:right">Tax: <span id="tax">6.00</span></label><br><br>
                <label class="pull-right text-align:right" style="color:green">Bill total: <span id="totalpaymentconfirm" style="color:green"></span></label>
            </div>
            <div class="col-md-12">
                <label class=""><input type="radio"  name="step2_radio" id="payvia" value="payvia" />&nbsp;Pay via PayPal</label>
            </div>
            <div class="col-md-12">
                <div class="row" style="padding:10px">
                    <div class="col-md-4"></div>
                    <div class="col-md-4" style="text-align:center">
                        <form action="<?php echo $paypal_url; ?>" onsubmit="return solvecart() " method="post">
                            <input type="hidden" name="business" value="<?php echo $paypal_id; ?>">
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="item_name" value="Orders">
                            <input type="hidden" name="item_number" value="<?php echo $_SESSION['order_id']?>">
                            <input type="hidden" id="paypalamnt" name="amount" value="">
                            <input type="hidden" name="currency_code" value="USD">
                            <input type='hidden' name='return' value='payment.php?type=payment'>
                            <input type="image" name="submit" border="0" src="" alt="Pay Now" style="background: #002856;border-radius: 7px;color: white;margin-left: 5%;
                            margin-top: 21%;padding: 10px 40px">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!------------------------------payment confirmation div end here------------------------------------------------------------------>
    </div>
    <div class="col-md-1"></div>
</div>
<div style="clear:both"></div><br>
<?php
include("footer1.php");
?>
<!--<div id="" class="shopify-section">
    <footer class="site-footer small--text-center has-content" role="contentinfo">
        <div class="footer-wrapper">
            <div class="grid small--text-left">
                <div class="footer-blocks-container">
                    <div class="grid__item large--one-quarter footer-content-item ">
                        <p class="header">About Benjamin</p>
                        <p>Photographs provided by Bogart Menswear - Ryan T Crown / Simon Hutchinson. Bogart stocks
                            modern classics for men. Check them out at bogart.co.uk.</p>
                    </div>
                    <div class="grid__item large--one-tenth footer-content-item list-column ">
                        <p class="header">Quick Links</p>
                        <ul class="no-bullets">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="aboutus.php">About Us</a></li>
                            <li><a href="contactus.php">Contact Us </a></li>
                            <?php
/*                            if(!isset($_SESSION['benj_user_id'])) {
                                */?>
                                <li><a href="login/index.php">Order Now </a></li>
                                <li><a href="login/index.php">Login/Register</a></li>
                                <?php
/*                            }else{
                                */?>
                                <li><a href="customization.php">Order Now </a></li>
                                <li><a href="logout.php">Logout</a></li>
                                <?php
/*                            }
                            */?>
                        </ul>
                    </div>
                    <div class="grid__item large--one-tenth footer-content-item list-column ">
                        <p class="header">Connect</p>
                        <ul class="no-bullets">
                            <li>
                                <a href="#">
                                    <span class="icon icon-facebook" aria-hidden="true">Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="icon icon-twitter" aria-hidden="true">Twitter</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="icon icon-pinterest" aria-hidden="true">Pinterest</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="icon icon-instagram" aria-hidden="true">Instagram</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="grid__item large--one-quarter footer-content-item floatR">
                        <p class="header">Join our Newsletter</p>
                        <p>We only send emails once a month, and only about good things.</p>
                        <button class="btn btn--small show-exit-intent">Subscribe Now</button>
                    </div>
                </div>
                <hr class="full-width"/>
                <div class="grid__item small--text-center muted">
                    <div class="large--left medium--left">
                        <a href="index.php" itemprop="url">
                            <h1 class='shop-name'>Benjamin</h1>
                        </a>
                    </div>
                    <div class='large--text-right medium--text-right small--text-center'>
                        <small>
                            <a target="_blank" rel="nofollow" href="#">Powered by Scan2Tailor</a> /
                            <a href="#" title="Benjamin">Designed by Benjamin</a>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>-->
<div id="modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
function backon_1(){
    var path = window.location.href;
    if(path.indexOf("?") >= 1){
        path = path.split("?");
        var temp = path[1].split("=");
        if(temp[0] == "stp" && temp[1] == "2" || temp[0] == "stp" && temp[1] == "3"){
            window.location="cart.php?stp=1";
        }
    }
}
function backon_2(){
    var path = window.location.href;
    if(path.indexOf("?") >= 2){
        path = path.split("?");
        var temp = path[1].split("=");
        if(temp[0] == "stp" && temp[1] == "3"){
            window.location="cart.php?stp=2";
        }
    }
}
function continue_shoping(){
    window.location="customization.php";
}
function incrementValue(pprice,rowid,pid,limit){
    var pprice=parseFloat(pprice);
    var value = parseFloat(document.getElementById("quantity"+rowid).value, 10);
    value = isNaN(value) ? 0 : value;
    if(value<limit){
        value++;
        var prod_quan=document.getElementById("quantity"+rowid).value = value;
        document.getElementById("total"+rowid).value = (prod_quan*pprice).toFixed(1);
        var amount=document.getElementById("finalcartsubtotal").value;
        var values = amount.split('$');
        var amount = values[1];
        var subtotal=(parseFloat(amount)+parseFloat(pprice)).toFixed(1);
        document.getElementById("finalcartsubtotal").value="$"+subtotal;
        var prod_total_amount=parseFloat(subtotal)+parseFloat(6);
        $("#totalcartsubtotal").val("$"+prod_total_amount);
        increment_cart_value(prod_quan,pid,pprice);
    }
    if(value==limit){
        var prod_quan=document.getElementById("quantity"+rowid).value = value;
        value++;
        document.getElementById("total"+rowid).value = (prod_quan*pprice).toFixed(1);
        var amount=document.getElementById("finalcartsubtotal").value;
        var values = amount.split('$');
        var amount = values[1];
        increment_cart_value(prod_quan,pid,pprice);
    }
    if(value>limit){
        $(".modal-title").html("<label style='color:green'>Quantity Limit Reach !!!</label>");
        $(".modal-body").html("<h5>We have only "+limit+" Items Available in Stock.</h5>");
        $("#modal").modal("show");
    }
}
function increment_cart_value(prod_quan,pid,pprice){
    var prod_total=(pprice*prod_quan).toFixed(1);
    var url="admin/api/orderProcess.php";
    $.post(url,{"type":"updateCart","pid":pid,"pprice":pprice,"prod_quan":prod_quan,"prod_total":prod_total},function(data){
        var status = data.Status;
        if(status != "Success"){
            $(".modal-title").html("<label style='color:red'>Error Occured</label>");
            $(".modal-body").html(data.Message);
            $("#modal").modal("show");
        }
    }).fail(function(){
        alert("page error");
    });
}
function decrementValue(pprice,rowid,pid){
    var pprice=parseFloat(pprice);
    var value = parseFloat(document.getElementById("quantity"+rowid).value, 10);
    value = isNaN(value) ? 0 : value;
    if(value>1){
        value--;
        var prod_quancheck=document.getElementById("limit_reach"+rowid).value = value;
        var prod_quan=document.getElementById("quantity"+rowid).value = value;
        document.getElementById("total"+rowid).value = (prod_quan*pprice).toFixed(1);
        var amount=document.getElementById("finalcartsubtotal").value;
        var values = amount.split('$');
        var amount = values[1];
        var subtotal=(parseFloat(amount)-parseFloat(pprice)).toFixed(1);
        document.getElementById("finalcartsubtotal").value="$"+subtotal;
        var prod_total_amount=parseFloat(subtotal)+parseFloat(6);
        $("#totalcartsubtotal").val("$"+prod_total_amount);
        increment_cart_value(prod_quan,pid,pprice);
    }
}
var path = window.location.href;
if(path.indexOf("?") >= 0){
    path = path.split("?");
    var temp = path[1].split("=");
    if(temp[0] == "stp" && temp[1] == "2"){
        $("#paymentcart").removeClass("optionnumboxactive");
        $("#optioncart").removeClass("optionnumboxactive");
        $("#shippingcart").addClass("optionnumboxactive");
        document.getElementById("cart").style.display="none";
        document.getElementById("payment").style.display="none";
        document.getElementById("shipping").style.display="none";
        document.getElementById("finalpaymentcart").style.display="none";
        document.getElementById("totalpaymentcart").style.display="none";
        document.getElementById("stp1_buttons").style.display="none";
        document.getElementById("step2_div").style.display="block";
        getUserData();
    }
    else if(temp[0] == "stp" && temp[1] == "3"){
        $("#optioncart").removeClass("optionnumboxactive");
        $("#shippingcart").removeClass("optionnumboxactive");
        $("#paymentcart").addClass("optionnumboxactive");
        $("#payvia").attr('checked', 'checked');
        document.getElementById("cart").style.display="none";
        document.getElementById("payment").style.display="none";
        document.getElementById("shipping").style.display="none";
        document.getElementById("finalpaymentcart").style.display="none";
        document.getElementById("totalpaymentcart").style.display="none";
        document.getElementById("stp1_buttons").style.display="none";
        document.getElementById("payment_confirmation_div").style.display="block";
    }
    else{
        $("#paymentcart").removeClass("optionnumboxactive");
        $("#shippingcart").removeClass("optionnumboxactive");
        $("#optioncart").addClass("optionnumboxactive");
    }
}
else{
    $("#paymentcart").removeClass("optionnumboxactive");
    $("#shippingcart").removeClass("optionnumboxactive");
    $("#optioncart").addClass("optionnumboxactive");
}
$(".loader").fadeIn();
var user_id = $("#user_id").val();
var stock=0;
var url = "admin/api/orderProcess.php";
$.post(url,{"type":"getCart","user_id":user_id},function(data){
    var status = data.Status;
    var button="<input type='button' class='custommpbtn pull-right' style='margin:0 15px 5px 0;'" +
    " value='Clear Cart' onclick=clear_cart('"+user_id+"') id='clear_cart' >";
    var cartshow = "";
    if(status == "Success")
    {
        var items = data.cartData;
        var totalamount = 0;
        for(var i=0;i<items.length;i++)
        {
            if(items[i].product_type == "men_shirt"){
                stock = 10;
                totalamount = parseFloat(totalamount) + parseFloat(items[i].total_amount);
                var tm = (items[i].total_amount);
                tm = Math.round(tm * 1000) / 1000;
                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-9 productinfo'>" +
                "<div class='col-md-11'><table style='letter-spacing:1px;line-height:36px;'><tr><th class=''>" +
                "Product Type </th><td>&nbsp;&nbsp;: "+items[i].product_type + "</td></tr><tr><th>Shirt Fit</th>" +
                "<td>&nbsp;&nbsp;: " + items[i].sh_fit+"</td></tr><tr><th>Shirt Sleeve </th><td>&nbsp;&nbsp;: " +
                items[i].sh_sleeve+"</td></tr><tr><th>Shirt Collar</th><td>&nbsp;&nbsp;: "+items[i].sh_collar+
                "</td></tr><tr><th>Shirt Pocket</th><td>&nbsp;&nbsp;: " + items[i].sh_pocket+"</td></tr></table>" +
                "</div><div class='col-md-1' style='text-align:right;padding-right:26px'><span style='cursor:pointer'"+
                "onclick=deletecart('"+items[i].cart_id+"') class='cartdelicon'><img src='images/delete-xxl.png' class='trash' /></span>" +
                "</div></div><div class='col-md-1 quantity'><img src='images/plus.png' class='increment_quantity'"+
                "onclick=incrementValue('"+items[i].product_price + "','"+i+"','" + items[i].cart_id+"','"+stock+"')>" +
                "<img src='images/minus.png' class='decrement_quantity' onclick=decrementValue('"+
                items[i].product_price+"','"+i+ "','"+items[i].cart_id +"')><input type='text' id='limit_reach"+i+
                "' value='"+items[i].quantity+"' hidden  /><input type='text' class='form-control text-center' " +
                "readonly style='margin-top:83px;height:35px;background:#fff' id='quantity"+i+"' value='" +
                items[i].quantity + "'/></div><div class='col-md-2 productpricing'><input class='form-control' " +
                "type='text' style='margin-top:83px;background:#fff;height:35px;border:none' readonly value='" + tm + "' id='total" + i + "' /><input type='text' id='stock_value' " +
                "value='" + stock + "' hidden  /><input type='text' hidden id='stock_value' value='" + stock + "'  />" +
                "</div></div>";
            }
            else if(items[i].product_type == "men_trouser"){
                stock = 10;
                totalamount = parseFloat(totalamount) + parseFloat(items[i].total_amount);
                var tm = (items[i].total_amount);
                tm = Math.round(tm * 1000) / 1000;
                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-9 productinfo'>" +
                "<div class='col-md-11'><table style='letter-spacing:1px;line-height:36px;'><tr><th class=''>" +
                "Product Type </th><td>&nbsp;&nbsp;: "+items[i].product_type + "</td></tr><tr><th>Trouser Fit</th>" +
                "<td>&nbsp;&nbsp;: " + items[i].tr_fit+"</td></tr><tr><th>Trouser Coin Pocket </th><td>&nbsp;&nbsp;: " +
                items[i].tr_coinpocket+"</td></tr><tr><th>Trouser Side Pocket</th><td>&nbsp;&nbsp;: "+items[i].tr_sidepocket+
                "</td></tr><tr><th>Trouser Back Pocket</th><td>&nbsp;&nbsp;: " + items[i].tr_backpocket+"</td></tr></table>" +
                "</div><div class='col-md-1' style='text-align:right;padding-right:26px'><span style='cursor:pointer'"+
                "onclick=deletecart('"+items[i].cart_id+"') class='cartdelicon'><img src='images/delete-xxl.png' class='trash' /></span>" +
                "</div></div><div class='col-md-1 quantity'><img src='images/plus.png' class='increment_quantity'"+
                "onclick=incrementValue('"+items[i].product_price + "','"+i+"','" + items[i].cart_id+"','"+stock+"')>" +
                "<img src='images/minus.png' class='decrement_quantity' onclick=decrementValue('"+
                items[i].product_price+"','"+i+ "','"+items[i].cart_id +"')><input type='text' id='limit_reach"+i+
                "' value='"+items[i].quantity+"' hidden  /><input type='text' class='form-control text-center' " +
                "readonly style='margin-top:83px;height:35px;background:#fff' id='quantity"+i+"' value='" +
                items[i].quantity + "'/></div><div class='col-md-2 productpricing'><input class='form-control' " +
                "type='text' style='margin-top:83px;background:#fff;height:35px;border:none' readonly value='" + tm + "' id='total" + i + "' /><input type='text' id='stock_value' " +
                "value='" + stock + "' hidden  /><input type='text' hidden id='stock_value' value='" + stock + "'  />" +
                "</div></div>";
            }
            else if(items[i].product_type == "men_suit"){
                stock = 10;
                totalamount = parseFloat(totalamount) + parseFloat(items[i].total_amount);
                var tm = (items[i].total_amount);
                tm = Math.round(tm * 1000) / 1000;
                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-9 productinfo'>" +
                "<div class='col-md-11'><table style='letter-spacing:1px;line-height:36px;'><tr><th class=''>" +
                "Product Type </th><td>&nbsp;&nbsp;: "+items[i].product_type + "</td></tr><tr><th>Suit Fit</th>" +
                "<td>&nbsp;&nbsp;: " + items[i].su_fit+"</td></tr><tr><th>Suit Buttons </th><td>&nbsp;&nbsp;: " +
                items[i].su_button+"</td></tr><tr><th>Suit Lapel</th><td>&nbsp;&nbsp;: "+items[i].su_lapel+
                "</td></tr><tr><th>Suit Pocket</th><td>&nbsp;&nbsp;: " + items[i].su_pocket+"</td></tr></table>" +
                "</div><div class='col-md-1' style='text-align:right;padding-right:26px'><span style='cursor:pointer'"+
                "onclick=deletecart('"+items[i].cart_id+"') class='cartdelicon'><img src='images/delete-xxl.png' class='trash' /></span>" +
                "</div></div><div class='col-md-1 quantity'><img src='images/plus.png' class='increment_quantity'"+
                "onclick=incrementValue('"+items[i].product_price + "','"+i+"','" + items[i].cart_id+"','"+stock+"')>" +
                "<img src='images/minus.png' class='decrement_quantity' onclick=decrementValue('"+
                items[i].product_price+"','"+i+ "','"+items[i].cart_id +"')><input type='text' id='limit_reach"+i+
                "' value='"+items[i].quantity+"' hidden  /><input type='text' class='form-control text-center' " +
                "readonly style='margin-top:83px;height:35px;background:#fff' id='quantity"+i+"' value='" +
                items[i].quantity + "'/></div><div class='col-md-2 productpricing'><input class='form-control' " +
                "type='text' style='margin-top:83px;background:#fff;height:35px;border:none' readonly value='" + tm + "' id='total" + i + "' /><input type='text' id='stock_value' " +
                "value='" + stock + "' hidden  /><input type='text' hidden id='stock_value' value='" + stock + "'  />" +
                "</div></div>";
            }
        }
        $("#cart").html(button+cartshow);
        var totalamount =totalamount.toFixed(1);
        $("#finalcartsubtotal").val("$"+totalamount);
        $("#totalcartsubtotal").val("$"+(parseFloat(totalamount)+6));
        $("#paymentconfirm").html("$"+totalamount);
        $("#totalpaymentconfirm").html("$"+(parseFloat(totalamount)+6));
        $("#paypalamnt").val(parseFloat(totalamount)+6);
        $(".loader").fadeOut();
    }
    else
    {
        document.getElementById("finalpaymentcart").style.display="none";
        document.getElementById("totalpaymentcart").style.display="none";
        document.getElementById("placeorder").style.display="none";
        $("#placeorder").attr("disabled",true);
        $("#placeorder").css({"opacity":"0.5"});
        $(".loader").fadeOut();
    }
});
function step_2_task()
{
    var user_id = $("#user_id").val();
    var url = "admin/api/orderProcess.php";
    $.post(url,{"type":"getCart","user_id":user_id},function(data)
    {
        var status = data.Status;
        var error="";
        if(status == "Success")
        {
            var items = data.cartData;
            var totalamount = 0;
            for(var i=0;i<items.length;i++)
            {
                var cartshow ="<input type='text' id='stock_value"+i+"' value='"+stock+"' hidden  />" +
                "<input type='text' id='cart_product_quantity"+i+"' hidden value='"+items[i].quantity+"' />";
                var stock=parseInt(stock);
                var prod_quan=parseInt(items[i].quantity);
                if(stock < prod_quan)
                {
                    error=error+"<li style='margin-bottom:10px;'>There is only "+stock+" Items Available in the " +
                    "Stock of "+unescape(items[i].product_name)+".</li>";
                    displaycart(error);
                }
                else
                {
                    window.location="cart.php?stp=2";
                }
            }
        }
    });
}
function displaycart(error)
{
    $(".modal-title").html("<label style='color:red'>Out of Stock Items Demand.</label>");
    $(".modal-body").html(error);
    $("#modal").modal("show");
}
function getUserData(){
    user_id = $("#user_id").val();
    var uurl = "admin/api/registerUser.php";
    $.post(uurl,{"type":"getParticularUserData","user_id":user_id},function(data){
        if(data.Status == "Success"){
            var items = data.userData;
            var name = items.name;
            name = name.split(" ");
            $("#first_name").val(name[0]);
            $("#last_name").val(name[1]);
            $("#adressline1").val(items.address);
            $("#countryId").append("<option selected='selected' value='"+items.b_country+"' countryid='0'>"+items.b_country+"</option>");
            $("#stateId").append("<option selected='selected' value='"+items.b_state+"' countryid='0'>"+items.b_state+"</option>");
            $("#cityId").append("<option selected='selected' value='"+items.b_city+"' countryid='0'>"+items.b_city+"</option>");
            $("#postalcode").val(items.pincode);
            $("#phonenumber").val(items.mobile);
        }
    });
}
function payment_confirmation()
{
    var user_id = $("#user_id").val();
    var billing_fname=$("#first_name").val();
    var billing_lname=$("#last_name").val();
    var addressline1=$("#adressline1").val();
    var addressline2=$("#adressline2").val();
    var country=$("#countryId").val();
    var state=$("#stateId").val();
    var city=$("#cityId").val();
    var pincode=$("#postalcode").val();
    var phone=$("#phonenumber").val();
    var billing_address=addressline1+" "+addressline2;
    if(billing_fname=="" || billing_lname=="" || addressline1=="" || country=="" || state=="" || city=="" || pincode=="" || phone=="")
    {
        $(".modal-title").html("<label style='color:red'>Error Occured</label>");
        $(".modal-body").html("All Feilds are Required Filled...");
        $("#modal").modal("show");
    }
    else
    {
        var url = "admin/api/registerUser.php";
        $.post(url,{"type":"updateBillingAddress","user_id":user_id,"fname":billing_fname,"lname":billing_lname,
        "address":billing_address,"country":country,"state":state,"city":city,"pincode":pincode,"contact":phone},function(data)
        {
            var status = data.Status;
            if(status == "Success")
            {
                get_cart_data();
            }
        });
    }
}
function get_cart_data()
{
    var userid = $("#user_id").val();
    var total = '0';
    var url = "admin/api/orderProcess.php";
    var product="";
    var tam=0;
    $.post(url,{"type":"getCart","user_id":user_id},function(data)
    {
        var status = data.Status;
        if (status == "Success") {
            var items = data.cartData;
            for (var i = 0; i < items.length; i++) {
                var product_type = items[i].product_type;
                if (product_type == "men_shirt") {
                    var product_price = items[i].product_price;
                    var quantity = items[i].quantity;
                    var sh_fit = items[i].sh_fit;
                    var sh_fabric = items[i].sh_fabric;
                    var sh_sleeve = items[i].sh_sleeve;
                    var sh_collar = items[i].sh_collar;
                    var sh_cuff = items[i].sh_cuff;
                    var sh_placket = items[i].sh_placket;
                    var sh_bottomcut = items[i].sh_bottomcut;
                    var sh_pocket = items[i].sh_pocket;
                    var sh_button_color = items[i].sh_button_color;
                    var sh_thread_color = items[i].sh_thread_color;
                    var subtotal = product_price * quantity;
                    tam = tam + parseInt(subtotal);
                    var products = '{"product_type":"'+product_type+'","product_price":"'+product_price+'",' +
                    '"product_quantity":"'+quantity+'","sh_fit":"'+sh_fit+'","sh_fabric":"'+sh_fabric+
                    '","sh_sleeve":"'+sh_sleeve+'","sh_collar":"'+sh_collar+'","sh_cuff":"'+sh_cuff+
                    '","sh_placket":"'+sh_placket+'","sh_bottom_cut":"'+sh_bottomcut+'","sh_pocket":"'+sh_pocket
                    +'","sh_button_color":"'+sh_button_color+'","sh_thread_color":"'+sh_thread_color+'"}';
                    product = product + products;
                    if (i != items.length - 1) {
                        product = product + ",";
                    }
                }
                else if (product_type == "men_trouser") {
                    var product_price = items[i].product_price;
                    var quantity = items[i].quantity;
                    var tr_type = items[i].tr_type;
                    var tr_fabric = items[i].tr_fabric;
                    var tr_fit = items[i].tr_fit;
                    var tr_sidepocket = items[i].tr_sidepocket;
                    var tr_backpocket = items[i].tr_backpocket;
                    var tr_seam = items[i].tr_seam;
                    var tr_coinpocket = items[i].tr_coinpocket;
                    var tr_button_color = items[i].tr_button_color;
                    var tr_thread_color = items[i].tr_thread_color;
                    var subtotal = product_price * quantity;
                    tam = tam + parseInt(subtotal);
                    var products = '{"product_type":"'+product_type+'","product_price":"'+product_price+'",' +
                    '"product_quantity":"'+quantity+'","tr_type":"'+tr_type+'","tr_fit":"'+tr_fit+
                    '","tr_fabric":"'+tr_fabric+'","tr_fit":"'+tr_fit+'","tr_sidepocket":"'+tr_sidepocket+
                    '","tr_backpocket":"'+tr_backpocket+'","tr_seam":"'+tr_seam+'","tr_coinpocket":"'+tr_coinpocket
                    +'","tr_button_color":"'+tr_button_color+'","tr_thread_color":"'+tr_thread_color+'"}';
                    product = product + products;
                    if (i != items.length - 1) {
                        product = product + ",";
                    }
                }
                else if (product_type == "men_suit") {
                    var product_price = items[i].product_price;
                    var quantity = items[i].quantity;
                    var su_fabric = items[i].su_fabric;
                    var su_fit = items[i].su_fit;
                    var su_button = items[i].su_button;
                    var su_lapel = items[i].su_lapel;
                    var su_sleeve_button = items[i].su_sleeve_button;
                    var su_lapel_stich = items[i].su_lapel_stich;
                    var su_pocket = items[i].su_pocket;
                    var su_ticket_pocket = items[i].su_ticket_pocket;
                    var su_vent = items[i].su_vent;
                    var su_elbow_patch = items[i].su_elbow_patch;
                    var su_elbow_fabric = items[i].su_elbow_fabric;
                    var su_suit_linning = items[i].su_suit_linning;
                    var su_button_color = items[i].su_button_color;
                    var su_thread_color = items[i].su_thread_color;
                    var subtotal = product_price * quantity;
                    tam = tam + parseInt(subtotal);
                    var products = '{"product_type":"'+product_type+'","product_price":"'+product_price+'",' +
                    '"product_quantity":"'+quantity+'","su_fabric":"'+su_fabric+'","su_fit":"'+su_fit+
                    '","su_button":"'+su_button+'","su_lapel":"'+su_lapel+'","su_sleeve_button":"'+su_sleeve_button+
                    '","su_lapel_stich":"'+su_lapel_stich+'","su_pocket":"'+su_pocket+'","su_ticket_pocket":"'+
                    su_ticket_pocket+'","su_vent":"'+su_vent+'","su_elbow_patch":"'+su_elbow_patch+'",' +
                    '"su_elbow_fabric":"'+su_elbow_fabric+'","su_suit_linning":"'+su_suit_linning+
                    '","su_button_color":"'+su_button_color+'","su_thread_color":"'+su_thread_color+'"}';
                    product = product + products;
                    if (i != items.length - 1) {
                        product = product + ",";
                    }
                }
            }
            order_detail(product, userid, tam);
        }
        else
        {
            alert("cart should contains some items...");
        }
    });
}
function order_detail(product,userid,total)
{
    var json='{"userid":"'+userid+'","total":'+total+',"items":['+product+']}';
    var url="admin/api/orderProcess.php";
    $.post(url,{"type":"createOrder","json":json},function(data)
    {
        var status = data.Status;
        if(status == "Success")
        {
            window.location="cart.php?stp=3";
        }
        else
        {
            $(".modal-title").html("<label style='color:red'>Error Occured</label>");
            $(".modal-body").html(data.Message);
            $("#modal").modal("show");
        }
    });
}
function deletecart(cart_id)
{
    $(".modal-title").html("<label style='color:green'>Confirm Status !!!</label>");
    $(".modal-body").html("<h5>Are you sure want to delete this item.</h5>");
    $(".modal-footer").html("<input type='button' class='btn btn-warning' value='Confirm' onclick=confirm_deletecart('"+cart_id+"') />");
    $("#modal").modal("show");
}
function confirm_deletecart(cart_id)
{
    var url="admin/api/orderProcess.php";
    $.post(url,{"type":"removeItem","cart_id":cart_id},function(data)
    {
        var status = data.Status;
        if(status == "Success"){
            window.location="cart.php?stp=1";
        }else{
            $(".modal-title").html("<label style='color:red'>Error Occured</label>");
            $(".modal-body").html(data.Message);
            $("#modal").modal("show");
        }
    });
}
function clear_cart(user_id)
{
    $(".modal-title").html("<label style='color:green'>Confirm Status !!!</label>");
    $(".modal-body").html("<h5>Are you sure want to delete all items from cart.</h5>");
    $(".modal-footer").html("<input type='button' class='btn btn-warning' value='Confirm' " +
    "onclick=confirm_clear_cart('"+user_id+"') />");
    $("#modal").modal("show");
}
function confirm_clear_cart(user_id)
{
    var url="admin/api/orderProcess.php";
    $.post(url,{"type":"clearCart","user_id":user_id},function(data)
    {
        var status = data.Status;
        if(status == "Success"){
            window.location="cart.php?stp=1";
        }else{
            $(".modal-title").html("<label style='color:red'>Error Occured</label>");
            $(".modal-body").html(data.Message);
            $("#modal").modal("show");
        }
    });
}
function getCartCount(){
    var user_id = $("#user_id").val();
    var url = "admin/api/orderProcess.php";
    $.post(url,{"type":"getCartCount","user_id":user_id},function(data){
        var Status = data.Status;
        if(Status == "Success"){
            showCartCount(data.count);
        }else{
            // alert("cd"+data.Message);
        }
    }).fail(function(){
        alert("error occured on url");
    });
}
function showCartCount(count){
    $("#cartCount").html(count);
}
function opencart(){
    window.location="cart.php";
}
function solvecart(){
    $(".loader").fadeIn();
    var user_id = $("#user_id").val();
    var bool = false;
    var url="admin/api/orderProcess.php";
    $.ajax({
        type: 'POST',
        url: url,
        data: {"type":"clearCart","user_id":user_id},
        async:false
    });
    $(".loader").fadeOut();
    return true;
}
getCartCount();
</script>