<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 6/28/2017
 * Time: 11:44 AM
 */
  include("header1.php");
  ?>
    <style>
        .back{
            background-image: url("images/slide2_blur.png") ;
            background-repeat: no-repeat;
            width: 100%;
            height: 910px;
            background-size:cover ;
            padding: 5%;
        }
    </style>
    <link href="css/pricing.css" rel="stylesheet"/>
    <div class="container-fluid pricing-back back">
        <div class="row no-gutter back1">
        <div class="col-md-12 pricing-txt pricing-spacing pricing-margin">
            <span >New Clients receive $500 off collections and 10% off single items on first order.</span>
        </div>

        <div class="col-md-12 pricing-txt">
            <ul class="list-inline list-unstyled">
                <li>SUITS</li>
                <li>TUXEDOS</li>
                <li>SHIRTS</li>

            </ul>
        </div>

        <div class="col-md-12 pricing-txt pricing-custom">
            <span>Custom 3 Suit Collections</span>
        </div>

        <div class="col-md-12 ">
            <div class="col-md-4 no-gutter menu-item">
             <div class="col-md-12 no-gutter">
                 <div class="col-md-9 no-gutter"><span class="menu-title">Vitale Barberis</span> </div>
                 <div class="col-md-3 no-gutter"><span class="menu-desc">$1,995</span></div>
             </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">Super 110's Imported from Biella, Italy</span></div>
            </div>

            <div class="col-md-4 no-gutter menu-item">
                <div class="col-md-12 no-gutter">
                    <div class="col-md-9 no-gutter"><span class="menu-title">LORO PIANA</span> </div>
                    <div class="col-md-3 no-gutter"><span class="menu-desc">$2,495</span></div>
                </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">4 Seasons Imported from Valsesia, Italy</span></div>
            </div>

            <div class="col-md-4 no-gutter menu-item">
                <div class="col-md-12 no-gutter">
                    <div class="col-md-9 no-gutter"><span class="menu-title">ERMENEGILDO ZEGNA </span> </div>
                    <div class="col-md-3 no-gutter"><span class="menu-desc"> $3,295</span></div>
                </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">Traveler & Trofeo Imported from Trivero Italy</span></div>
            </div>

            <div class="col-md-4 no-gutter menu-item">
                <div class="col-md-12 no-gutter">
                    <div class="col-md-9 no-gutter"><span class="menu-title">REDA </span> </div>
                    <div class="col-md-3 no-gutter"><span class="menu-desc"> $2,195</span></div>
                </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">Super 150's Imported from Bella, Italy</span></div>
            </div>

            <div class="col-md-4 no-gutter menu-item">
                <div class="col-md-12 no-gutter">
                    <div class="col-md-9 no-gutter"><span class="menu-title">DORMEUIL </span> </div>
                    <div class="col-md-3 no-gutter"><span class="menu-desc"> $2,995</span></div>
                </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">Amadeus 365 Imported from West Yorkshire UK</span></div>
            </div>


        </div>

        <div class="col-md-12 pricing-txt pricing-custom" style="margin-top: 5%">
            <span>Custom Single Suits</span>
        </div>

        <div class="col-md-12 ">
            <div class="col-md-4 no-gutter menu-item">
                <div class="col-md-12 no-gutter">
                    <div class="col-md-9 no-gutter"><span class="menu-title">Vitale Barberis</span> </div>
                    <div class="col-md-3 no-gutter"><span class="menu-desc">$695</span></div>
                </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">Super 110's Imported from Biella, Italy</span></div>
            </div>

            <div class="col-md-4 no-gutter menu-item">
                <div class="col-md-12 no-gutter">
                    <div class="col-md-9 no-gutter"><span class="menu-title">LORO PIANA</span> </div>
                    <div class="col-md-3 no-gutter"><span class="menu-desc">$895</span></div>
                </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">4 Seasons Imported from Valsesia, Italy</span></div>
            </div>

            <div class="col-md-4 no-gutter menu-item">
                <div class="col-md-12 no-gutter">
                    <div class="col-md-9 no-gutter"><span class="menu-title">ERMENEGILDO ZEGNA </span> </div>
                    <div class="col-md-3 no-gutter"><span class="menu-desc"> $1,095</span></div>
                </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">Traveler & Trofeo Imported from Trivero Italy</span></div>
            </div>

            <div class="col-md-4 no-gutter menu-item">
                <div class="col-md-12 no-gutter">
                    <div class="col-md-9 no-gutter"><span class="menu-title">REDA </span> </div>
                    <div class="col-md-3 no-gutter"><span class="menu-desc"> $795</span></div>
                </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">Super 150's Imported from Bella, Italy</span></div>
            </div>

            <div class="col-md-4 no-gutter menu-item">
                <div class="col-md-12 no-gutter">
                    <div class="col-md-9 no-gutter"><span class="menu-title">DORMEUIL </span> </div>
                    <div class="col-md-3 no-gutter"><span class="menu-desc"> $995</span></div>
                </div>
                <div class="col-md-12 no-gutter"><span class="menu-desc">Amadeus 365 Imported from West Yorkshire UK</span></div>
            </div>


        </div>
        <div class="col-md-12 pricing-txt" style="margin-top: 2%">
            <a href="customization.php"><button type="button" class="custom-btn">Go Custom</button></a>
        </div>

        </div>
    </div>
  <?php
  include ("footer1.php");
?>