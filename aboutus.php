<?php include('header.php');?>
<style>
.main-content{margin-top:0!important;}
    #img1{
        float:left; margin-left: 3%; margin-right: 5%; height: 400px; width: 350px
    }

    }#text2{
        float: left; margin-left: 4%; margin-right: 5%; height:auto; width: 320px
    }
</style>
  <div id="PageContainer" class="is-moved-by-drawer">
  <img class="page-fullImg" alt="" src="images/2.jpg" style="width:100%;">
    <main class="main-content" role="main">
      <!-- /templates/page.liquid -->
<div class="grid--full">
  <div>
    <div class="text-center content-wrapper--horizontal section-header">
      <div class="display-table-cell">
        <h1>About Us</h1>
      </div>
    </div>
  </div>
    <div id="img1">
        <div class="rte">
            <div>
                <img src="images/slide5.jpg" width="150%" height="400px">
            </div>
        </div>
    </div>
    <div id="text2">
    <div class="rte" style="">
      <p>A great About Us page helps builds trust between you and your customers. The more content you provide about you and your business, the more confident people will be when purchasing from your store.</p>
	<p>Your About Us page might include:</p>
<ul>
	<li>Who you are</li>
	<li>Why you sell the items you sell</li>
	<li>Where you are located</li>
	<li>How long you have been in business</li>
	<li>How long you have been running your online shop</li>
	<li>Who are the people on your team</li>
	<li>Contact information</li>
	<li>Social links (Twitter, Facebook)</li>
</ul>
<p>To edit the content on this page, go to the <a href="/admin/pages">Pages</a> section of your Shopify admin.</p>
    </div>
   </div>
   <hr class="hr--center">
</div>
      </main>
   <?php include('footer.php');?>