<?php
session_start();
if(isset($_SESSION['benj_user_id'])){
    echo "<input type='hidden' value='".$_SESSION['benj_user_id']."' id='user_id' />";
}else{
    echo "<input type='hidden' value='' id='user_id' />";
}


?>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="shortcut icon" href="./images/icons/favicon_32x.png" type="image/png"/>
    <title>Benjamin || Custom Suits</title>
    <link href="css/bootstrap.min1.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/header.css" rel="stylesheet" />

    <!--    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>-->
    <style>
        .badge {
            background: red none repeat scroll 0 0;
            margin-left: -15px;
            margin-top: -27px;
        }
    </style>
</head>
<body>
 <div class="row  main-header " style="margin: 0px;border-bottom: 1px grey solid;padding-bottom: 2%">
    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12" >
         <span class="header-txt"><i class="fa fa-2x fa-instagram"></i></span>
         <span class="header-txt"><i class="fa fa-2x fa-google-plus"></i></span>
         <span class="header-txt"><i class="fa fa-2x fa-facebook"></i></span>

     </div>
     <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 header-child" style="margin-top: auto;">
         <h1 class="site-header__logo shop-name">
             <a href="index.php" class='site-header__link'>
                 <img src="images/logo/logo.png" class="img-responsive" style="border: 1px white solid" />
             </a>
         </h1>
     </div>
     <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 header-child right-header">
         <ul class="list-unstyled">
         <li class="header-img"><i class="fa fa-2x fa-mobile" style="font-size: 27px"></i></li>
             <li class="header-txt header-right-size" ><span>212.235.2354</span></li>
         <li class="header-img"><i class="fa fa-2x fa-envelope" style="font-size: 17px"></i></li>
         <li class="header-txt header-right-size" ><span>info@benjaminscustom.com</span></li>
             <li class="header-img" onclick="opencart()"><span class="fa fa-shopping-cart fa-2x" style="color:white;
                            cursor:pointer;margin-top:5px"></span>
                 <label class="badge" id="cartCount">0</label></li>
         </ul>
     </div>

 </div>

 <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 no-gutter"  style="margin: 0px;background-color: #252525;">
     <div class="col-md-10 col-sm-12 col-xs-12 col-lg-10 no-gutter " id="main-header" >
<!--       <ul class="list-unstyled ">-->

         <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 no-gutter"  style="border-bottom:1px grey solid;border-right:1px grey solid;">
             <a class="href-txt" href="index.php"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter" style="background-color:#000 ">ABOUT</div></a>
             <a class="href-txt" href="customsuit.php"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter">CUSTOM SUITS</div></a>
             <a href="tuxedo.php" class="href-txt"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter">CUSTOM TUXEDOS</div></a>
             <a href="customshirts.php" class="href-txt"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter">CUSTOM SHIRTS</div></a>
             <a href="pricing.php" class="href-txt"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter">PRICING</div></a>
             <a href="financing.php" class="href-txt"><div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">0% FINANCING</div></a>
             <a href="scanning.php" class="href-txt"><div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">3D BODY SCANNING</div></a>
<!--             <div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">LUXURY FABRICS</div>-->
             <a href="process.php" class="href-txt"><div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">THE PROCESS</div></a>
<!--             <div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">BESPOKE</div>-->
         </div>

         <!--       </ul>-->
     </div>
     <div class="col-md-2 col-sm-12 col-xs-12 col-lg-2 no-gutter book-appointment" >
         <span class="col-md-12 col-sm-12 col-xs-12 book-appointment-txt" onclick="onAppointmentLoad()">Book Appointment</span>
         <?php
         if(isset($_SESSION['benj_user_id'])) {
             ?>
             <span class="col-md-12 col-sm-12 col-xs-12 guest-txt" ><a href="logout.php" class="href-txt"><i class="fa fa-sign-out"></i> Logout</a></span>
             <?php
         }
         else{
             ?>
             <span class="col-md-12 col-sm-12 col-xs-12 guest-txt" onclick="onAppointmentLoad()"> Login/Register</span>
             <?php
         }

         ?>
     </div>
 </div>





