<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/load.css">
<script src="js/jquery-3.1.1.min.js"></script>
<?php include('header.php');?>
<div id="PageContainer" class="is-moved-by-drawer">
  <img class="page-fullImg" alt="" src="images/2.jpg" style="width:100%;">
    <main class="main-content" role="main">
      <!-- /templates/page.liquid -->
<div class="grid--full ">
  <div>
    <div class="text-center content-wrapper--horizontal section-header">
      <div class="display-table-cell">
        <h1>Contact us</h1>
      </div>
    </div>
  </div>
  <div>
    <div class="rte">
    <section id="contact" style="">
    <div class="container">
        <div class="row">
            <div class="about_our_company" style="margin-bottom: 20px;">
                <h1 style="color:#fff;">Write Your Message</h1>
                <div class="titleline-icon"></div>
                <p style="color:#fff;">Lorem Ipsum is simply dummy text of the printing and typesetting </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <form name="sentMessage" id="contactForm" novalidate="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Name *" id="name" required="" data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Your Email *" id="email" required="" data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required="" data-validation-required-message="Please enter your phone number.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Your Message *" id="message" required="" data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-xl get" style="background:white!important;float:right;color:black;">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <p style="color:#fff;">
                    <strong><i class="fa fa-map-marker"></i> Address</strong><br>
                    Benjamin Custom Suits<br>
                    501 Fifth Avenue
                    Suite 712<br>
                    New York<br>
                    NY 10017<br>
                    USA
                </p>
                <p style="color:#fff;"><strong><i class="fa fa-phone"></i> Phone Number</strong><br>
                    +1 212 227 4040</p>
                <p style="color:#fff;">
                    <strong><i class="fa fa-envelope"></i>  Email Address</strong><br>
                    gerald.tuchman@benjaminscustomsuits.com</p>
                <p></p>
            </div>
        </div>
    </div>
</section>
 </div>
   </div>
   <hr class="hr--center">
</div>
 </main>
<?php include('footer.php');?>