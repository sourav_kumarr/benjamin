<?php
    session_start();
    if(isset($_SESSION['benj_user_id'])){
        echo "<input type='hidden' value='".$_SESSION['benj_user_id']."' id='user_id' />";
    }else{
        echo "<input type='hidden' value='' id='user_id' />";
    }
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="shortcut icon" href="images/icons/favicon_32x.png" type="image/png"/>
        <title>Benjamin || Custom Suits</title>
        <link href="css/bootstrap.css" rel="stylesheet" />
        <link href="css/font-awesome.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
        <style>
            .badge {
                background: red none repeat scroll 0 0;
                margin-left: -15px;
                margin-top: -45px;
            }
        </style>
    </head>
    <body>
        <div id="shopify-section-header" class="shopify-section header">
            <header
                role="banner"
                class="site-header is-moved-by-drawer transparent-header with-notification-bar offset-by-notification-bar sm-offset-by-notification-bar squished-header inline-navigation sticky-header"
                data-sticky-header="true"
                data-inline-navigation="true"
                data-notification-bar="true"
                data-transparent-header="true">
                <div class="grid--full grid--full-height">
                    <div class="grid__item grid--full-height table large--three-quarters medium--one-whole header-shop-name" tab="1">
                        <div class="large--text-left display-table-cell">
                            <h1 class="site-header__logo shop-name">
                                <a href="index.php" class='site-header__link'>
                                    <img src="images/logo/logo.png" style="width:280px; float: left ! important;" />
                                </a>
                            </h1>
                            <nav class="main-navigation nav-loading">
                                <ul>
                                    <li class="">
                                        <a href="index.php" data-navigation-top-level>Home</a>
                                    </li>
                                    <?php
                                    if(isset($_SESSION['benj_user_id'])) {
                                        ?>
                                        <li class="has-dropdown">
                                            <a href="customization.php" data-navigation-top-level>Order Now</a>
                                        </li>
                                        <?php
                                    }else{
                                        ?>
                                        <li class="has-dropdown">
                                            <a href="login/index.php" data-navigation-top-level>Order Now</a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <li class="">
                                        <a href="contactus.php" data-navigation-top-level>Contact us</a>
                                    </li>
                                    <li class="">
                                        <a href="aboutus.php" data-navigation-top-level>About Us</a>
                                    </li>
                                    <?php
                                    if(isset($_SESSION['benj_user_id'])){
                                    ?>
                                    <li class="">
                                        <a href="#" data-navigation-top-level>My Account</a>
                                    </li>
                                    <li class="">
                                        <a href="logout.php" data-navigation-top-level>Logout</a>
                                    </li>
                                    <li onclick="opencart()"><span class="fa fa-shopping-cart fa-2x" style="cursor:pointer;
                                        color:white;margin-top:29px"></span>
                                        <label class="badge" id="cartCount">0</label>
                                    </li>
                                    <?php
                                    }else{
                                    ?>
                                    <li class="has-dropdown">
                                        <a href="login/" data-navigation-top-level>Login/Register</a>
                                    </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="grid__item grid--full-height large--one-quarter medium--one-whole small--one-whole controls-container">
                        <div class="grid--full grid--full-height site-header-controls"></div>
                    </div>
                </div>
            </header>
        </div>
