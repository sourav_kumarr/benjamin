
var sctm =["logo.ctm",
"MT-Back_Pocket_hook.ctm",
"MT-Back_Pocket_hook_T.ctm",
"MT-Back_Pocket_hook_B.ctm",
"MT-Back_Pocket_hook_H.ctm",

"MT-Back_Pocket_btn.ctm",
"MT-Back_Pocket_btn_B.ctm",
"MT-Back_Pocket_btn_T.ctm",
"MT-Back_Pocket_btn_H.ctm",

"MT-Back_Pocket_SingleWelt.ctm",
"MT-Back_Pocket_SingleWelt_T.ctm",

"MT-Back_Pocket_R-hook.ctm",
"MT-Back_Pocket_R-hook_T.ctm",
"MT-Back_Pocket_R-hook_B.ctm",
"MT-Back_Pocket_R-hook_H.ctm",

"MT-Back_Pocket_R-SingleWelt.ctm",
"MT-Back_Pocket_R-SingleWelt_T.ctm",

"MT-Back_Pocket_R-btn.ctm",
"MT-Back_Pocket_R-btn_B.ctm",
"MT-Back_Pocket_R-btn_H.ctm",
"MT-Back_Pocket_R-btn_T.ctm",

"MT-Back_Pocket_L-hook.ctm",
"MT-Back_Pocket_L-hook_T.ctm",
"MT-Back_Pocket_L-hook_B.ctm",
"MT-Back_Pocket_L-hook_H.ctm",

"MT-Back_Pocket_L-SingleWelt.ctm",
"MT-Back_Pocket_L-SingleWelt_T.ctm",

"MT-Back_Pocket_L-btn.ctm",
"MT-Back_Pocket_L-btn_B.ctm",
"MT-Back_Pocket_L-btn_H.ctm",
"MT-Back_Pocket_L-btn_T.ctm",



"MT-Coin_Pocket.ctm",
"MT-Coin_Pocket_T.ctm",
"MT-Fit_Loose.ctm",
"MT-Fit_Loose_TS.ctm",
"MT-Fit_Loose_T.ctm",
"MT-Fit_Regular.ctm",
"MT-Fit_Regular_TS.ctm",
"MT-Fit_Regular_T.ctm",
"MT-Fit_Tapered.ctm",
"MT-Fit_Tapered_TS.ctm",
"MT-Fit_Tapered_T.ctm",

"MT-Fixed_parts.ctm",
"MT-Fixed_parts_B.ctm",
"MT-Fixed_parts_T.ctm",
"MT-Fixed_parts_H.ctm",
"MT-Side_Pocket_Beveled.ctm",
"MT-Side_Pocket_Beveled_T.ctm",
"MT-Side_Pocket_Rounded.ctm",
"MT-Side_Pocket_Rounded_T.ctm",
"MT-Side_Pocket_Straight.ctm",
"MT-Side_Pocket_Straight_T.ctm",
"MT-Type_Regular.ctm",
"MT-Type_Regular_TS.ctm",
"MT-Type_Regular_T.ctm",
"MT-Type_Shorts.ctm",
"MT-Type_Shorts_TS.ctm",
"MT-Type_Shorts_T.ctm"
			];
			
var texture =["Black Pique.jpg",
			  "Black Twill.jpg",
			  "Blood red Pique.jpg",
			  "Brown Check.jpg",
			  "Brown HB Twill.jpg",
			  "Brown Pique.jpg",
			  "Brown Stripe.jpg",
			  "Brown Twill.jpg",
			  "Dark Blue Tusser.jpg",
			  "Dark Brown Tusser.jpg",
			  "Dark Gray Plain.jpg",
			  "Dark Gray Tusser.jpg",
			  "Gray Check.jpg",
			  "Gray HB Twill.jpg",
			  "Gray Stripe.jpg",
			  "Light Gray Plain.jpg",
			  "Navy Check.jpg"
			  ];



var xCod =["5","2.5","6",  "4","5","5",  "13","10","5","5","5",
			"5","5","5","15","5","4"];

var yCod = ["5","2.5","6",  "5","5","5"  ,"13","10","5","5","5",
			"5","5","5","15","5","5"];