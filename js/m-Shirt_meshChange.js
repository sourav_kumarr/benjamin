function collorMeshChange(collorId){
	switch(collorId) {
        case "fit_BodyFitModern":
            selectedfit = "body_fit";
            selectedbottomcut = "modern";
            break;
        case "fit_BodyFitClassic":
            selectedfit = "body_fit";
            selectedbottomcut = "classic";
            break;
        case "fit_BodyFitStraight":
            selectedfit = "body_fit";
            selectedbottomcut = "straight";
            break;
            /////////////////
		case "fit_SlimFitModern":
            selectedfit = "slim_fit";
            selectedbottomcut = "modern";
            break;
        case "fit_SlimFitClassic":
            selectedfit = "slim_fit";
            selectedbottomcut = "classic";
            break;
        case "fit_SlimFitStraight":
            selectedfit = "slim_fit";
            selectedbottomcut = "straight";
            break;
            ////////////////////////
        case "fit_NormalFitModern":
            selectedfit = "normal_fit";
            selectedbottomcut = "modern";
            break;
        case "fit_NormalFitClassic":
            selectedfit = "normal_fit";
            selectedbottomcut = "classic";
            break;
        case "fit_NormalFitStraight":
            selectedfit = "normal_fit";
            selectedbottomcut = "straight";
            break;
            /////////////////////////
        case "fit_LooseFitModern":
            selectedfit = "loose_fit";
            selectedbottomcut = "modern";
            break;
        case "fit_LooseFitClassic":
            selectedfit = "loose_fit";
            selectedbottomcut = "classic";
            break;
        case "fit_LooseFitStraight":
            selectedfit = "loose_fit";
            selectedbottomcut = "straight";
            break;
            ///////////////
        case "collar_BusinessClassic":
            selectedcollar = "business";
            break;
        case "collar_BusinessSuperior":
            selectedcollar = "business_superior";
            break;
        case "collar_ButtonDownClassic":
            selectedcollar = "buttondown_classic";
            break;
        case "collar_ButtonDownModern":
            selectedcollar = "buttondown_modern";
            break;
        case "collar_Club":
            selectedcollar = "club";
            break;
        case "collar_ModernClub":
            selectedcollar = "modern_club";
            break;
        case "collar_CutAwayClassic":
            selectedcollar = "cutaway_classic";
            break;
        case "collar_CutAwayCasual":
            selectedcollar = "cutaway_classic";
            break;
        case "collar_CutAwayExtreme":
            selectedcollar = "cutaway_extreme";
            break;
        case "collar_CutAwayModern":
            selectedcollar = "cutaway_modern";
            break;
        case "collar_CutAwaySuperior":
            selectedcollar = "cutaway_superior";
            break;
        case "collar_CutAwayTwoBtn":
            selectedcollar = "cutaway_two_btn";
            break;
        case "collar_TurnDown_Sperior":
            selectedcollar = "turndown_superior";
            break;
        case "collar_Tab":
            selectedcollar = "tab";
            break;
        case "collar_Wing":
            selectedcollar = "wing";
            break;
        case "collar_Mao":
            selectedcollar = "mao";
            break;
        case "collar_Pin":
            selectedcollar = "pin";
            break;
        case "placket_WithPlacket":
            selectedplacket = "yes";
            break;
        case "placket_hiddenButton":
            selectedplacket = "hidden_button";
            break;
        case "placket_Narrow":
            selectedplacket = "narrow";
            break;
        case "pocket_no":
            selectedpocket = "no";
            break;
        case "pocket_Lt-Straight_Poc":
            selectedpocket = "left_straight";
            break;
        case "pocket_Lt-Vshaped_Poc":
            selectedpocket = "left_v_shaped";
            break;
        case "pocket_Lt-Ushaped_Poc":
            selectedpocket = "left_u_shaped";
            break;
        case "pocket_Straight_Poc":
            selectedpocket = "both_straight";
            break;
        case "pocket_Vshaped_Poc":
            selectedpocket = "both_v_shaped";
            break;
        case "pocket_Ushaped_Poc":
            selectedpocket = "both_u_shaped";
            break;
	}
	var coll = collorId;
	var collSpt = coll.split("_");
	var collFront,collStndFront,collStndBack,Thrd,Btn,Hole;
	var cuffFront;
	var placketIn,placketOutFront,placketOutBack;
		scene.traverse( function (shirt_mesh) {
			if ( shirt_mesh instanceof THREE.Mesh ) {	
				 if(shirt_mesh.visible){
					 //alert(collorId);
					  var collRemove = shirt_mesh.name.split("_");	
					 if(collRemove[0] == collSpt[0] || collRemove[0] == "Flap"){
						shirt_mesh.visible=false;
					 }
				 }
				 if(shirt_mesh.name == coll){
					 shirt_mesh.visible = true;
						if(shirt_mesh.visible){
							var collAdd = shirt_mesh.name.split("_");	
							if(collAdd[0] == collSpt[0]){
								collFront = coll+'_'+"Front";collStndFront = coll+'_'+"Stnd-Front";collStndBack = coll+'_'+"Stnd-Back";
								cuffFront = coll+'_'+"Cuff-Front";
								placketIn = coll+'_'+"In",placketOutFront = coll+'_'+"OutFront",placketOutBack = coll+'_'+"OutBack";
								Thrd = coll+'_'+"Thread";Btn = coll+'_'+"Btn";Hole = coll+'_'+"Hole";								
							}
						}
				 }
				 if(collFront == shirt_mesh.name || collStndFront == shirt_mesh.name || collStndBack == shirt_mesh.name 
					|| cuffFront == shirt_mesh.name
					|| placketIn == shirt_mesh.name || placketOutFront == shirt_mesh.name || placketOutFront == shirt_mesh.name
					|| Thrd == shirt_mesh.name || Btn == shirt_mesh.name || Hole == shirt_mesh.name){
					 shirt_mesh.visible = true;
				 }
				 
			}
		});
}
function cuffMeshChange(cuffId){
	switch(cuffId){
		case "cuff_SingleButtonRounded":
			selectedcuff = "single_button_rounded";
			break;
        case "cuff_SingleButtonBeveled":
            selectedcuff = "single_button_beveled";
            break;
        case "cuff_SingleButtonStraight":
            selectedcuff = "single_button_straight";
            break;
        case "cuff_ConvertibleRounded":
            selectedcuff = "convertible_round";
            break;
        case "cuff_DoubleButtonRounded":
            selectedcuff = "double_button_rounded";
            break;
        case "cuff_DoubleButtonBeveled":
            selectedcuff = "single_button_beveled";
            break;
        case "cuff_French":
            selectedcuff = "french";
            break;
        case "cuff_Link":
            selectedcuff = "link";
            break;
        case "cuff_Narrow":
            selectedcuff = "narrow";
            break;
        case "cuff_SingleButtonCasual":
            selectedcuff = "single_button_casual";
            break;
	}
	var cuff = cuffId;
	var cuffSpt = cuff.split('_');
	var cuffFront,Thrd,Btn,Hole;
	var slvBtn,slvHole,slvThread;
		scene.traverse(function(shirt_mesh){
			if(shirt_mesh instanceof THREE.Mesh){
				if(shirt_mesh.visible){
					var cuffRemove = shirt_mesh.name.split('_');
						if(cuffRemove[0] == cuffSpt[0]){
							shirt_mesh.visible = false;
						}
					}
					if(cuff != "cuff_French" || cuff != "cuff_Link" ){
								if(!shirt_mesh.visible){
									if(shirt_mesh.name == "sleeve_long"){
										shirt_mesh.visible = true;
										slvBtn = "sleeve_long_Btn";
										slvHole = "sleeve_long_Hole";
										slvThread = "sleeve_long_Thread";
									}
								}
							}
					if(shirt_mesh.name == cuff){
						shirt_mesh.visible = true;	
							if(shirt_mesh.visible){
								var cuffAdd = shirt_mesh.name.split("_");	
								if(cuffAdd[0] == cuffSpt[0]){									
									cuffFront = cuff+'_'+"Cuff-Front";Thrd = cuff+'_'+"Thread";
									Btn = cuff+'_'+"Btn";Hole = cuff+'_'+"Hole";								
								}
						}
					}
				}
				if(cuffFront == shirt_mesh.name || Thrd == shirt_mesh.name || Btn == shirt_mesh.name || Hole == shirt_mesh.name
				  || slvBtn == shirt_mesh.name || slvHole == shirt_mesh.name || slvThread == shirt_mesh.name){
					shirt_mesh.visible = true;
				}
			});
}
function linkFrenchCuff(linkFrnCuff){
	var frCuff = linkFrnCuff;
	var frCuffSpt = frCuff.split('_');
	var Btn,Hole,Thread;
		scene.traverse(function(shirt_mesh){
			if(shirt_mesh instanceof THREE.Mesh){
				if(shirt_mesh.visible){
					var fCuffRemove = shirt_mesh.name.split('_');
						if(fCuffRemove[0] == "sleeve" || fCuffRemove[0] == "cuff" ){
							shirt_mesh.visible = false;
						}
					}
					if(shirt_mesh.name == frCuff){
						shirt_mesh.visible = true;
							if(shirt_mesh.visible){
								var fCuffAdd = shirt_mesh.name.split('_');
									if(fCuffAdd[1] == frCuffSpt[1]){
										Btn = frCuff+'_'+"Btn";
										Hole = frCuff+'_'+"Hole";
										Thread = frCuff+'_'+"Thread";
									}
								}
							}
						}
						if(Btn == shirt_mesh.name || Hole == shirt_mesh.name || Thread == shirt_mesh.name){
							shirt_mesh.visible =true;
						}
		});
}
function longSleeveChange(longSleeve){
	switch(longSleeve){
		case "sleeve_long":
			selectedsleeve = "long";
			break;
        case "sleeve_Rollup":
            selectedsleeve = "roll_up";
            break;
        case "sleeve_Short":
            selectedsleeve = "short";
            break;
	}
	var slv = longSleeve;
	var slvSpt = slv.split("_");
	var cuff,cuffFront,cuffBtn,cuffHole,cuffThread;
		scene.traverse(function(shirt_mesh){
			if(shirt_mesh instanceof THREE.Mesh){
				if(shirt_mesh.visible){
					var lSlv = shirt_mesh.name.split('_');
						if(lSlv[0] == slvSpt[0] || lSlv[0] =="cuff" || lSlv[0] == "elbow"){
							shirt_mesh.visible = false;
						}
					}
						if(shirt_mesh.name == slv ){
							shirt_mesh.visible = true;
							if(slvSpt[1] == "long"){
								cuff = "cuff_ConvertibleRounded";
								cuffFront = "cuff_ConvertibleRounded_Cuff-Front";
								cuffBtn = "cuff_ConvertibleRounded_Btn";
								cuffHole = "cuff_ConvertibleRounded_Hole";
								cuffThread = "cuff_ConvertibleRounded_Thread";
							}
							else{
								cuffBtn = slv+'_'+"Btn";
								cuffHole = slv+'_'+"Hole";
								cuffThread = slv+'_'+"Thread";
							}
						}
					}
				if(cuff == shirt_mesh.name||cuffFront == shirt_mesh.name||cuffBtn == shirt_mesh.name
					||cuffHole == shirt_mesh.name || cuffThread == shirt_mesh.name){
					shirt_mesh.visible=true;
				}
			});
}


function tuxedoChange(tuxMesh){
	switch(tuxMesh){
        case "placket_S-Fit_Tux":
            selectedplacket = "tuxedo_slim_fit";
            break;
        case "placket_B-Fit_Tux":
            selectedplacket = "tuxedo_body_fit";
            break;
        case "placket_R-Fit_Tux":
            selectedplacket = "tuxedo_regular_fit";
            break;
        case "placket_L-Fit_Tux":
            selectedplacket = "tuxedo_loose_fit";
            break;
        case "placket_B-Fit_Tie":
            selectedplacket = "white_tie";
            break;
	}
	var s =tuxMesh;
	var k = s.split("_");
		scene.traverse( function (shirt_mesh) {
			if ( shirt_mesh instanceof THREE.Mesh ) {
				if(shirt_mesh.visible){
					var m = shirt_mesh.name.split('_');
							if(m[0] == k[0] || m[0] == "pocket"){						
								shirt_mesh.visible = false;								
								}
						}
			}
			if(shirt_mesh.name == s){
				shirt_mesh.visible=true;
			}
		});
}
//FOR ELBOW CHANGE
function elbowChange(getId){
	switch(getId){
		case "elbow":
			selectedelbowpatch = "no";
			break;
        case "elbow_Patch":
            selectedelbowpatch = "yes";
            break;
	}
	var a = getId;
	var q = a.split('_');
	var Thrd;
	scene.traverse( function (menBlazer_mesh) {
		if ( menBlazer_mesh instanceof THREE.Mesh ) {
			if(menBlazer_mesh.visible){
				var n = menBlazer_mesh.name.split('_');
				if(q[0] == n[0]){
					menBlazer_mesh.visible = false;
				}
			}
			if(menBlazer_mesh.name == a){
				menBlazer_mesh.visible=true;
				if(menBlazer_mesh.visible){									
					var j = menBlazer_mesh.name.split('_');
						if(j[1][2] == q[1][2]){
							Thrd = a+'_'+"T";	
								}
									}
			}
			if(menBlazer_mesh.name == Thrd){
				menBlazer_mesh.visible=true;
				
			}
		}
	});
}