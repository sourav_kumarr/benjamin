var check, ind = 0;
function otherLoad() {
    check = mShirt_RemLoad.length;
    (function () {
        function load() {
            if (ind < check) {
                var name = mShirt_RemLoad[ind].substring(0, mShirt_RemLoad[ind].lastIndexOf('.'));
                loader.load("ctm/" + mShirt_RemLoad[ind], function (geometry) {
                    var c = name.split("_");
                    if (c[c.length - 1] == "Btn") {
                        mShirtLoad(geometry, .035, button_material, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Tux") {
                        tux_mat = new THREE.MeshPhongMaterial({
                            map: texArray[73],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        tux_mat.map.wrapS = tux_mat.map.wrapT = THREE.RepeatWrapping;
                        tux_mat.map.repeat.set(forX[73], forY[73]);//.027  .027
                        mShirtLoad(geometry, .035, tux_mat, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Tie") {
                        tie_material = new THREE.MeshPhongMaterial({
                            map: texArray[16],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        tie_material.map.wrapS = tie_material.map.wrapT = THREE.RepeatWrapping;
                        tie_material.map.repeat.set(forX[16], forY[16]);//.027  .027
                        mShirtLoad(geometry, .035, tie_material, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Hole" || c[c.length - 1] == "Thread") {
                        mShirtLoad(geometry, .035, thread_material, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Patch") {
                        patch_material = new THREE.MeshPhongMaterial({
                            map: texArray[12],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        patch_material.map.wrapS = patch_material.map.wrapT = THREE.RepeatWrapping;
                        patch_material.map.repeat.set(forX[12], forY[12]);//.027  .027
                        mShirtLoad(geometry, .035, patch_material, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Poc") {
                        poc_material = new THREE.MeshPhongMaterial({
                            map: texArray[12],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        colFront_mat.map.wrapS = colFront_mat.map.wrapT = THREE.RepeatWrapping;
                        colFront_mat.map.repeat.set(forX[12], forY[12]);//.027  .027
                        mShirtLoad(geometry, .035, poc_material, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Front") {
                        colFront_mat = new THREE.MeshPhongMaterial({
                            map: texArray[12],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        colFront_mat.map.wrapS = colFront_mat.map.wrapT = THREE.RepeatWrapping;
                        colFront_mat.map.repeat.set(forX[12], forY[12]);//.027  .027
                        mShirtLoad(geometry, .035, colFront_mat, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Stnd-Back") {
                        colStndBack_mat = new THREE.MeshPhongMaterial({
                            map: texArray[12],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        colStndBack_mat.map.wrapS = colStndBack_mat.map.wrapT = THREE.RepeatWrapping;
                        colStndBack_mat.map.repeat.set(forX[12], forY[12]);//.027  .027
                        mShirtLoad(geometry, .035, colStndBack_mat, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Stnd-Front") {
                        colStndFront_mat = new THREE.MeshPhongMaterial({
                            map: texArray[12],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        colStndFront_mat.map.wrapS = colStndFront_mat.map.wrapT = THREE.RepeatWrapping;
                        colStndFront_mat.map.repeat.set(forX[12], forY[12]);//.027  .027
                        mShirtLoad(geometry, .035, colStndFront_mat, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "Cuff-Front") {
                        cuffFront_mat = new THREE.MeshPhongMaterial({
                            map: texArray[12],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        cuffFront_mat.map.wrapS = cuffFront_mat.map.wrapT = THREE.RepeatWrapping;
                        cuffFront_mat.map.repeat.set(forX[12], forY[12]);//.027  .027
                        mShirtLoad(geometry, .035, cuffFront_mat, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "In") {
                        placketIn_mat = new THREE.MeshPhongMaterial({
                            map: texArray[0],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        placketIn_mat.map.wrapS = placketIn_mat.map.wrapT = THREE.RepeatWrapping;
                        placketIn_mat.map.repeat.set(forX[12], forY[12]);//.027  .027
                        mShirtLoad(geometry, .035, placketIn_mat, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "OutBack") {
                        placketOutBack_mat = new THREE.MeshPhongMaterial({
                            map: texArray[12],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        placketOutBack_mat.map.wrapS = placketOutBack_mat.map.wrapT = THREE.RepeatWrapping;
                        placketOutBack_mat.map.repeat.set(forX[12], forY[12]);//.027  .027
                        mShirtLoad(geometry, .035, placketOutBack_mat, 0, -48, 0, 0, name, false);
                    }
                    else if (c[c.length - 1] == "OutFront") {
                        placketOutFront_mat = new THREE.MeshPhongMaterial({
                            map: texArray[12],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        placketOutFront_mat.map.wrapS = placketOutFront_mat.map.wrapT = THREE.RepeatWrapping;
                        placketOutFront_mat.map.repeat.set(forX[12], forY[12]);//.027  .027
                        mShirtLoad(geometry, .035, placketOutFront_mat, 0, -48, 0, 0, name, false);
                    }
                    else {
                        material = new THREE.MeshPhongMaterial({
                            map: texArray[12],
                            combine: THREE.MixOperation,
                            specular: 0xBABABA,
                            shininess: 5,
                            side: THREE.DoubleSide,
                        });
                        material.map.wrapS = material.map.wrapT = THREE.RepeatWrapping;
                        material.map.repeat.set(forX[12], forY[12]);
                        mShirtLoad(geometry, .035, material, 0, -48, 0, 0, name, false);
                    }
                    ++ind;
                    load(); // for looping the function
                }, {useWorker: true});
            }
        }

        load();
    })();
}