function fitMesh(getId){
	switch (getId){
		case "MS-Tro_Fit_RFit_BPanel":
			selectedfit = "regular";
			break;
		case "MS-Tro_Fit_SFit_BPanel":
			selectedfit = "slim";
			break;
	}
	var a =getId;
	var q =a.split("_");
	var mesh ="";
		scene.traverse( function (menTrouser_mesh) {
			 if ( menTrouser_mesh instanceof THREE.Mesh ) {
				 if(menTrouser_mesh.visible){
					var n = menTrouser_mesh.name.split('_');
						if(n[1] == q[1] ){						
							menTrouser_mesh.visible = false;								
							}
			  			}
						 if(menTrouser_mesh.name == a){
							 menTrouser_mesh.visible=true;
								 if(menTrouser_mesh.visible){
									 var j = menTrouser_mesh.name.split('_');
										 if(j[1] == q[1]){
											mesh = j[0]+'_'+j[1]+'_'+j[2]+"_FPanel_BevelPoc";
										 }
								}
						}
							 if(mesh == menTrouser_mesh.name){
								 menTrouser_mesh.visible =true;
							 }
				 }			 
		});
}
function pocChange(getId){
	switch(getId){
		case "MS-Tro_Fit_RFit_FPanel_BevelPoc":
			selectedsidepocketval = "beveled";
			break;
        case "MS-Tro_Fit_RFit_FPanel_StraightPoc":
            selectedsidepocketval = "straight";
            break;
        case "MS-Tro_Fit_SFit_FPanel_BevelPoc":
            selectedsidepocketval = "beveled";
            break;
        case "MS-Tro_Fit_SFit_FPanel_StraightPoc":
            selectedsidepocketval = "straight";
            break;
	}
	var a = getId;
	var q = a.split("_");
		scene.traverse( function (menTrouser_mesh) {
			if ( menTrouser_mesh instanceof THREE.Mesh ) {
				  if(menTrouser_mesh.visible){	
					var n = menTrouser_mesh.name.split('_');
						if(n[3]==q[3]){
							menTrouser_mesh.visible = false;
						}
				  }
				  if(menTrouser_mesh.name == a){
					  menTrouser_mesh.visible=true;
				  }
			}
		});
}
function meshChange(getId){
	switch (getId){
		case "MS-Tro_B-poc_Single_Lt":
			selectedbackpocket = "left_single_welt";
			break;
        case "MS-Tro_B-poc_Hook_Lt":
            selectedbackpocket = "left_with_hook";
            break;
        case "MS-Tro_B-poc_Double_Lt":
            selectedbackpocket = "left_with_button";
            break;
        case "MS-Tro_B-poc_Single_Rt":
            selectedbackpocket = "right_single_welt";
            break;
        case "MS-Tro_B-poc_Hook_Rt":
            selectedbackpocket = "right_with_hook";
            break;
        case "MS-Tro_B-poc_Double_Rt":
            selectedbackpocket = "right_with_button";
            break;
		case "MS-Tro_B-poc_Single_Both":
            selectedbackpocket = "both_single_welt";
            break;
        case "MS-Tro_B-poc_Hook_Both":
            selectedbackpocket = "both_with_hook";
            break;
        case "MS-Tro_B-poc_Double-Btn_Both":
            selectedbackpocket = "both_with_button";
            break;
	}
	var a = getId;
	var q = a.split("_");
	var btn;	
	var hole;
		scene.traverse( function (menTrouser_mesh) {
          if ( menTrouser_mesh instanceof THREE.Mesh ) {			  
			  if(menTrouser_mesh.visible){	
					var n = menTrouser_mesh.name.split('_');
						if(n[1] == q[1] ){	
								menTrouser_mesh.visible = false;								
								}
			  				}			  			
						if( menTrouser_mesh.name == a ){			
								menTrouser_mesh.visible = true;
								var j = menTrouser_mesh.name.split('_');
										 if(j[1] == q[1]){
											hole = a+"_H";
											btn = a+ "_B";
										 }
								}	
							if(hole == menTrouser_mesh.name || btn == menTrouser_mesh.name ){
								 menTrouser_mesh.visible =true;
							 }
					}							
			});
}

function btnChange(getId){
	switch (getId){
		case "MS-Tro_B-FixedPart_B":
			selectedbuttoning = "visible";
			break;
        case "MS-Tro_B-FixedPart":
            selectedbuttoning = "hidden";
            break;
	}
	var a =getId;
	var q = a.split("_");
	var hole ="";
		scene.traverse( function (menTrouser_mesh) {
			if ( menTrouser_mesh instanceof THREE.Mesh ) {
				if(menTrouser_mesh.visible){
					var n = menTrouser_mesh.name.split('_');
						if(n[1] == q[1] ){	
								menTrouser_mesh.visible = false;								
								}
						}
						if( menTrouser_mesh.name == a ){			
								menTrouser_mesh.visible = true;
								//if(n[1] == ){}
								if(menTrouser_mesh.visible){
									var j = menTrouser_mesh.name.split('_');
										 if(j[1] == q[1]){
											hole = j[0]+'_'+j[1]+"_H";
										 }
									}
								}
								if(hole == menTrouser_mesh.name){
								 menTrouser_mesh.visible =true;
							 }
			}
		});
	
}