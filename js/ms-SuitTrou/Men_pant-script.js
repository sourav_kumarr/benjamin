
//filters
	$( document ).ready(function() {
		$("#all").click(function() {
			$(".fabric").fadeIn(500);
			$("#all").addClass("active");
		});
		$("#Black").click(function() {
			$(".Black").fadeIn(500);
			$(".fabric:not(.Black)").fadeOut(0);
			$("#Black").addClass("active");
		});
		$("#Blue").click(function() {
			$(".Blue").fadeIn(500);
			$(".fabric:not(.Blue)").fadeOut(0);
			$("#Blue").addClass("active");
		});
		$("#Brown").click(function() {
			$(".Brown").fadeIn(500);
			$(".fabric:not(.Brown)").fadeOut(0);
			$("#Brown").addClass("active");
		});
		$("#Gray").click(function() {
			$(".Gray").fadeIn(500);
			$(".fabric:not(.Gray)").fadeOut(0);
			$("#Gray").addClass("active");
		});
		$("#Red").click(function() {
			$(".Red").fadeIn(500);
			$(".fabric:not(.Red)").fadeOut(0);
			$("#Red").addClass("active");
		});
		$("#Navy").click(function() {
			$(".Navy").fadeIn(500);
			$(".fabric:not(.Navy)").fadeOut(0);
			$("#Navy").addClass("active");
		});
		$("#solid").click(function() {
			$(".solid").fadeIn(500);
			$(".fabric:not(.solid)").fadeOut(0);
			$("#solid").addClass("active");
		});
		$("#check").click(function() {
			$(".check").fadeIn(500);
			$(".fabric:not(.check)").fadeOut(0);
			$("#check").addClass("active");
		});
		$("#stripe").click(function() {
			$(".stripe").fadeIn(500);
			$(".fabric:not(.stripe)").fadeOut(0);
			$("#stripe").addClass("active");
		});
	});
//onload function
	function hideDiv() {
      	document.getElementById("filte").style.display = 'none';
    } 
//filter 
	function filter() {
		$(".filters").show(500);
    }
//hide filter
	function filter1() {
        $(".filters").hide(500);
    }
//Show each panel
    function showPanel(id)
	{
		$(".mol_panel").css("display","none");
		$("#" + id).css("display","block");
		if(id == "panel_side_pocket")
		{
			var eleid = $(".fit.active").attr("id");
			if(eleid == "MS-Tro_Fit_RFit_BPanel")
		 {
		    $(".reg_side").show();
			$(".slim_side").hide();
			 
		 }
		if(eleid == "MS-Tro_Fit_SFit_BPanel")
		 {
		    $(".reg_side").hide();
			$(".slim_side").show();
		 }
		}
	else if(id == "panel_pocket_design")
		{
			var eleid = $(".Back_pocket.active").attr("id");
			if(eleid == "MS-Tro_B-poc_Single_Lt")
			{
				$(".left_design").show();
				$(".right_design").hide();
				$(".both_design").hide();
			}
			if(eleid == "MS-Tro_B-poc_Single_Rt")
			{
				$(".left_design").hide();
				$(".right_design").show();
				$(".both_design").hide();
			}
			if(eleid == "MS-Tro_B-poc_Single_Both")
			{
				$(".left_design").hide();
				$(".right_design").hide();
				$(".both_design").show();
			}
		}
	}
	$(document).ready(function() {
            $("#MS-Tro_B-poc_Single").click(function() {
				document.getElementById("pocket_design-button-icon-div").style.pointerEvents = 'none';
				document.getElementById("pocket_design-button-icon-div").style.color = "blue";
			});	
			$("#MS-Tro_B-poc_Single_Both").click(function() {
				document.getElementById("pocket_design-button-icon-div").style.pointerEvents = 'auto'; 
				document.getElementById("pocket_design-button-icon-div").style.color = "black";
			});
			$("#MS-Tro_B-poc_Single_Rt").click(function() {
				document.getElementById("pocket_design-button-icon-div").style.pointerEvents = 'auto'; 
				document.getElementById("pocket_design-button-icon-div").style.color = "black";
			});
			$("#MS-Tro_B-poc_Single_Lt").click(function() {
				document.getElementById("pocket_design-button-icon-div").style.pointerEvents = 'auto'; 
				document.getElementById("pocket_design-button-icon-div").style.color = "black";
			});
    });
			
 
        function closePanel()
        {
        $(".mol_panel").css("display","none");
        }
//toggle visibility
        var divs = ["panel_fit","panel_fabric", "panel_side_pocket","panel_pocket_design", 
        "panel_pocket_fabric","panel_buttoning","panel_back_pocket",
         "panel_button", "panel_thread","panel_waist_band"];
        var visibleDivId = null;
        function toggleVisibility(divId) {
        if(visibleDivId === divId) {
            visibleDivId = null;
            } else {
            visibleDivId = divId;
        }
        hideNonVisibleDivs();
        }
        function hideNonVisibleDivs() {
        var i, divId, div;
        for(i = 0; i < divs.length; i++) {
        divId = divs[i];
        div = document.getElementById(divId);
        if(visibleDivId === divId) {
          div.style.display = "block";
        } else {
          div.style.display = "none";
        }
      }
    }

//left conatiner main image swipe
		$(function(){
	      $( ".content_inner" ).click(function() {
		  var container_title = $(this).attr("title");
		  var title = container_title.split("|");
		  var container = title[0];
		  var image = title[1];
		  var src = "images2/"+container+"/"+image;
		  /* 
		  alert(title);
		  alert(container);
		  alert(image);
		  alert(src);
		   */
		  $("."+container).removeClass("active");
		  $("#img_"+container).attr("src",src);
		  $(this).addClass("active");
		  	});
		});
// // moveable and resize able of panel
    // $(document).ready(function() {
        // $(function() {
          // $("#panel_fit").draggable();
         // $("#panel_fabric").draggable();
         // $("#panel_pocket_fabric").draggable();
         // $("#panel_side_pocket").draggable();
         // $("#panel_buttoning").draggable();
         // $("#panel_back_pocket").draggable();
         // $("#panel_back_pocket_button").draggable();
         // $("#panel_button").draggable();
         // $("#panel_thread").draggable();
          // $("#panel_waist_band").draggable();
                // });
           // });
		
		      

