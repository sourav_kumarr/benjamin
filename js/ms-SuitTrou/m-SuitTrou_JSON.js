
var sctm =[
			"MS-Tro_Band.ctm",
			"MS-Tro_B-Poc_Li.ctm",
			"MS-Tro_B-poc_Single_Both.ctm",
			"MS-Tro_FixedPart.ctm",
			"MS-Tro_B-FixedPart_B.ctm",
			"MS-Tro_B-FixedPart_H.ctm",
			"MS-Tro_F-Poc_Li.ctm",
			"MS-Tro_Fit_SFit_BPanel.ctm",
			"MS-Tro_Fit_SFit_FPanel_BevelPoc.ctm",						
			
			"MS-Tro_Fit_RFit_BPanel.ctm",
			"MS-Tro_Fit_RFit_FPanel_BevelPoc.ctm",
			
			"MS-Tro_Fit_RFit_FPanel_StraightPoc.ctm",
			"MS-Tro_Fit_SFit_FPanel_StraightPoc.ctm",
			
			"MS-Tro_B-poc_Hook_Both.ctm",
			"MS-Tro_B-poc_Hook_Both_B.ctm",
			"MS-Tro_B-poc_Hook_Both_H.ctm",
			
			"MS-Tro_B-poc_Hook_Lt.ctm",
			"MS-Tro_B-poc_Hook_Lt_B.ctm",
			"MS-Tro_B-poc_Hook_Lt_H.ctm",
			
			"MS-Tro_B-poc_Hook_Rt.ctm",
			"MS-Tro_B-poc_Hook_Rt_B.ctm",
			"MS-Tro_B-poc_Hook_Rt_H.ctm",
			
			"MS-Tro_B-poc_Single_Both.ctm",
			"MS-Tro_B-poc_Single_Lt.ctm",
			"MS-Tro_B-poc_Single_Rt.ctm",
			
			"MS-Tro_B-poc_Double_Both.ctm",
			
			
			"MS-Tro_B-poc_Double_Lt.ctm",
			"MS-Tro_B-poc_Double_Lt_B.ctm",
			"MS-Tro_B-poc_Double_Lt_H.ctm",
			
			"MS-Tro_B-poc_Double_Rt.ctm",
			"MS-Tro_B-poc_Double_Rt_B.ctm",
			"MS-Tro_B-poc_Double_Rt_H.ctm",
			
			"MS-Tro_B-poc_Double-Btn_Both.ctm",
			"MS-Tro_B-poc_Double-Btn_Both_B.ctm",
			"MS-Tro_B-poc_Double-Btn_Both_H.ctm"
			];
			
			
var texture =["AR 2231 01 600 all wool 260G.jpg",
			  "AR 2235 05 580 73 wool 27 mohair 290 gr.jpg",
			  "AR 2251 01 600 all wool 260G.jpg",
			  "AR 2251 02 600 all wool 260g.jpg",
			  "AR 2251 16 600 all wool 260gr.jpg",
			  "AR 2251 17 600 all wool 260gr.jpg",
			  "AR 2251 49 600 all wool 260gr.jpg",
			  "AR 2251 50 600 all wool 260g.jpg",
			  "AR 2251 55 600 all wool 260gr.jpg",
			  "AR 2251 58 600 all wool 260gr.jpg",
			  "AR 2251 59 600 all wool 260gr.jpg",
			  "AR 2251 60 600 all wool 260gr.jpg",
			  "AR 2258 74 560 sunny season.jpg",
			  "AR 2258 75 560 sunny season.jpg"
			  
			  ];


var xCod =["65.41","39.23","67.87","67.87","97.55","32.75","436.3","184.6","515.6","515.6","246.6","246.6","111.8","111.8","56"];

var yCod =["56.07","32.43","53.58","53.58","77.85","32.07","397.0","132.7","543.8","543.8","248.1","248.1","110.2","110.2","56"];
			
var bandTexture =["0_Red.jpg","1_Black.jpg","2_Blue.jpg","3_Brown.jpg"];