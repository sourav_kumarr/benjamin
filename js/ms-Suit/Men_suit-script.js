//filters
	$( document ).ready(function() {
		$("#all").click(function() {
			$(".fabric").fadeIn(500);
			$("#all").addClass("active");
		});
		$("#Black").click(function() {
			$(".Black").fadeIn(500);
			$(".fabric:not(.Black)").fadeOut(0);
			$("#Black").addClass("active");
		});
		$("#Blue").click(function() {
			$(".Blue").fadeIn(500);
			$(".fabric:not(.Blue)").fadeOut(0);
			$("#Blue").addClass("active");
		});
		$("#Brown").click(function() {
			$(".Brown").fadeIn(500);
			$(".fabric:not(.Brown)").fadeOut(0);
			$("#Brown").addClass("active");
		});
		$("#White").click(function() {
			$(".White").fadeIn(500);
			$(".fabric:not(.White)").fadeOut(0);
			$("#White").addClass("active");
		});
		$("#Green").click(function() {
			$(".Green").fadeIn(500);
			$(".fabric:not(.Green)").fadeOut(0);
			$("#Green").addClass("active");
		});
		$("#Purple").click(function() {
			$(".Purple").fadeIn(500);
			$(".fabric:not(.Purple)").fadeOut(0);
			$("#Purple").addClass("active");
		});
		$("#Gray").click(function() {
			$(".Grey").fadeIn(500);
			$(".fabric:not(.Grey)").fadeOut(0);
			$("#Gray").addClass("active");
		});
		$("#Red").click(function() {
			$(".Red").fadeIn(500);
			$(".fabric:not(.Red)").fadeOut(0);
			$("#Red").addClass("active");
		});
		$("#Beige").click(function() {
			$(".Beige").fadeIn(500);
			$(".fabric:not(.Beige)").fadeOut(0);
			$("#Beige").addClass("active");
		});
		$("#Beige1").click(function() {
			$(".Beige").fadeIn(500);
			$(".fabric:not(.Beige)").fadeOut(0);
			$("#Beige1").addClass("active");
		});
		$("#Navy").click(function() {
			$(".Navy").fadeIn(500);
			$(".fabric:not(.Navy)").fadeOut(0);
			$("#Navy").addClass("active");
		});
		$("#Pink").click(function() {
			$(".Pink").fadeIn(500);
			$(".fabric:not(.Pink)").fadeOut(0);
			$("#Pink").addClass("active");
		});
		$("#Yellow").click(function() {
			$(".Yellow").fadeIn(500);
			$(".fabric:not(.Yellow)").fadeOut(0);
			$("#Yellow").addClass("active");
		});
		$("#solid").click(function() {
			$(".solid").fadeIn(500);
			$(".fabric:not(.solid)").fadeOut(0);
			$("#solid").addClass("active");
		});
		$("#check").click(function() {
			$(".check").fadeIn(500);
			$(".fabric:not(.check)").fadeOut(0);
			$("#check").addClass("active");
		});
		$("#print").click(function() {
			$(".print").fadeIn(500);
			$(".fabric:not(.print)").fadeOut(0);
			$("#print").addClass("active");
		});
		$("#stripe").click(function() {
			$(".stripe").fadeIn(500);
			$(".elbowfabric:not(.stripe)").fadeOut(0);
			$("#stripe").addClass("active");
		});
		$("#solid1").click(function() {
			$(".solid").fadeIn(500);
			$(".elbowfabric:not(.solid)").fadeOut(0);
			$("#solid1").addClass("active");
		});
		$("#check1").click(function() {
			$(".check").fadeIn(500);
			$(".elbowfabric:not(.check)").fadeOut(0);
			$("#check1").addClass("active");
		});
		$("#print1").click(function() {
			$(".print").fadeIn(500);
			$(".elbowfabric:not(.print)").fadeOut(0);
			$("#print1").addClass("active");
		});
		$("#stripe1").click(function() {
			$(".stripe").fadeIn(500);
			$(".elbowfabric:not(.stripe)").fadeOut(0);
			$("#stripe1").addClass("active");
		});
	});

//filters contrast fabric
	$( document ).ready(function() {
		$("#all1").click(function() {
			$(".elbowfabric").fadeIn(500);
			$("#all1").addClass("active");
		});
		$("#Black1").click(function() {
			$(".Black").fadeIn(500);
			$(".elbowfabric:not(.Black)").fadeOut(0);
			$("#Black1").addClass("active");
		});
		$("#Blue1").click(function() {
			$(".Blue").fadeIn(500);
			$(".elbowfabric:not(.Blue)").fadeOut(0);
		$("#Blue1").addClass("active");
		});
		$("#Brown1").click(function() {
			$(".Brown").fadeIn(500);
			$(".elbowfabric:not(.Brown)").fadeOut(0);
			$("#Brown1").addClass("active");
		});
		$("#White1").click(function() {
			$(".White").fadeIn(500);
			$(".elbowfabric:not(.White)").fadeOut(0);
			$("#White1").addClass("active");
		});
		$("#Green1").click(function() {
			$(".Green").fadeIn(500);
			$(".elbowfabric:not(.Green)").fadeOut(0);
			$("#Green1").addClass("active");
		});
		$("#Purple1").click(function() {
			$(".Purple").fadeIn(500);
			$(".elbowfabric:not(.Purple)").fadeOut(0);
			$("#Purple1").addClass("active");
		});
		$("#Gray1").click(function() {
			$(".Gray").fadeIn(500);
			$(".elbowfabric:not(.Gray)").fadeOut(0);
			$("#Gray1").addClass("active");
		});
		$("#Red1").click(function() {
			$(".Red").fadeIn(500);
			$(".elbowfabric:not(.Red)").fadeOut(0);
			$("#Red1").addClass("active");
		});
		$("#Navy1").click(function() {
			$(".Navy").fadeIn(500);
			$(".elbowfabric:not(.Navy)").fadeOut(0);
			$("#Navy1").addClass("active");
		});
		$("#Pink1").click(function() {
			$(".Pink").fadeIn(500);
			$(".elbowfabric:not(.Pink)").fadeOut(0);
			$("#Pink1").addClass("active");
		});
		$("#Yellow1").click(function() {
			$(".Yellow").fadeIn(500);
			$(".elbowfabric:not(.Yellow)").fadeOut(0);
			$("#Yellow1").addClass("active");
		});
	});

	function filter() {
        $(".filters").show(500);
    }
//hide filter
	function filter1() {
        $(".filters").hide(500);
    }
//On load hide 
    function hideDiv() { 
		document.getElementById("elbow-fabric-icon-div").style.pointerEvents = 'none';
		document.getElementById("elbow-fabric-icon-div").style.color = "blue";		
		document.getElementById("filte").style.display = 'none';	
		document.getElementById("filte1").style.display = 'none';				
	}
//Show Elbow Fabric
	$(document).ready(function() {
        $("#MS-Elbow_Patch").click(function() {
			document.getElementById("elbow-fabric-icon-div").style.pointerEvents = 'auto'; 
			document.getElementById("elbow-fabric-icon-div").style.color = "black";
        });
    });
//hide elbow fabric		
	$(document).ready(function() {
        $("#MS-Elbow").click(function() {
			document.getElementById("elbow-fabric-icon-div").style.pointerEvents = 'none';
			document.getElementById("elbow-fabric-icon-div").style.color = "blue";	
        });
    });

    function closePanel() {
        $(".mol_panel").css("display","none");
     }
//mo panel 1	
	function showPanel(id) {
		$(".mol_panel").css("display","none");
		$("#" + id).css("display","block");
		if(id == "panel_regular_button")
			{
				var eleid = $(".fit.active").attr("id");
				if(eleid == "MS-B-Panel_R-fit")
				{
				$(".reg_button").show();
				$(".slim_button").hide();
				}
				if(eleid == "MS-B-Panel_S-fit")
				{
				$(".reg_button").hide();
				$(".slim_button").show();
				}
		}
		else if(id =="panel_lapel_1")
		{
		 var eleid = $(".Breasted1.active").attr("id");
		 if(eleid == "MS-F-Panel_R-fit_One")
		 {
		      $(".but_one_lapel").show();
			  $(".but_two_lapel").hide();
			  $(".but_three_lapel").hide();
			  $(".but_four_lapel").hide();
		 }
		 if(eleid == "MS-F-Panel_R-fit_Two")
		 {
		      $(".but_one_lapel").hide();
			  $(".but_two_lapel").show();
			  $(".but_three_lapel").hide();
			  $(".but_four_lapel").hide();
		 }
		 if(eleid == "MS-F-Panel_R-fit_Three")
		 {
		      $(".but_one_lapel").hide();
			  $(".but_two_lapel").hide();
			  $(".but_three_lapel").show();
			  $(".but_four_lapel").hide();
		 }
		 if(eleid == "MS-F-Panel_R-fit_Four")
		 {
		      $(".but_one_lapel").hide();
			  $(".but_two_lapel").hide();
			  $(".but_three_lapel").hide();
			  $(".but_four_lapel").show();
		 } 
		 if(eleid == "MS-F-Panel_S-fit_One")
		 {
		      $(".but_one_lapel").show();
			  $(".but_two_lapel").hide();
			  $(".but_three_lapel").hide();
			  $(".but_four_lapel").hide();
		 }
		 if(eleid == "MS-F-Panel_S-fit_Two")
		 {
		      $(".but_one_lapel").hide();
			  $(".but_two_lapel").show();
			  $(".but_three_lapel").hide();
			  $(".but_four_lapel").hide();
		 }
		 if(eleid == "MS-F-Panel_S-fit_Three")
		 {
		      $(".but_one_lapel").hide();
			  $(".but_two_lapel").hide();
			  $(".but_three_lapel").show();
			  $(".but_four_lapel").hide();
		 }
		 if(eleid == "MS-F-Panel_S-fit_Four")
		 {
		      $(".but_one_lapel").hide();
			  $(".but_two_lapel").hide();
			  $(".but_three_lapel").hide();
			  $(".but_four_lapel").show();
		 } 
		}
		else if(id == "panel_ticket")
		{
	    	var eleid = $(".fit.active").attr("id");
			if(eleid == "MS-B-Panel_R-fit")
			{
		      $(".regular_ticket").show();
			  $(".slim_ticket").hide();
			 
			}
		if(eleid == "MS-B-Panel_S-fit")
		{
		      $(".regular_ticket").hide();
			  $(".slim_ticket").show();
			  
		}
		}
		else if(id == "panel_regular_vent")
		{
	    	var eleid = $(".fit.active").attr("id");
			if(eleid == "MS-B-Panel_R-fit")
			{
		      $(".reg_vent").show();
			  $(".slim_vent").hide();
			 
			}
		if(eleid == "MS-B-Panel_S-fit")
		{
		      $(".reg_vent").hide();
			  $(".slim_vent").show();
			  
		}
		}
	}
        //toggle visibility
        var divs = ["panel_fabric","panel_fit","panel_regular_button","panel_ticket","panel_elbow_fabric", "panel_lapel_1","panel_sleeve","panel_stitch", "panel_pocket", "panel_regular_vent","panel_lining","panel_button_color","panel_thread","panel_elbow"];
        var visibleDivId = null;
        function toggleVisibility(divId) {
        if(visibleDivId === divId) {
            visibleDivId = null;
            } else {
            visibleDivId = divId;
        }
        hideNonVisibleDivs();
        }
        function hideNonVisibleDivs() {
        var i, divId, div;
        for(i = 0; i < divs.length; i++) {
        divId = divs[i];
        div = document.getElementById(divId);
        if(visibleDivId === divId) {
          div.style.display = "block";
        } else {
          div.style.display = "none";
        }
      }
    }

	
		   $(function(){
	$( ".content_inner" ).click(function() {
		  var container_title = $(this).attr("title");
		  var title = container_title.split("|");
		  var container = title[0];
		  var image = title[1];
		  var src = "images/"+container+"/"+image;
		  /* 
		  alert(title);
		  alert(container);
		  alert(image);
		  alert(src);
		   */
		  $("."+container).removeClass("active");
		  $("#img_"+container).attr("src",src);
		  $(this).addClass("active");
		  	});
});

$(function(){
	      $( ".content_inner1" ).click(function() {
		  var container_title = $(this).attr("title");
		  var title = container_title.split("|");
		  var container = title[0];
		  var image = title[1];
		  var src = "images/"+container+"/"+image;
		  /* 
		  alert(title);
		  alert(container);
		  alert(image);
		  alert(src);
		   */
		  $("."+container).removeClass("active");
		  $("#img_"+container).attr("src",src);
		  $(this).addClass("active");
		  	});
});