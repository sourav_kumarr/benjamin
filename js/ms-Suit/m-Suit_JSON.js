var sctm =[
"MS-Yoke.ctm",
"MS-B-Panel_R-fit_WithSingleVent.ctm",
"MS-B-Panel_R-fit_WithSingleVent_Li.ctm",
"MS-F-Panel_R-fit_One.ctm",
"MS-F-Panel_R-fit_One_B.ctm",
"MS-F-Panel_R-fit_One_H.ctm",
"MS-F-Panel_R-fit_One_Li.ctm",
"MS-Lap_Std_One.ctm",
"MS-Lap_Std_One_T.ctm",
"MS-Poc_Welt.ctm",
"MS-Sleeve.ctm",
"MS-Sleeve_Li.ctm",
"MS-SleeveBtn_Four_B.ctm",
"MS-SleeveBtn_Four_B_H.ctm",
"MS-S-Panel_R-fit.ctm",
"MS-S-Panel_R-fit_Li.ctm"
	];
var lateCtm =[

"MS-TKPoc_R.ctm",
"MS-TKPoc_S.ctm",
"MS-B-Panel_R-fit_WithDoubleVent.ctm",
"MS-B-Panel_R-fit_WithDoubleVent_Li.ctm",
"MS-B-Panel_R-fit_WithOutVent.ctm",
"MS-B-Panel_R-fit_WithOutVent_Li.ctm",

"MS-S-Panel_S-fit.ctm",
"MS-S-Panel_S-fit_Li.ctm",


"MS-B-Panel_S-fit_WithDoubleVent.ctm",
"MS-B-Panel_S-fit_WithDoubleVent_Li.ctm",
"MS-B-Panel_S-fit_WithOutVent.ctm",
"MS-B-Panel_S-fit_WithOutVent_Li.ctm",
"MS-B-Panel_S-fit_WithSingleVent.ctm",
"MS-B-Panel_S-fit_WithSingleVent_Li.ctm",

"MS-Elbow_Patch.ctm",
"MS-Elbow_Patch_T.ctm",

"MS-F-Panel_S-fit_One.ctm",
"MS-F-Panel_S-fit_One_B.ctm",
"MS-F-Panel_S-fit_One_H.ctm",
"MS-F-Panel_S-fit_One_Li.ctm",

"MS-F-Panel_S-fit_Two.ctm",
"MS-F-Panel_S-fit_Two_B.ctm",
"MS-F-Panel_S-fit_Two_H.ctm",
"MS-F-Panel_S-fit_Two_Li.ctm",

"MS-F-Panel_S-fit_Three.ctm",
"MS-F-Panel_S-fit_Three_B.ctm",
"MS-F-Panel_S-fit_Three_H.ctm",
"MS-F-Panel_S-fit_Three_Li.ctm",

"MS-F-Panel_S-fit_Four.ctm",
"MS-F-Panel_S-fit_Four_B.ctm",
"MS-F-Panel_S-fit_Four_H.ctm",
"MS-F-Panel_S-fit_Four_Li.ctm",


"MS-F-Panel_R-fit_Two.ctm",
"MS-F-Panel_R-fit_Two_B.ctm",
"MS-F-Panel_R-fit_Two_H.ctm",
"MS-F-Panel_R-fit_Two_Li.ctm",

"MS-F-Panel_R-fit_Three.ctm",
"MS-F-Panel_R-fit_Three_B.ctm",
"MS-F-Panel_R-fit_Three_H.ctm",
"MS-F-Panel_R-fit_Three_Li.ctm",

"MS-F-Panel_R-fit_Four.ctm",
"MS-F-Panel_R-fit_Four_B.ctm",
"MS-F-Panel_R-fit_Four_H.ctm",
"MS-F-Panel_R-fit_Four_Li.ctm",

"MS-Lap_Peak_One.ctm",
"MS-Lap_Peak_One_T.ctm",

"MS-Lap_Shawl_One.ctm",
"MS-Lap_Shawl_One_T.ctm",
"MS-Lap_Slim_One.ctm",
"MS-Lap_Slim_One_T.ctm",

"MS-Lap_Peak_Two.ctm",
"MS-Lap_Peak_Two_T.ctm",
"MS-Lap_Std_Two.ctm",
"MS-Lap_Std_Two_T.ctm",
"MS-Lap_Shawl_Two.ctm",
"MS-Lap_Shawl_Two_T.ctm",
"MS-Lap_Slim_Two.ctm",
"MS-Lap_Slim_Two_T.ctm",

"MS-Lap_Peak_Three.ctm",
"MS-Lap_Peak_Three_T.ctm",
"MS-Lap_Std_Three.ctm",
"MS-Lap_Std_Three_T.ctm",
"MS-Lap_Shawl_Three.ctm",
"MS-Lap_Shawl_Three_T.ctm",
"MS-Lap_Slim_Three.ctm",
"MS-Lap_Slim_Three_T.ctm",

"MS-Lap_Peak_Four.ctm",
"MS-Lap_Peak_Four_T.ctm",
"MS-Lap_Std_Four.ctm",
"MS-Lap_Std_Four_T.ctm",
"MS-Lap_Shawl_Four.ctm",
"MS-Lap_Shawl_Four_T.ctm",
"MS-Lap_Slim_Four.ctm",
"MS-Lap_Slim_Four_T.ctm",
"MS-Poc_Flap.ctm",
"MS-SleeveBtn_Three_B.ctm",
"MS-SleeveBtn_Three_B_H.ctm"


];
			
var texture =["AR 2231 01 600 all wool 260G.jpg",
			  "AR 2235 05 580 73 wool 27 mohair 290 gr.jpg",
			  "AR 2251 01 600 all wool 260G.jpg",
			  "AR 2251 02 600 all wool 260g.jpg",
			  "AR 2251 16 600 all wool 260gr.jpg",
			  "AR 2251 17 600 all wool 260gr.jpg",
			  "AR 2251 49 600 all wool 260gr.jpg",
			  "AR 2251 50 600 all wool 260g.jpg",
			  "AR 2251 55 600 all wool 260gr.jpg",
			  "AR 2251 58 600 all wool 260gr.jpg",
			  "AR 2251 59 600 all wool 260gr.jpg",
			  "AR 2251 60 600 all wool 260gr.jpg",
			  "AR 2258 74 560 sunny season.jpg",
			  "AR 2258 75 560 sunny season.jpg"
			  ];


var xCod =["42.04","25.22","43.63","43.63","62.71","21.05","280.4","118.7","331.4","331.4","158.5","158.5","71.89","71.89","36"];

var yCod =["36.05","20.85","34.44","34.44","50.04","20.61","255.2","85.36","349.6","349.6","159.5","159.5","70.9","70.9","36"];