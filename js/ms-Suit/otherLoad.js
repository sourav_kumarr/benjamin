var check,ind =0;
function otherLoad(){
	//console.log("Loading.....");
	check = lateCtm.length;
	(function(){		
		 function load(){
			 if ( ind < check){
					 var name = lateCtm[ind].substring(0,lateCtm[ind].lastIndexOf('.'));
					 loader.load("ctm/"+lateCtm[ind], function(geometry){							
						 var c = name.split("_");
							 if( c[c.length-1] == "B" ){				 
								callbackModel( geometry, .03, button_material, 0, -43, 0,  1,name,false );						
						 }
						 else if( c[c.length-1] == "H" || c[c.length-1] == "T"  ){
							callbackModel( geometry, .03, thread_material, 0, -43, 0,  1,name,false );				
										
						 }
						 else if(c[c.length-1] == "Li"){
							 callbackModel( geometry, .03, lining_material, 0, -43, 0,  1,name,false );						
					}		
					else if(c[c.length-1] == "Patch"){
							elbow_material = new THREE.MeshPhongMaterial( { map:texArray[0],
												   combine: THREE.MixOperation,
												   specular: 0xBABABA,
												   shininess: .5,
												   side: THREE.DoubleSide,
												 });
									elbow_material.map.wrapS = elbow_material.map.wrapT = THREE.RepeatWrapping;
									elbow_material.map.repeat.set( forX[0],forY[0] );	
									callbackModel( geometry, .03, elbow_material, 0, -43, 0,  1,name,false );
									
					}						
						 else{					
										 material = new THREE.MeshPhongMaterial( { map:texArray[0],
												   combine: THREE.MixOperation,
												   specular: 0xBABABA,
												   shininess: .5,
												   side: THREE.DoubleSide,
												 });
								material.map.wrapS = material.map.wrapT = THREE.RepeatWrapping;
								material.map.repeat.set( forX[0],forY[0] );	
								callbackModel( geometry, .03, material, 0, -43, 0, 1,name,false );							
					}	 
						
 		  ++ind;
 		  load(); // for looping the function
},{ useWorker: true });
				}
	   	
    }

    load();

})();

}