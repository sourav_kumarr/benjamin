// FOR MESH CHANGE
function meshChange(getId){
	switch (getId){
		case "MS-F-Panel_R-fit_One":
			selectedbutton = "one";
			break;
        case "MS-F-Panel_R-fit_Two":
            selectedbutton = "two";
            break;
        case "MS-F-Panel_R-fit_Three":
            selectedbutton = "three";
            break;
        case "MS-F-Panel_R-fit_Four":
            selectedbutton = "double_breasted";
            break;
        case "MS-F-Panel_S-fit_One":
            selectedbutton = "one";
            break;
        case "MS-F-Panel_S-fit_Two":
            selectedbutton = "two";
            break;
        case "MS-F-Panel_S-fit_Three":
            selectedbutton = "three";
            break;
        case "MS-F-Panel_S-fit_Four":
            selectedbutton = "double_breasted";
            break;
		case "MS-Lap_Std_One":
        	selectedlapel = "standard";
        break;
        case "MS-Lap_Peak_One":
            selectedlapel = "peak";
            break;
        case "MS-Lap_Slim_One":
            selectedlapel = "slim";
            break;
        case "MS-Lap_Shawl_One":
            selectedlapel = "shawl";
            break;
        case "MS-Lap_Std_Two":
            selectedlapel = "standard";
            break;
        case "MS-Lap_Peak_Two":
            selectedlapel = "peak";
            break;
        case "MS-Lap_Slim_Two":
            selectedlapel = "slim";
            break;
        case "MS-Lap_Shawl_Two":
            selectedlapel = "shawl";
            break;
        case "MS-Lap_Std_Three":
            selectedlapel = "standard";
            break;
        case "MS-Lap_Peak_Three":
            selectedlapel = "peak";
            break;
        case "MS-Lap_Slim_Three":
            selectedlapel = "slim";
            break;
        case "MS-Lap_Shawl_Three":
            selectedlapel = "shawl";
            break;
        case "MS-Lap_Std_Four":
            selectedlapel = "standard";
            break;
        case "MS-Lap_Peak_Four":
            selectedlapel = "peak";
            break;
        case "MS-Lap_Slim_Four":
            selectedlapel = "slim";
            break;
        case "MS-Lap_Shawl_Four":
            selectedlapel = "shawl";
            break;

	}
	var a = getId;	
	var q = a.split("_");
	var thrd,btn,hole,lining,lapel,lapel_T;	
		scene.traverse( function (menBlazer_mesh) {
          if ( menBlazer_mesh instanceof THREE.Mesh ) {			  
			  if(menBlazer_mesh.visible){	
					var n = menBlazer_mesh.name.split('_');
						if(n[0] == q[0] || n[1] == "Std" || n[1] == "Peak" ||n[1] == "Slim"||n[1] == "Shawl"){	
								menBlazer_mesh.visible = false;								
								}
							}							
						if(menBlazer_mesh.name == a  ){								
								menBlazer_mesh.visible = true;
								if(q[0]=="MS-F-Panel"){
								var l = menBlazer_mesh.name.split('_');								
								if(q[q.length-1] == l[l.length-1]){
									lapel = "MS-Lap_Std"+'_'+l[l.length-1];
									lapel_T = lapel+'_'+"T";
									}
								}								
									if(menBlazer_mesh.visible){									
											var j = menBlazer_mesh.name.split('_');
												if(j[1] == q[1]){
													thrd = a+'_'+"T";	
													btn = a+'_'+"B";
													hole = a+'_'+"H";
													lining = a+'_'+"Li";													
										}
									}
								}
									if(lapel == menBlazer_mesh.name || lapel_T == menBlazer_mesh.name||thrd == menBlazer_mesh.name 
									|| btn == menBlazer_mesh.name || hole == menBlazer_mesh.name|| lining == menBlazer_mesh.name ){
										menBlazer_mesh.visible = true;	
									}						
							}							
			});
}
//FOR POCKET CHANGE
function pocketChange(getId){
	switch(getId){
		case "MS-Poc_Welt":
			selectedpocket = "welt";
			break;
        case "MS-Poc_Flap":
            selectedpocket = "flap";
            break;
	}
	var a = getId;
	var q =a.split('_');
		scene.traverse( function (menBlazer_mesh) {
			if(menBlazer_mesh instanceof THREE.Mesh){
				if(menBlazer_mesh.visible){
					var n = menBlazer_mesh.name.split('_');
						if(q[0]==n[0]){
							menBlazer_mesh.visible = false;	
						}
				}
				if(menBlazer_mesh.name == a){
					menBlazer_mesh.visible=true;
				}
			}
		});	
}
//FOR BUTTON MESH CHANGE
function btnMeshChange(getId){
	switch(getId){
		case "MS-SleeveBtn_Four_B":
			selectedbutton = "four";
			break;
        case "MS-SleeveBtn_Three_B":
            selectedbutton = "three";
            break;
        case "MS-SleeveBtn":
            selectedbutton = "no";
            break;
	}
	var a = getId;
	var q = a.split("_");
	var thrd="";
	scene.traverse( function (menBlazer_mesh) {
		if ( menBlazer_mesh instanceof THREE.Mesh ) {
			if(menBlazer_mesh.visible){
				var n = menBlazer_mesh.name.split('_');
					if(q[0]==n[0]){
						menBlazer_mesh.visible = false;	
						}
					}
					if(menBlazer_mesh.name == a){
						menBlazer_mesh.visible = true;
						if(menBlazer_mesh.visible){									
								var j = menBlazer_mesh.name.split('_');
									if(j[1][2] == q[1][2]){
										thrd = a+'_'+"H";	
										}
									}
								}
					if(thrd == menBlazer_mesh.name){
						menBlazer_mesh.visible=true;
					}
		}		
	});
}
// FOR REMVOVE THREAD 
function removeThread(getId){
	var a = getId;
		scene.traverse( function (menBlazer_mesh) {
			if ( menBlazer_mesh instanceof THREE.Mesh ) {
					if(menBlazer_mesh.visible){
						var n = menBlazer_mesh.name.split('_');
							if(n[n.length-1] == "T"){
								menBlazer_mesh.visible =false;
							}
					}							
				}
		});
}
// FOR ADD THREAD
function addThread(getId){
	switch(getId){
		case "MS-Lap_Std-One":
			selectedlapelstich = "yes";
			break;
        case "MS-Lap_Std-One_T":
            selectedlapelstich = "no";
            break;
	}
	var a = getId;
	var g = "";
	scene.traverse( function (menBlazer_mesh) {
			if ( menBlazer_mesh instanceof THREE.Mesh ) {
					if(menBlazer_mesh.visible){
						var n = menBlazer_mesh.name;
							if(n == "MS-Lap_Peak_One" || n == "MS-Lap_Std_One" || n == "MS-Lap_Shawl_One" || n =="MS-Lap_Slim_One"
							|| n == "MS-Lap_Peak_Two" || n == "MS-Lap_Std_Two" || n == "MS-Lap_Shawl_Two" || n =="MS-Lap_Slim_Two"
							|| n == "MS-Lap_Peak_Three" || n == "MS-Lap_Std_Three" || n == "MS-Lap_Shawl_Three" || n =="MS-Lap_Slim_Three"
							|| n == "MS-Lap_Peak_Four" || n == "MS-Lap_Std_Four" || n == "MS-Lap_Shawl_Four" || n == "MS-Lap_Slim_Four"){
								g = n+'_'+'T';
							}							
					}
					if(menBlazer_mesh.name == g){
								menBlazer_mesh.visible =true;
						}
			}
	});
}
// FOR FIT CHANGE
function fitMeshChange(getId){
	switch(getId){
		case "MS-B-Panel_R-fit":
			selectedfit = "regular";
			break;
		case "MS-B-Panel_S-fit":
			selectedfit = "slim";
			break;
	}
	var f = getId;
	var q = f.split("_");
	var vent = "";
	var B_lining="";
	var fit="";
	var btn = "";
	var hole ="";
	var F_lining="";
	var side_vent="";
	var side_lining="";
	var lapel="";
	var lapel_T="";
			scene.traverse( function (menBlazer_mesh) {
			if ( menBlazer_mesh instanceof THREE.Mesh ) {
					if(menBlazer_mesh.visible){	
					var n = menBlazer_mesh.name.split('_');
						if(n[1] == "S-fit" || n[1] == "R-fit"||n[0] == "MS-Lap" || n[0] == "MS-TKPoc"){						
								menBlazer_mesh.visible = false;								
								}								
							
					}								
									if(q[1] == "R-fit"||q[1] == "S-fit"){
										vent = "MS-B-Panel"+'_'+q[1]+'_'+"WithSingleVent";
										B_lining = vent+'_'+"Li";
										fit = "MS-F-Panel"+'_'+q[1]+'_'+"One";
										btn = fit+'_'+"B";
										hole = fit+'_'+"H";
										F_lining =fit+'_'+"Li";
										side_vent = "MS-S-Panel"+'_'+q[1];
										side_lining = side_vent+'_'+"Li";
										lapel = "MS-Lap"+"_"+"Std"+"_"+"One";
										lapel_T = lapel+"_"+"T";
										
										}																			
							if(menBlazer_mesh.name == vent||menBlazer_mesh.name == B_lining||menBlazer_mesh.name == fit||menBlazer_mesh.name == btn
							||menBlazer_mesh.name == hole||menBlazer_mesh.name == F_lining ||menBlazer_mesh.name == side_vent||menBlazer_mesh.name == side_lining
							||menBlazer_mesh.name == lapel||menBlazer_mesh.name == lapel_T){
								
								menBlazer_mesh.visible=true;
								}							
				}
		});
		
}
//FOR BACK PANEL VENT
function ventChange(getId){
	switch(getId){
		case "MS-B-Panel_R-fit_WithSingleVent":
			selectedvent = "single";
			break;
		case "MS-B-Panel_R-fit_WithOutVent":
			selectedvent = "no";
			break;
		case "MS-B-Panel_R-fit_WithDoubleVent":
			selectedvent = "double";
			break;
        case "MS-B-Panel_S-fit_WithSingleVent":
            selectedvent = "single";
            break;
        case "MS-B-Panel_S-fit_WithOutVent":
            selectedvent = "no";
            break;
        case "MS-B-Panel_S-fit_WithDoubleVent":
            selectedvent = "double";
            break;
	}
	var a = getId;
	var q = a.split('_');
	var Li ="";
	scene.traverse( function (menBlazer_mesh) {
		if ( menBlazer_mesh instanceof THREE.Mesh ) {
			if(menBlazer_mesh.visible){
				var n = menBlazer_mesh.name.split('_');
					if(q[0]==n[0]){
						menBlazer_mesh.visible = false;
					}
			}
			if(menBlazer_mesh.name == a){
				menBlazer_mesh.visible=true;
				if(menBlazer_mesh.visible){									
					var j = menBlazer_mesh.name.split('_');
						if(j[j.length-1] == q[q.length-1]){
							Li = a+'_'+"Li";
						}
					}
			}
			if(Li == menBlazer_mesh.name){
				
				menBlazer_mesh.visible=true;
			}
		}
	});
}
//FOR ELBOW CHANGE
function elbowChange(getId){
	switch(getId){
		case "MS-Elbow":
			selectedelbowpatch = "no";
			break;
        case "MS-Elbow_Patch":
            selectedelbowpatch = "yes";
            break;
	}
	var a = getId;
	var q = a.split('_');
	var Thrd;
	scene.traverse( function (menBlazer_mesh) {
		if ( menBlazer_mesh instanceof THREE.Mesh ) {
			if(menBlazer_mesh.visible){
				var n = menBlazer_mesh.name.split('_');
				if(q[0] == n[0]){
					menBlazer_mesh.visible = false;
				}
			}
			if(menBlazer_mesh.name == a){
				menBlazer_mesh.visible=true;
				if(menBlazer_mesh.visible){									
					var j = menBlazer_mesh.name.split('_');
						if(j[1][2] == q[1][2]){
							Thrd = a+'_'+"T";	
								}
									}
			}
			if(menBlazer_mesh.name == Thrd){
				menBlazer_mesh.visible=true;
				
			}
		}
	});
}
//TICKET POCKET CHANGE
function ticketPocChange(getId){
	switch(getId){
		case "MS-TKPoc":
			selectedticketpocket = "no";
			break;
		case "MS-TKPoc_R":
			selectedticketpocket = "yes";
			break;
		case "MS-TKPoc_S":
			selectedticketpocket = "yes";
			break;
	}
	var a = getId;
	var q = a.split('_');
		scene.traverse( function (menBlazer_mesh) {
			if ( menBlazer_mesh instanceof THREE.Mesh ) {
			if(menBlazer_mesh.visible){
				var n = menBlazer_mesh.name.split('_');
				if(q[0] == n[0]){
					menBlazer_mesh.visible = false;
				}
			}
			if(menBlazer_mesh.name == a){
				menBlazer_mesh.visible=true;
				console.log(menBlazer_mesh.name);
			}
			}
		});
}