function camerAnim(getId){
	switch(getId){
		case "collar-icon-div":
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:-3,z:0,y:9,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:6,z:14,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
		case "cuff-icon-div":
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:8,z:3,y:-11,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:5,z:-15,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
		case "fabric-icon-div":
						controls.target = new THREE.Vector3(-2, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:0,z:0,y:0,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:-8,z:15,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
		case "placket-icon-div":
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:0,z:0,y:4,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:-5,z:25,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
		case "button-icon-div":
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:-1,z:0,y:6,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:-6,z:17,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
		case "thread-icon-div":
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:-3,z:0,y:8,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:6,z:12,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
		case "fit-icon-div":
		
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:0,z:0,y:0,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:30,z:50,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
		case "pocket-icon-div":
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:0,z:0,y:5,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:0,z:30,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
		case "elbow-icon-div":
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:0,z:0,y:-3,onUpdate:function(){
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:-30,z:-30,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
				break;
		case "sleeve-icon-div":
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:0,z:0,y:-4,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:20,z:33,y:-1,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
		case "bottom-icon-div":
						controls.target = new THREE.Vector3(0, 0, 0);
						var a= cube.position.x;
						var b = cube.position.z;
						var c = cube.position.y;
						TweenMax.to(cube.position,2,{x:0,z:0,y:-5,onUpdate:function(){
						
						}});
					var x = camera.position.x;
					var z = camera.position.z;
					var y = camera.position.y;
					TweenMax.to(camera.position,2,{x:-40,z:-25,y:0,onUpdate:function(){
					camera.updateProjectionMatrix();
					camera.lookAt(scene.position);									
				}});
		break;
	}
	
}