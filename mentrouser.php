<?php
include("header1.php");
//if($user_id == ""){}

if(!isset($_SESSION['benj_user_id'])){
    echo "<input type='hidden' value='' id='user_id' />";
}
else{
    echo "<input type='hidden' value='".$_SESSION['benj_user_id']."' id='user_id' />";

}
?>
    <head>
        <link rel="stylesheet" href="css/shirtstyle.css">
        <link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="js/libs/jquery-1.11.3.js"></script>
        <script src="js/libs/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/ms-SuitTrou/Men_pant-script.js"></script>
        <script src="js/libs/jquery-ui.min.js"></script>
        <style>
            .progdiv {
                left: 40%;
                position: absolute;
                top: 50%;
                width: 30%;
                display:none;
            }
            .pload{
                left: 40%;
                position: absolute;
                top: 46%;
                width: 30%;
                display:none;
                text-align: center;
            }
            .cartbtn {
                position: fixed;
                right: 10px;
                top: 300px;
                width: 200px;
                z-index: 999999;
                display: none;
            }
        </style>
    </head>
    <body onload="hideDiv();">
    <div id="main-container">
        <div id="left_container" class="left-container">
            <div id="fit-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_fit');toggleVisibility('panel_fit');">
                <img id="img_fit"   src="images2/fit/regular.svg"/>
                <div class="left_font">Fit</div>
            </div>
            <div id="fabric-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_fabric');toggleVisibility('panel_fabric');">
                <img id="img_fabric"   src="images2/fabric/AR 2231 01 600 all wool 260G.jpg"/>
                <div class="left_font">Fabric</div>
            </div>
            <div id="side-pocket-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_side_pocket');toggleVisibility('panel_side_pocket');">
                <img id="img_Side_pocket"   src="images2/Side_pocket/Side-Pocket-Beveled.svg"/>
                <div class="left_font">Side Pocket</div>
            </div>
            <div id="pocket-fabric-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_pocket_fabric');toggleVisibility('panel_pocket_fabric');">
                <img id="img_pocket_fabric"   src="images2/pocket_fabric/Pocket-Lining-Blue.png"/>
                <div class="left_font">Pocket Fabric</div>
            </div>
            <div id="buttoning-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_buttoning');toggleVisibility('panel_buttoning');">
                <img id="img_Buttoning"   src="images2/Buttoning/Visible-Button.svg"/>
                <div class="left_font">Buttoning</div>
            </div>
            <div id="back-pocket-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_back_pocket');toggleVisibility('panel_back_pocket');">
                <img id="img_Back_pocket"   src="images2/Back_pocket/Back-Pocket-Placement.svg"/>
                <div class="left_font">Placement</div>
            </div>
            <div id="pocket_design-button-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_pocket_design');toggleVisibility('panel_pocket_design');">
                <img id="img_Back_Detail"  src="images2/Back_Detail/02a_Single_Welt_Pocket.svg"/>
                <div class="left_font">Pocket Design</div>
            </div>
            <div id="button-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_button');toggleVisibility('panel_button');">
                <img id="img_button"   src="images2/button/Sorrento-white.png"/>
                <div class="left_font">Button</div>
            </div>
            <div id="thread-icon-div" class="left_container_img " onclick="camerAnim(this.id);showPanel('panel_thread');toggleVisibility('panel_thread');">
                <img id="img_thread" style="width:50px; height:70px;" src="images2/thread/White.png"/>
                <div class="left_font">Thread</div>
            </div>
            <div id="waist_band-icon-div" class="left_container_img" onclick="camerAnim(this.id);showPanel('panel_waist_band');toggleVisibility('panel_waist_band');">
                <img id="img_Waistband"   src="images2/Waistband/Waistband-Black-(Default).png" />
                <div class="left_font">Waist Band</div>
            </div>
        </div>
        <p class="pload">Please Wait ... Object Is Loading</p>
        <div class="progress progdiv">
            <div id="prog" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40"
                 aria-valuemin="0" aria-valuemax="100" style="width:0%">0%</div>
        </div>
        <!--End Of Left Container -->
    </div><!--End Of Main Container -->
    <!--Start of Fabric -->
    <div id="panel_fabric" class="panel mol_panel" style="background-color:transparent;" >
        <div class="heading" style="background-color:transparent;">
            <div class="title">Fabric</div>
            <img id="close" class="close" src="images2/close.png" onclick="camerAnim(this.id);closePanel();closeNav();"/>
        </div>
        <div class="content" style="overflow-y:scroll;background-color:transparent;">
            <div id="fabric_0" onclick="textureChange(this.id);" class="content_inner Navy fabric stripe active" title="fabric|AR 2231 01 600 all wool 260G.jpg">
                <img src="images2/fabric/AR 2231 01 600 all wool 260G.jpg"  />
                <div>Navy Stripe</div>
            </div>
            <div id="fabric_1" onclick="textureChange(this.id);" class="content_inner Black stripe fabric " title="fabric|AR 2235 05 580 73 wool 27 mohair 290 gr.jpg">
                <img src="images2/fabric/AR 2235 05 580 73 wool 27 mohair 290 gr.jpg"  />
                <div>Black Stripe</div>
            </div>
            <div id="fabric_2" onclick="textureChange(this.id);" class="content_inner Blue stripe fabric " title="fabric|AR 2251 01 600 all wool 260G.jpg">
                <img src="images2/fabric/AR 2251 01 600 all wool 260G.jpg"  />
                <div>Blue Stripe</div>
            </div>
            <div id="fabric_3" onclick="textureChange(this.id);" class="content_inner Grey stripe fabric " title="fabric|AR 2251 02 600 all wool 260g.jpg">
                <img src="images2/fabric/AR 2251 02 600 all wool 260g.jpg"  />
                <div>Dark Grey Stripe</div>
            </div>
            <div id="fabric_4" onclick="textureChange(this.id);" class="content_inner stripe Grey fabric " title="fabric|AR 2251 16 600 all wool 260gr.jpg">
                <img src="images2/fabric/AR 2251 16 600 all wool 260gr.jpg"  />
                <div>Light Grey Stripe</div>
            </div>
            <div id="fabric_5" onclick="textureChange(this.id);" class="content_inner Grey stripe fabric " title="fabric|AR 2251 17 600 all wool 260gr.jpg">
                <img src="images2/fabric/AR 2251 17 600 all wool 260gr.jpg"  />
                <div>Steel Grey Stripe</div>
            </div>
            <div id="fabric_6" onclick="textureChange(this.id);" class="content_inner Blue check fabric " title="fabric|AR 2251 49 600 all wool 260gr.jpg">
                <img src="images2/fabric/AR 2251 49 600 all wool 260gr.jpg"  />
                <div>Blue Check</div>
            </div>
            <div id="fabric_7" onclick="textureChange(this.id);" class="content_inner Grey check fabric " title="fabric|AR 2251 50 600 all wool 260g.jpg">
                <img src="images2/fabric/AR 2251 50 600 all wool 260g.jpg"  />
                <div>Grey Check</div>
            </div>
            <div id="fabric_8" onclick="textureChange(this.id);" class="content_inner Navy check fabric " title="fabric|AR 2251 55 600 all wool 260gr.jpg">
                <img src="images2/fabric/AR 2251 55 600 all wool 260gr.jpg"  />
                <div>Navy Check</div>
            </div>
            <div id="fabric_9" onclick="textureChange(this.id);" class="content_inner Navy check fabric" title="fabric|AR 2251 56 600 all wool 260gr.jpg">
                <img src="images2/fabric/AR 2251 56 600 all wool 260gr.jpg"  />
                <div>Navy Check</div>
            </div>
            <div id="fabric_10" onclick="textureChange(this.id);" class="content_inner Brown check fabric" title="fabric|AR 2251 56 600 all wool 260gr_1.jpg">
                <img src="images2/fabric/AR 2251 56 600 all wool 260gr_1.jpg"  />
                <div>Brown Check</div>
            </div>
            <div id="fabric_11" onclick="textureChange(this.id);" class="content_inner Gray check fabric" title="fabric|AR 2251 58 600 all wool 260gr_1.jpg">
                <img src="images2/fabric/AR 2251 58 600 all wool 260gr_1.jpg"  />
                <div>Gray Check</div>
            </div>
            <div id="fabric_12" onclick="textureChange(this.id);" class="content_inner Brown stripe fabric" title="fabric|AR 2258 74 560 sunny season.jpg">
                <img src="images2/fabric/AR 2258 74 560 sunny season.jpg"  />
                <div>Brown HB</div>
            </div>
            <div id="fabric_13" onclick="textureChange(this.id);" class="content_inner Gray stripe fabric" title="fabric|AR 2258 75 560 sunny season.jpg">
                <img src="images2/fabric/AR 2258 75 560 sunny season.jpg"  />
                <div>Gray HB</div>
            </div>
        </div>
    </div>
    <!--End Of fabric-->
    <!--Start of Trouser Fit -->
    <div id="panel_fit" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Trouser Fit</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content" style="background-color:transparent;">
            <div id="MS-Tro_Fit_RFit_BPanel"  onclick="fitMesh(this.id);" class="content_inner fit active" title="fit|regular.svg">
                <img  class="bottom_img" src="images2/fit/regular.svg"/>
                <div>Regular Fit</div>
            </div>
            <div  id="MS-Tro_Fit_SFit_BPanel"  onclick="fitMesh(this.id);" class="content_inner fit" title="fit|Slim_fit.svg">
                <img  class="bottom_img" src="images2/fit/Slim_fit.svg"/>
                <div>Slim  Fit</div>
            </div>
        </div>
    </div>
    <!--End Of suit Fit-->
    <!-- Start of pocket fabric -->
    <div id="panel_pocket_fabric" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Pocket Fabric</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content" style="background-color:transparent;">
            <div id="000000" onclick="pocColorChange(this.id);" class="content_inner pocket_fabric active" title="pocket_fabric|Pocket-Lining-Black(Default).png">
                <img class="bottom_img" src="images2/pocket_fabric/Pocket-Lining-Black(Default).png"/>
                <div>Black </div>
            </div>
            <!--  image needed -->
            <div id="000526"  onclick="pocColorChange(this.id);" class="content_inner pocket_fabric" title="pocket_fabric|Pocket-Lining-Blue.png">
                <img class="bottom_img" src="images2/pocket_fabric/Pocket-Lining-Blue.png"/>
                <div>Blue</div>
            </div>
            <div id="160c00" onclick="pocColorChange(this.id);" class="content_inner pocket_fabric" title="pocket_fabric|Pocket-Lining-Dark-Brown.png">
                <img class="bottom_img" src="images2/pocket_fabric/Pocket-Lining-Dark-Brown.png"/>
                <div>Dark brown</div>
            </div>
            <div id="1c0000" onclick="pocColorChange(this.id);" class="content_inner pocket_fabric" title="pocket_fabric|Pocket-Lining-Dark-Red.png">
                <img class="bottom_img" src="images2/pocket_fabric/Pocket-Lining-Dark-Red.png"/>
                <div>Dard Red</div>
            </div>
        </div>
    </div>
    <!--End of Pocket fabric -->
    <!-- Start of Regular Side pocket -->
    <div id="panel_side_pocket" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading reg_side" style="background-color:transparent;">
            <div class="title">Regular_Side Pocket</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content reg_side" style="background-color:transparent">
            <div id="MS-Tro_Fit_RFit_FPanel_BevelPoc" onclick="pocChange(this.id);" class="content_inner Side_pocket active" title="Side_pocket|Side-Pocket-Beveled.svg">
                <img class="bottom_img" src="images2/Side_pocket/Side-Pocket-Beveled.svg"/>
                <div>Beveled</div>
            </div>
            <div id="MS-Tro_Fit_RFit_FPanel_StraightPoc" onclick="pocChange(this.id);" class="content_inner Side_pocket" title="Side_pocket|Side-Pocket-Straight.svg">
                <img class="bottom_img" src="images2/Side_pocket/Side-Pocket-Straight.svg"/>
                <div>Straight</div>
            </div>
        </div>
        <div class="heading slim_side" style="background-color:transparent;">
            <div class="title">Slim_Side Pocket</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content slim_side" style="background-color:transparent">
            <div id="MS-Tro_Fit_SFit_FPanel_BevelPoc" onclick="pocChange(this.id);" class="content_inner Side_pocket active" title="Side_pocket|Side-Pocket-Beveled.svg">
                <img class="bottom_img" src="images2/Side_pocket/Side-Pocket-Beveled.svg"/>
                <div>Beveled</div>
            </div>
            <div id="MS-Tro_Fit_SFit_FPanel_StraightPoc" onclick="pocChange(this.id);" class="content_inner Side_pocket" title="Side_pocket|Side-Pocket-Straight.svg">
                <img class="bottom_img" src="images2/Side_pocket/Side-Pocket-Straight.svg"/>
                <div>Straight</div>
            </div>
        </div>
    </div>
    <!--End of regular Side Pocket -->
    <!-- Start Of Buttoning -->
    <div id="panel_buttoning" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Buttoning</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content" style="background-color:transparent;">
            <div id="MS-Tro_B-FixedPart_B" onclick="btnChange(this.id);" class="content_inner Buttoning active" title="Buttoning|Visible-Button.svg">
                <img class="bottom_img" src="images2/Buttoning/Visible-Button.svg"/>
                <div>Visible Button</div>
            </div>
            <div id="MS-Tro_B-FixedPart" onclick="btnChange(this.id);" class="content_inner Buttoning " title="Buttoning|Hidden-Button.svg">
                <img class="bottom_img" src="images2/Buttoning/Hidden-Button.svg"/>
                <div>Hidden Button</div>
            </div>
        </div>
    </div>
    <!-- End Of Buttoning -->
    <!-- Start Of Back Pocket-->
    <div id="panel_back_pocket" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Back Pocket</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content" style="background-color:transparent;">
            <div id="MS-Tro_B-poc_Single_Both" onclick="meshChange(this.id);" class="content_inner Back_pocket active" title="Back_pocket|Back-Pocket-Placement-Both.svg">
                <img class="bottom_img" src="images2/Back_pocket/Back-Pocket-Placement-Both.svg"/>
                <div>Both</div>
            </div>
            <div id="MS-Tro_B-poc_Single" onclick="meshChange(this.id);" class="content_inner Back_pocket" title="Back_pocket|Back-Pocket-Placement-No-Thanks.svg">
                <img class="bottom_img" src="images2/Back_pocket/Back-Pocket-Placement-No-Thanks.svg"/>
                <div>No thanks</div>
            </div>
            <div id="MS-Tro_B-poc_Single_Rt" onclick="meshChange(this.id);" class="content_inner Back_pocket" title="Back_pocket|Back-Pocket-Placement-Right.svg">
                <img class="bottom_img" src="images2/Back_pocket/Back-Pocket-Placement-Right.svg"/>
                <div>Right</div>
            </div>
            <div id="MS-Tro_B-poc_Single_Lt" onclick="meshChange(this.id);" class="content_inner Back_pocket" title="Back_pocket|Back-Pocket-Placement-Left.svg">
                <img class="bottom_img" src="images2/Back_pocket/Back-Pocket-Placement-Left.svg"/>
                <div>Left</div>
            </div>
        </div>
    </div>
    <!-- End Of Back pocket -->
    <!-- Start Of Back Pocket Design-->
    <div id="panel_pocket_design" class="panel mol_panel" style="background-color:transparent">
        <!--Left Pocket-->
        <div class="heading left_design" style="background-color:transparent">
            <div class="title">Left Pocket Design</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content1 left_design" style="background-color:transparent;">
            <div id="MS-Tro_B-poc_Single_Lt" onclick="meshChange(this.id);" class="content_inner Back_Detail" title="Back_Detail|02a_Single_Welt_Pocket.svg">
                <img  src="images2/Back_Detail/02a_Single_Welt_Pocket.svg"/>
                <div>Single Welt Pocket</div>
            </div>
            <div id="MS-Tro_B-poc_Hook_Lt" onclick="meshChange(this.id);" class="content_inner Back_Detail" title="Back_Detail|02c_With_Hook.svg">
                <img  src="images2/Back_Detail/02c_With_Hook.svg"/>
                <div>With Hook</div>
            </div>
            <div id="MS-Tro_B-poc_Double_Lt" onclick="meshChange(this.id);" class="content_inner Back_Detail" title="Back_Detail|02b_Buttoned.svg">
                <img  src="images2/Back_Detail/02b_Buttoned.svg"/>
                <div>Buttoned</div>
            </div>
        </div>
        <!--Right Pocket-->
        <div class="heading right_design" style="background-color:transparent;">
            <div class="title">Right Pocket Design</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content1 right_design" style="background-color:transparent;">
            <div id="MS-Tro_B-poc_Single_Rt" onclick="meshChange(this.id);" class="content_inner Back_Detail" title="Back_Detail|02a_Single_Welt_Pocket.svg">
                <img  src="images2/Back_Detail/02a_Single_Welt_Pocket.svg"/>
                <div>Single Welt Pocket</div>
            </div>
            <div id="MS-Tro_B-poc_Hook_Rt" onclick="meshChange(this.id);" class="content_inner Back_Detail" title="Back_Detail|02c_With_Hook.svg">
                <img  src="images2/Back_Detail/02c_With_Hook.svg"/>
                <div>With Hook</div>
            </div>
            <div id="MS-Tro_B-poc_Double_Rt" onclick="meshChange(this.id);" class="content_inner Back_Detail" title="Back_Detail|02b_Buttoned.svg">
                <img  src="images2/Back_Detail/02b_Buttoned.svg"/>
                <div>Buttoned</div>
            </div>
        </div>
        <!--both Pocket-->
        <div class="heading both_design" style="background-color:transparent;">
            <div class="title">Both Pocket Design</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content1 both_design" style="background-color:transparent;">
            <div id="MS-Tro_B-poc_Single_Both" onclick="meshChange(this.id);" class="content_inner Back_Detail" title="Back_Detail|02a_Single_Welt_Pocket.svg">
                <img  src="images2/Back_Detail/02a_Single_Welt_Pocket.svg"/>
                <div>Single Welt Pocket</div>
            </div>
            <div id="MS-Tro_B-poc_Hook_Both" onclick="meshChange(this.id);" class="content_inner Back_Detail" title="Back_Detail|02c_With_Hook.svg">
                <img  src="images2/Back_Detail/02c_With_Hook.svg"/>
                <div>With Hook</div>
            </div>
            <div id="MS-Tro_B-poc_Double-Btn_Both" onclick="meshChange(this.id);" class="content_inner Back_Detail" title="Back_Detail|02b_Buttoned.svg">
                <img  src="images2/Back_Detail/02b_Buttoned.svg"/>
                <div>Buttoned</div>
            </div>
        </div>
    </div>
    <!-- End Of Back pocket Design -->
    <!--Start OF Thread -->
    <div id="panel_thread" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading">
            <div class="title">Thread</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;background-color:transparent;">
            <div id="ffffff" onclick="threadColor(this.id);" class="content_inner thread active" title="thread|White.png">
                <img class="bottom_img" src="images2/thread/White.png"/>
                <div>White</div>
            </div>
            <div id="000000" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Black.png">
                <img class="bottom_img" src="images2/thread/Black.png"/>
                <div>Black</div>
            </div>
            <div id="633719" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Brown.png">
                <img class="bottom_img" src="images2/thread/Brown.png"/>
                <div>Brown</div>
            </div>
            <div id="727271" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Gray.png">
                <img class="bottom_img" src="images2/thread/Gray.png"/>
                <div>Gray</div>
            </div>
            <div id="e0e0a1" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Ivory.png">
                <img class="bottom_img" src="images2/thread/Ivory.png"/>
                <div>Ivory</div>
            </div>
            <div id="0b1628" onclick="threadColor(this.id);" class="content_inner thread" title="thread|Navy.png">
                <img class="bottom_img" src="images2/thread/Navy.png"/>
                <div>Navy</div>
            </div>
        </div>
    </div>
    <!--End of Thread-->
    <!--Start Of button_color -->
    <div id="panel_button" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Button</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content" style="overflow-y:scroll;background-color:transparent;">
            <div id="f1f4e5" onclick="buttonColor(this.id);" class="content_inner button active" title="button|Sorrento-white.png">
                <img  src="images2/button/Sorrento-white.png"/>
                <div>Sorrento white</div>
            </div>
            <div id="161817" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-black.png">
                <img  src="images2/button/Sorrento-black.png"/>
                <div>Sorrento black</div>
            </div>
            <div id="f27597" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Cerise.png">
                <img  src="images2/button/Langa-Cerise.png"/>
                <div>Langa Cerise</div>
            </div>
            <div id="ece1d5" onclick="buttonColor(this.id);" class="content_inner button" title="button|Tineo-Ivory.png">
                <img  src="images2/button/Tineo-Ivory.png"/>
                <div>Tineo Ivory</div>
            </div>
            <div id="55161e" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Wine.png">
                <img  src="images2/button/Coria-Wine.png"/>
                <div>Coria Wine</div>
            </div>
            <div id="8a5c61" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-maroon.png">
                <img  src="images2/button/Sorrento-maroon.png"/>
                <div>Sorrento maroon</div>
            </div>
            <div id="243a2e" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-military green.png">
                <img  src="images2/button/Sorrento-military green.png"/>
                <div>Sorrento-military green</div>
            </div>
            <div id="72275a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Light Purple.png">
                <img  src="images2/button/Langa-Light Purple.png"/>
                <div>Langa-Light Purple</div>
            </div>
            <div id="707370" onclick="buttonColor(this.id);" class="content_inner button" title="button|Sorrento-light gray.png">
                <img  src="images2/button/Sorrento-light gray.png"/>
                <div>Sorrento-light gray</div>
            </div>
            <div id="4a67af" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Blue.png">
                <img  src="images2/button/Langa-Blue.png" />
                <div>Langa-Blue</div>
            </div>
            <div id="2b564d" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Green.png">
                <img  src="images2/button/Langa-Green.png"/>
                <div>Langa-Green</div>
            </div>
            <div id="21204c" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Dark Blue.png">
                <img  src="images2/button/Coria-Dark Blue.png"/>
                <div>Coria-Dark Blue</div>
            </div>
            <div id="1b1a1b" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Black.png">
                <img  src="images2/button/Coria-Black.png"/>
                <div>Coria-Black</div>
            </div>
            <div id="772326" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Wine1.png">
                <img  src="images2/button/Coria-Wine1.png"/>
                <div>Coria-Wine1</div>
            </div>
            <div id="bf653b" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Orange.png">
                <img  src="images2/button/Langa-Orange.png"/>
                <div>Langa-Orange</div>
            </div>
            <div id="e7dcd1" onclick="buttonColor(this.id);" class="content_inner button" title="button|Girona-Ivory.png">
                <img  src="images2/button/Girona-Ivory.png"/>
                <div>Girona-Ivory</div>
            </div>
            <div id="9ba0a6" onclick="buttonColor(this.id);" class="content_inner button" title="button|Girona-Light Gray.png">
                <img  src="images2/button/Girona-Light Gray.png"/>
                <div>Girona-Light Gray</div>
            </div>
            <div id="3e2a22" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Brown.png">
                <img  src="images2/button/Coria-Brown.png"/>
                <div>Coria-Brown</div>
            </div>
            <div id="06082d" onclick="buttonColor(this.id);" class="content_inner button" title="button|Coria-Dark Blue1.png">
                <img  src="images2/button/Coria-Dark Blue1.png"/>
                <div>Coria-Dark Blue1</div>
            </div>
            <div id="ae89b4" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Dark Purple.png">
                <img  src="images2/button/Langa-Dark Purple.png"/>
                <div>Langa-Dark Purple</div>
            </div>
            <div id="61b53a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Lime.png">
                <img  src="images2/button/Langa-Lime.png"/>
                <div>Langa-Lime</div>
            </div>
            <div id="8d8c8c" onclick="buttonColor(this.id);" class="content_inner button" title="button|Silver-(Push).png">
                <img  src="images2/button/Silver-(Push).png"/>
                <div>Silver-(Push)</div>
            </div>
            <div id="a89860" onclick="buttonColor(this.id);" class="content_inner button" title="button|Gold-(Push).png">
                <img  src="images2/button/Gold-(Push).png"/>
                <div>Gold-(Push)</div>
            </div>
            <div id="61b53a" onclick="buttonColor(this.id);" class="content_inner button" title="button|Langa-Lime.png">
                <img  src="images2/button/Langa-Lime.png"/>
                <div>Langa-Lime</div>
            </div>
            <div id="5b5d62" onclick="buttonColor(this.id);" class="content_inner button" title="button|Black-(Push).png">
                <img  src="images2/button/Black-(Push).png"/>
                <div>Black-(Push)</div>
            </div>
            <div id="b1ae99" onclick="buttonColor(this.id);" class="content_inner button" title="button|Ivory-(Push).png">
                <img src="images2/button/Ivory-(Push).png"/>
                <div>Ivory-(Push)</div>
            </div>
        </div>
    </div>
    <!--End of button-->
    <!-- Start Of Waist band-->
    <div id="panel_waist_band" class="panel mol_panel" style="background-color:transparent;">
        <div class="heading" style="background-color:transparent;">
            <div class="title">Waist Band</div>
            <img class="close" src="images2/close.png" onclick="closePanel()"/>
        </div>
        <div class="content" style="background-color:transparent;">
            <div id="band_0" onclick="bandTexChange(this.id);" class="content_inner Waistband active" title="Waistband|Waistband-Pocket-Lining Dark-Red.png">
                <img src="images2/Waistband/Waistband-Pocket-Lining Dark-Red.png"/>
                <div>Red</div>
            </div>
            <div id="band_1" onclick="bandTexChange(this.id);" class="content_inner Waistband" title="Waistband|Waistband-Black-(Default).png">
                <img src="images2/Waistband/Waistband-Black-(Default).png"/>
                <div>Black</div>
            </div>
            <div id="band_2" onclick="bandTexChange(this.id);" class="content_inner Waistband" title="Waistband|Waistband-Pocket-Lining-Blue.png">
                <img  src="images2/Waistband/Waistband-Pocket-Lining-Blue.png"/>
                <div>Blue</div>
            </div>
            <div id="band_3" onclick="bandTexChange(this.id);" class="content_inner Waistband" title="Waistband|Waistband-Pocket-Lining-Brown.png">
                <img src="images2/Waistband/Waistband-Pocket-Lining-Brown.png"/>
                <div>Brown</div>
            </div>
        </div>
    </div>
    <!-- End Of Waist band -->
    <input type="button" value="Add to Cart" onclick="addToCart()" class="btn cartbtn" />
    </body>
    <script>
        var selectedfit = "regular";
        var selectedfabric = "fabric_0";
        var selectedsidepocketval = "beveled";
        var selectedpocketfabric = "000000";
        var selectedbuttoning = "visible";
        var selectedbackpocket = "both_single_welt";
        var selectedbutton = "f1f4e5";
        var selectedthread = "ffffff";
        var selectedwaistband= "band_0";
    </script>
    <script src="js/libs/uiblock.js"></script>
    <script src="js/libs/TweenMax.js"></script>
    <script src="js/libs/three.min.js"></script>
    <script src="js/libs/OrbitControls.js"></script>
    <script src="js/libs/lzma.js"></script>
    <script src="js/libs/ctm.js"></script>
    <script src="js/libs/CTMLoader.js"></script>
    <script src="js/ms-SuitTrou/m-SuitTrou_JSON.js"></script>
    <script src="js/ms-SuitTrou/m-SuitTrou.js"></script>
    <script src="js/ms-SuitTrou/m-SuitTrou-MeshChange.js"></script>
    <script src="js/ms-SuitTrou/m-SuitTrou_Cam.js"></script>
    <script>
        function addToCart(){
            var user_id = $("#user_id").val();
            if(user_id=="") {
//                $("#myModalLogin").modal("show");
                window.location = "login/index.php";
                return false;
            }
            $(".backloader").show();
            var product_name = "Trouser"+Math.floor(Math.random()*10000);
            var url = "admin/api/orderProcess.php";
            $.post(url,{"type":"addToCart_tr","product_type":"men_trouser","product_name":product_name,"product_price":"450"
                ,"user_id":user_id,"quantity":"1","total_amount":"450","tr_fabric":selectedfabric,"tr_fit":selectedfit,
                "tr_sidepocket":selectedsidepocketval,"tr_pocketfabric":selectedpocketfabric,"tr_buttoning":selectedbuttoning,
                "tr_backpocket":selectedbackpocket,"tr_button_color":selectedbutton,"tr_thread_color":selectedthread,
                "tr_waistband":selectedwaistband},function(data){
                var Status = data.Status;
                if(Status == "Success"){
                    $(".backloader").hide();
                    showCartCount(data.cartCount);
                    window.location="cart.php";
                }
                else
                {
                    $(".backloader").hide();
                    $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                    $(".modal-body").html(data.Message);
                    $("#modal").modal("show");
                }
            }).fail(function(){
                $(".backloader").hide();
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html("Server Error !!! Please Try After Some Time...");
                $("#modal").modal("show");
            });
        }
        function getCartCount(){
            var user_id = $("#user_id").val();
            var url = "admin/api/orderProcess.php";
            $.post(url,{"type":"getCartCount","user_id":user_id},function(data){
                var Status = data.Status;
                if(Status == "Success"){
                    showCartCount(data.count);
                }
            }).fail(function(){
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html("Server Error !!! Please Try After Some Time...");
                $("#modal").modal("show");
            });
        }
        function showCartCount(count){
            $("#cartCount").html(count);
        }
        function opencart(){
            window.location="cart.php";
        }
        getCartCount();
    </script>
    <div id="modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php include("footer1.php"); ?>