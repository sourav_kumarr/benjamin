<?php
    include('header.php');
?>
<link rel="stylesheet" href="css/front.css">
<div id="PageContainer" class="is-moved-by-drawer">
    <main class="main-content" role="main">
        <div class="parallax-container js-transparent-header-fadein js-after-primary-carousel">
            <div id="shopify-section-1482524038727" class="shopify-section carousel">
                <div class="carousel-container" id="carousel-1482524038727" data-ken-burns="true" data-slide-timeout="10000">
                    <div class="carousel-slides ">
                        <div class="carousel-slide " data-index="1">
                            <div class="carousel-slide-image">
                                <link rel="stylesheet" type="text/css" href="slider/css/style.css"/>
                                <div id="wowslider-container1">
                                    <div class="ws_images">
                                        <ul>
                                            <li><img src="images/slide3.jpg" alt="jquery slider" title="" id="wows1_0"/></li>
                                            <li><img src="images/slide4.jpg" alt="" title="" id="wows1_1"/></li>
                                        </ul>
                                    </div>
                                </div>
                                <script type="text/javascript" src="slider/js/wowslider.js"></script>
                                <script type="text/javascript" src="slider/js/script.js"></script>
                            </div>
                            <div class="carousel-slide-text align-center-middle">
                                <div class="carousel-slide-text-inner">
                                    <h1 class="">Inspired Outerwear from the Old Country</h1>
                                    <p class="">Photographs graciously provided by Bogart Menswear, Ryan T Crown &
                                        Simon
                                        Hutchinson.
                                    </p>
                                    <a href="index.php" class='block button-container' tabindex='0'>
                                        <button class="btn btn--large ">Shop the Collection</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="shopify-section-1484338522175" class="shopify-section featured-collections">
                <div id='collectionBlock' class='grid--full'>
                    <a href="contactus.php" tabindex="0">
                        <div class="grid__item large--one-half medium--one-half small--one-whole collection-block__image-wrapper">
                            <div class='collectionBlock__image'
                                 style='background-image: url(images/link1.png)'>
                            </div>
                            <div class='collectionBlock__caption text-wrapper'>
                                <h3>Formalwears</h3>
                            </div>
                        </div>
                    </a>
                    <a href="#" tabindex="0">
                        <div class="grid__item large--one-half medium--one-half small--one-whole collection-block__image-wrapper">
                            <div class='collectionBlock__image' style='background-image: url(images/link2.jpg)'></div>
                            <div class='collectionBlock__caption text-wrapper'>
                                <h3>Suiting</h3>
                            </div>
                        </div>
                    </a>
                    <a href="#" tabindex="0">
                        <div class="grid__item large--one-half medium--one-half small--one-whole collection-block__image-wrapper">
                            <div class='collectionBlock__image' style='background-image: url(images/link3.jpg)'></div>
                            <div class='collectionBlock__caption text-wrapper'>
                                <h3>Outerwear</h3>
                            </div>
                        </div>
                    </a>
                    <a href="#" tabindex="0">
                        <div class="grid__item large--one-half medium--one-half small--one-whole collection-block__image-wrapper">
                            <div class='collectionBlock__image' style='background-image: url(images/link4.jpg)'></div>
                            <div class='collectionBlock__caption text-wrapper'>
                                <h3>Accessories</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <style>
                    .collectionBlock__caption h3, .collectionBlock__caption .h3 {
                        color: #fff
                    }
                    #collectionBlock .grid__item:after {
                        position: absolute;
                        top: 0;
                        left: 0;
                        bottom: 0;
                        content: '';
                        display: block;
                        opacity: 0;
                        background-color: #000
                    }
                    #img_container {
                        position: relative;
                        width: 100%
                    }
                    .bigImage {
                        opacity: 1;
                        display: block;
                        width: 100%;
                        height: 400px;
                        transition: .5s ease;
                        backface-visibility: hidden
                    }
                    .middle {
                        transition: .5s ease;
                        opacity: 0;
                        position: absolute;
                        top: 50%;
                        left: 50%;
                        transform: translate(-50%, -50%);
                        -ms-transform: translate(-50%, -50%)
                    }
                    #img_container:hover .bigImage {
                        opacity: .3
                    }
                    #img_container:hover .middle {
                        opacity: 1
                    }
                    .text {
                        background: rgba(34, 34, 34, 0.9) none repeat scroll 0 0 !important;
                        color: #fff !important;
                        font-size: 14px;
                        padding: 10px 20px
                    }

                </style>
            </div>
            <div id="shopify-section-1484338625351" class="shopify-section featured-blog">
                <div class="grid--full" id='featured-blog'>
                    <div class="grid__item large--one-third medium--one-third small--one-whole">
                        <div class="featured-blog__image-wrapper" id="img_container">
                            <div class='featured-blog__image bigImage' style='background-image: url(images/access.jpg)'></div>
                            <div class="middle">
                                <a href="#" class="text">Show More</a>
                            </div>
                        </div>
                        <div class="featured-blog__caption small--text-center text-wrapper">
                            <p class="meta-info">October 19, 2015</p>
                            <a href="#"><h4 class='featured-blog__title'>Fall Sweaters</h4></a>
                            <span class="excerpt">
                                <p>This year's fall sweaters are heavy on the earth tones. Burnt orange, British racing green and old boys' navy are some of the highlights.</p>
							</span>
                            <span class='blog-info'><a href="#">Read more</a></span>
                        </div>
                    </div>
                    <div class="grid__item large--one-third medium--one-third small--one-whole">
                        <div class="featured-blog__image-wrapper" id="img_container">
                            <div class='featured-blog__image bigImage' style='background-image: url(images/bottomlink2.jpg)'></div>
                            <div class="middle">
                                <a href="#" class="text">Show More</a>
                            </div>
                        </div>
                        <div class="featured-blog__caption small--text-center text-wrapper">
                            <p class="meta-info">October 19, 2015</p>
                            <a href="#"><h4 class='featured-blog__title'>The Fall Lookbook</h4></a>
                            <span class="excerpt">
                                <p>The Fall Lookbook takes selected styles from British and Italian menswear and combines them for a fresh,sophisticated fall look.</p>
						    </span>
                            <span class='blog-info'><a href="#">Read more</a></span>
                        </div>
                    </div>
                    <div class="grid__item large--one-third medium--one-third small--one-whole">
                        <div class="featured-blog__image-wrapper" id="img_container">
                            <div class='featured-blog__image bigImage' style='background-image: url(images/bottomlink3.jpg)'></div>
                            <div class="middle">
                                <a href="#" class="text">Show More</a>
                            </div>
                        </div>
                        <div class="featured-blog__caption small--text-center text-wrapper">
                            <p class="meta-info">July 20, 2015</p>
                            <a href="#"><h4 class='featured-blog__title'>Spotlight on Outerwear</h4></a>
                            <span class="excerpt">
							    <p>We've always admired the rugged, country jacket, purchased decades ago, that still looks classic. We've hunted the best, heritage items from our collection and put them together in this edit.</p>
						    </span>
                            <span class='blog-info'><a href="#">Read more</a></span>
                        </div>
                    </div>
                </div>
                <div class="grid--full" id='featured-blog'>
                    <div class="grid__item large--one-third medium--one-third small--one-whole">
                        <div class="featured-blog__image-wrapper" id="img_container">
                            <div class='featured-blog__image bigImage' style='background-image: url(images/preview3.jpg)'></div>
                            <div class="middle">
                                <a href="#" class="text">Show More</a>
                            </div>
                        </div>
                        <div class="featured-blog__caption small--text-center text-wrapper">
                            <p class="meta-info">October 19, 2015</p>
                            <a href="#"><h4 class='featured-blog__title'>Fall Sweaters</h4></a>
                            <span class="excerpt">
                                <p>This year's fall sweaters are heavy on the earth tones. Burnt orange, British racing green and old boys' navy are some of the highlights.</p>
                            </span>
                            <span class='blog-info'><a href="#">Read more</a></span>
                        </div>
                    </div>
                    <div class="grid__item large--one-third medium--one-third small--one-whole">
                        <div class="featured-blog__image-wrapper" id="img_container">
                            <div class='featured-blog__image bigImage' style='background-image: url(images/peview2.jpg)'></div>
                            <div class="middle">
                                <a href="#" class="text">Show More</a>
                            </div>
                        </div>
                        <div class="featured-blog__caption small--text-center text-wrapper">
                            <p class="meta-info">October 19, 2015</p>
                            <a href="#"><h4 class='featured-blog__title'>The Fall Lookbook</h4></a>
                            <span class="excerpt">
                                <p>The Fall Lookbook takes selected styles from British and Italian menswear and combines them for a fresh,sophisticated fall look.</p>
                            </span>
                            <span class='blog-info'><a href="#">Read more</a></span>
                        </div>
                    </div>
                    <div class="grid__item large--one-third medium--one-third small--one-whole">
                        <div class="featured-blog__image-wrapper" id="img_container">
                            <div class='featured-blog__image bigImage' style='background-image: url(images/prev3.jpg)'></div>
                            <div class="middle">
                                <a href="#" class="text">Show More</a>
                            </div>
                        </div>
                        <div class="featured-blog__caption small--text-center text-wrapper">
                            <p class="meta-info">July 20, 2015</p>
                            <a href="#"><h4 class='featured-blog__title'>Spotlight on Outerwear</h4></a>
                            <span class="excerpt">
                                <p>We've always admired the rugged, country jacket, purchased decades ago, that still looks classic. We've hunted the best, heritage items from our collection and put them together in this edit.</p>
                            </span>
                            <span class='blog-info'><a href="#">Read more</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<?php
    include('footer.php');
?>