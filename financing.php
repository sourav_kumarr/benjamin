<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 6/28/2017
 * Time: 3:43 PM
 */
 include("header1.php");
 ?>
 <link rel="stylesheet" href="css/pricing.css">
 <link rel="stylesheet" href="css/financing.css">
 <div class="container-fluid pricing-back back" style="height: 770px">
  <div class="row no-gutter back2" >
   <div class="col-md-12 ">
    <div class="col-md-3 discount-div">
     <span class="col-md-12 discount-txt">10<label style="font-size: 34px">%</label></span>
     <span class="col-md-12 discount-down">DOWN</span>

    </div>
       <div class="col-md-9 financing-txt financing-txt-margin">
           <ul class=" list-unstyled">
               <li>Qualified clients need only  pay 10% when ordering.</li>
               <li>The remainder can be paid over the course of 18 months.</li>
               <li>This allows our customers to purchase seasonally without charging each order at one time.</li>

           </ul>
       </div>
   </div>
   <div class="col-md-12 financing-txt financing-txt-margin" style="margin-left: 10px">
       <span >0% FINANCING / 10% DOWN</span>
   </div>

      <div class="col-md-12 pricing-txt" style="margin-top: 2%">
          <a href="customization.php"><button type="button" class="custom-btn">GO CUSTOM</button></a>
      </div>
 </div>
 </div>
<?php
 include ("footer1.php");
?>