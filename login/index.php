<?php
session_start();
if(isset($_SESSION['benj_user_id'])){
    header("Location:../index.php");
}
?>
<link rel="stylesheet" href="css/login.css">
<link href="../css/bootstrap.min1.css" rel="stylesheet">
<link rel="stylesheet" href="../css/daterangepicker.css"/>
<link rel="stylesheet" href="../css/font-awesome.css"/>
<link rel="stylesheet" href="../css/header.css"/>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/moment.js" type="text/javascript"></script>
<script src="../js/daterangepicker.js" type="text/javascript"></script>
<script src="../js/location.js" type="text/javascript"></script>
<script src="../js/jquery.flip.js"></script>
<style>
    select {
        -moz-appearance: none;
        -webkit-appearance: none;
    }
    .loadimage{
        display: none;
        height: 31px;
        left: 140px;
        position: absolute;
        top: 530px;
    }

</style>
<body class="main">
<!--<div class="login-screen"></div>-->
<!--<div class="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="collapse navbar-collapse" id="navigation">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="../index.php">Home</a></li>
                    <?php
/*                    if(isset($_SESSION['benj_user_id'])) {
                        */?>
                        <li class="has-dropdown">
                            <a href="../customization.php" data-navigation-top-level>Order Now</a>
                        </li>
                        <?php
/*                    }else{
                        */?>
                        <li class="has-dropdown">
                            <a href="index.php" data-navigation-top-level>Order Now</a>
                        </li>
                        <?php
/*                    }
                    */?>
                    <li><a href="../contactus.php">Contact us</a></li>
                    <li><a href="../aboutus.php">About us</a></li>
                    <li><a href="../login/">Login/Register</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>-->
<div class="row  main-header " style="margin: 0px;border-bottom: 1px grey solid;padding-bottom: 2%">
    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12" >
        <span class="header-txt"><i class="fa fa-2x fa-instagram"></i></span>
        <span class="header-txt"><i class="fa fa-2x fa-google-plus"></i></span>
        <span class="header-txt"><i class="fa fa-2x fa-facebook"></i></span>

    </div>
    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 header-child" style="margin-top: auto;">
        <h1 class="site-header__logo shop-name">
            <a href="index.php" class='site-header__link'>
                <img src="../images/logo/logo.png" class="img-responsive" style="border: 1px white solid" />
            </a>
        </h1>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 header-child right-header">
        <ul class="list-unstyled">
            <li class="header-img"><i class="fa fa-2x fa-mobile" style="font-size: 27px"></i></li>
            <li class="header-txt header-right-size" ><span>212.235.2354</span></li>
            <li class="header-img"><i class="fa fa-2x fa-envelope" style="font-size: 17px"></i></li>
            <li class="header-txt header-right-size" ><span>info@benjaminscustom.com</span></li>
        </ul>
    </div>

</div>

<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 no-gutter"  style="margin: 0px;background-color: #252525;">
    <div class="col-md-10 col-sm-12 col-xs-12 col-lg-10 no-gutter " id="main-header" >
        <!--       <ul class="list-unstyled ">-->

        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 no-gutter"  style="border-bottom:1px grey solid;border-right:1px grey solid;">
            <a class="href-txt" href="../index.php"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter" style="background-color:#000 ">ABOUT</div></a>
            <a class="href-txt" href="../customsuit.php"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter">CUSTOM SUITS</div></a>
            <a href="../tuxedo.php" class="href-txt"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter">CUSTOM TUXEDOS</div></a>
            <a href="../customshirts.php" class="href-txt"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter">CUSTOM SHIRTS</div></a>
            <a href="../pricing.php" class="href-txt"><div class="header-txt main-header1 col-lg-2 col-md-3 col-sm-3 col-xs-12 no-gutter">PRICING</div></a>
            <a href="../financing.php" class="href-txt"><div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">0% FINANCING</div></a>
            <a href="../scanning.php" class="href-txt"><div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">3D BODY SCANNING</div></a>
            <!--             <div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">LUXURY FABRICS</div>-->
            <a href="../process.php" class="href-txt"><div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">THE PROCESS</div></a>
            <!--             <div class="header-txt main-header1 col-md-3 col-sm-3 col-xs-12 col-lg-2 no-gutter">BESPOKE</div>-->
        </div>

        <!--       </ul>-->
    </div>
    <div class="col-md-2 col-sm-12 col-xs-12 col-lg-2 no-gutter book-appointment" onclick="onAppointmentLoad()">
        <span class="col-md-12 col-sm-12 col-xs-12 book-appointment-txt" >Book Appointment</span>
        <?php
        if(isset($_SESSION['benj_user_id'])) {
          ?>
            <span class="col-md-12 col-sm-12 col-xs-12 guest-txt" ><?php echo $_SESSION['benj_user_email'];?></span>
            <?php
         }
         else{
             ?>
             <span class="col-md-12 col-sm-12 col-xs-12 guest-txt" >hello ! guest</span>
             <?php
         }
        ?>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 img-responsive back ">
<!--    <div class="container " style="margin-top: 10px;">-->
<!--        <div class="row">-->
            <div class="col-xs-4 col-md-offset-3" style="margin-top: 10px">
                <div class="login" id="card">
                    <div class="front signin_form">
                        <p>Login Your Account</p>
                        <form class="login-form">
                            <p class="error" style="color:red;text-align:center"></p><br>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="email" id="logemail" class="form-control" placeholder="Type your E-Mail">
                                    <span class="input-group-addon">
                                          <i class="fa fa-user"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" id="logpassword" class="form-control" placeholder="Type your Password">
                                    <span class="input-group-addon">
                                          <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                            </div>
                            <label onclick="forgetpasspop()">Forgot Password ?</label>
                            <div class="form-group sign-btn" style="margin-top:10px">
                                <input type="button" id="loginbtn" class="btn" value="Log in">
                                <p>
                                    <br><strong>New to Benjamin? </strong>
                                    <a href="#" id="flip-btn" class="signup signup_link">Sign up Here...</a>
                                </p>
                            </div>
                        </form>
                    </div>
                    <div class="back signup_form" style="opacity: 0;">
                        <p>Sign Up for Your New Account</p>
                        <form class="login-form">
                            <p class="error" style="color:red;text-align:center"></p><br>
                            <div class="first-rows">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" id="fname" class="form-control" placeholder="First Name">
                                    <span class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" id="lname" class="form-control" placeholder="Last Name">
                                    <span class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <select class="form-control countries" id="countryId">
                                        <option value="">Please Wait...</option>
                                    </select>
                                    <span class="input-group-addon">
                                      <i class="fa fa-globe"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <select class="states form-control" id="stateId">
                                        <option value="">Select Country First</option>
                                    </select>
                                    <span class="input-group-addon">
                                      <i class="fa fa-globe"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div id="cityfeild">
                                        <select class="cities form-control" id="cityId">
                                            <option value="">Select State First</option>
                                        </select>
                                    </div>
                                    <span class="input-group-addon">
                                      <i class="fa fa-globe"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="inputdob" placeholder="DOB">
                                    <span class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                  </span>
                                </div>
                            </div>
                        </div>
                        <div class="secondr-rows">
                            <div class="form-group">
                                <div class="col-md-5" style="padding-left:0">
                                    <select id="heightparam" onchange="heightp()" class="form-control" style="background:transparent;height:44px">
                                        <option value="" selected="selected">Parameter</option>
                                        <option value="Pounds">Inches</option>
                                        <option value="Centimeter">Centimeter</option>
                                        <option value="Feets">Feets</option>
                                    </select>
                                </div>
                                <div class="col-md-7 input-group" id="dynaheight">
                                    <input type="text" class="form-control" id="height" placeholder="Height">
                                    <span class="input-group-addon">
                                          <i class="fa fa-user"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="file" class="form-control" id="file0">
                                    <span class="input-group-addon">
                                      <i class="fa fa-image"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="stylist" placeholder="Stylist">
                                    <span class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <select class="form-control" id="gender">
                                        <option selected="selected" value="">Select Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                    <span class="input-group-addon">
                                      <i class="fa fa-mars-double"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <select class="form-control" id="language">
                                        <option selected="selected" value="">Select Language</option>
                                        <option value="English (UK)">English (UK)</option>
                                        <option value="English (US)">English (US)</option>
                                        <option value="French">French</option>
                                    </select>
                                    <span class="input-group-addon">
                                      <i class="fa fa-globe"></i>
                                  </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5" style="padding-left:0">
                                    <select id="weightparam" class="form-control" style="background:transparent;height:44px">
                                        <option value="" selected="selected">Parameter</option>
                                        <option value="Pounds">Pounds</option>
                                        <option value="Kilograms">Kilograms</option>
                                        <option value="Stones">Stones</option>
                                    </select>
                                </div>
                                <div class="col-md-7 input-group">
                                    <input type="text" class="form-control" id="weight" placeholder="Weight">
                                    <span class="input-group-addon">
                                          <i class="fa fa-user"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="email" id="email" class="form-control" placeholder="Email Address">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="password" id="password" class="form-control" placeholder="Enter Password">
                                <span class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </div>
                        </div>
                        <input type="hidden" id="type" value="Register" />
                        <div class="form-group sign-btn">
                            <input type="button" onclick="register()" class="btn" value="Sign up">
                            <br><br>
                            <p>You have already Account So <a href="#" id="unflip-btn" class="signup">Log in</a></p>
                        </div>
                        <img src="../images/default.gif" class="loadimage" />
                        </form>
                    </div>
                </div>
            </div>
<!--        </div>-->
<!--    </div>-->
</div>
<div id="modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    function heightp() {
        var param = $("#heightparam").val();
        if(param == "Feets") {
            $("#dynaheight").html("<div class='row'><div class='col-md-12' style='margin-left:14px;padding-left:0'>" +
                "<input id='height0' class='form-control' type='text' placeholder='Feets' value='' style='float:left;" +
                "width:65px' /><input type='text' id='height1' class='form-control' style='border-left:1px solid #fff;" +
                "float:left;width:78px' placeholder='Inches' value='' /></div></div>");
        }
        else
        {
            $("#dynaheight").html("<input type='text' name='height' id='height' class='form-control' placeholder='Enter Height'>");
        }
    }
    function forgetpasspop(){
        $(".modal-title").html("<h4 style='color:red'>Enter E-Mail Address</h4>");
        $(".modal-body").css("height","115px")
        $(".modal-body").html("<p id='error' style='color:red;text-align: center'></p><div class='col-md-8'>" +
        "<div class='form-group'><label>Enter Email</label><input type='text' class='form-control' id='fp_email' " +
        "value='' placeholder='Enter Registered E-Mail Address'/></div></div>");
        $(".modal-footer").html("<input type='button' value='Submit' onclick='forgetpass()' " +
        " class='btn btn-danger pull-right' />")
        $("#modal").modal("show");
    }
    function forgetpass()
    {
        var email = $("#fp_email").val();
        if(email != "")
        {
            if(validateEmail(email)) {
                var url = "../admin/api/registerUser.php";
                $.post(url,{"type":"forgotPassword","email":email},function (data) {
                    var Status = data.Status;
                    if (Status == "Success") {
                        $(".modal-title").html("<h4 style='color:green'>Forgot Password</h4>");
                        $(".modal-body").html("<label>Reset Password Link has been sent to your Registered E-Mail Address</label>");
                        setTimeout(function () {
                            $("#modal").modal("hide");
                        }, 1000);
                    }
                    else if (Status == "Failure") {
                        $("#error").html("Entered E-Mail is not a Registered E-Mail Address");
                        return false;
                    } else {
                        $("#error").html("Server Error !!! Please Try After Some Time");
                        return false;
                    }
                }).fail(function () {
                    $("#error").html("Server Error !!! Please Try After Some Time");
                    return false;
                });
            }else{
                $("#error").html("Please Enter Valid E-Mail Address");
                return false;
            }
        }
        else{
            $("#error").html("Please Enter Your Registered E-Mail Address");
            return false;
        }
    }
    $().ready(function () {
        $("#card").flip({
            trigger: 'manual'
        });
    });
    $(".signup_link").click(function () {

        $(".signin_form").css('opacity', '0');
        $(".signup_form").css('opacity', '100');

         $(".back").css("height","730px");
        $("#card").flip(true);

        return false;
    });

    $("#unflip-btn").click(function () {

        $(".signin_form").css('opacity', '100');
        $(".signup_form").css('opacity', '0');
        $(".back").css("height","600px");
        $("#card").flip(false);

        return false;

    });
    $(document).ready(function () {
        $('#inputdob').daterangepicker({
            singleDatePicker: true,
            inputdob: moment().subtract(6, 'days')
        });
    });
    $("#loginbtn").click(function(){
        var username = $("#logemail").val();
        var password = $("#logpassword").val();
        if(username == "" || password == ""){
            $(".error").html("All Feilds are Required Filled");
            return false;
        }
        if(validateEmail(username)){
            var url = "../admin/api/registerUser.php";
            $.post(url, {"type": "Login","email":username,"password":password}, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    window.location="../index.php";
                }
                else {
                    $(".error").html(data.Message);
                }
            }).fail(function(){
                $(".error").html("Server Error !!! Please Try After Some Time....");
            });
        }
        else{
            $(".error").html("Invalid E-Mail Address Entered");
            return false;
        }
    });
    function register(){
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var countryId = $("#countryId").val();
        var stateId = $("#stateId").val();
        var cityId = $("#cityId").val();
        var dob = $("#inputdob").val();
        var heightparam = $("#heightparam").val();
        var stylist = $("#stylist").val();
        var gender = $("#gender").val();
        var language = $("#language").val();
        var weightparam = $("#weightparam").val();
        var email = $("#email").val();
        var password = $("#password").val();
        if(fname == "" || lname == "" || countryId == "" || stateId == "" || cityId == "" || dob == "" ||
        heightparam == "" || email == "" || password == "" || gender == "" || dob == "" ||  weightparam == "" || weight == ""){
            $(".error").html("All Feilds Should Required Filled");
            return false;
        }
        if(!(validateEmail(email))){
            $(".error").html("Invalid Email Address Used");
            return false;
        }
        //default height in inches and weight in pounds
        ////calculations for height
        var height;
        var weight;
        if(heightparam == "Feets"){
            if($("#height0").val() == "" || $("#height1").val() == "") {
                $(".error").html("Height Should not be Blank");
                return false;
            }
            height = parseInt($("#height0").val() * 12) + parseInt($("#height1").val());
        }
        else if(heightparam == "Centimeters"){
            if(!($("#height").val() == "")){
                height = parseInt($("#height").val()*0.393701);
            }
            else{
                $(".error").html("Height Should not be Blank");
                return false;
            }
        }
        else{
            if($("#height").val() == "") {
                $(".error").html("Height Should not be Blank");
                return false;
            }
            height = $("#height").val();
        }
        ////calculations for weight
        if(weightparam == "Kilograms"){
            weight = Math.round($("#weight").val()*2.20462);
        }
        else if(weightparam == "Stones"){
            weight = Math.round($("#weight").val()*14);
        }
        else{
            weight = Math.round($("#weight").val());
        }
        var data = new FormData();
        if($("#file0").val() != ""){
            var user_profile = $("#file0").val();
            var file_ext = user_profile.split(".");
            file_ext = file_ext[file_ext.length - 1];
            if (file_ext == "jpeg" || file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                var _file = document.getElementById('file0');
                data.append('user_profile', _file.files[0]);
            }
            else {
                $(".error").html("File Should Be JPG,PNG,JPEG,GIF Formats Only");
                return false;
            }
        }
        data.append("type", "Register");
        data.append("fname",fname);
        data.append("lname",lname);
        data.append("countryId",countryId);
        data.append("stateId",stateId);
        data.append("cityId",cityId);
        data.append("dob",dob);
        data.append("height",height);
        data.append("stylist",stylist);
        data.append("gender",gender);
        data.append("language",language);
        data.append("weight",weight);
        data.append("email",email);
        data.append("password",password);
        $(".loadimage").show();
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4) {
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if (status == "Success") {
                    location.reload(true);
                }
                else {
                    $(".error").html(response.Message);
                    $(".loadimage").hide();
                }
            }
        };
        request.open('POST', '../admin/api/registerUser.php');
        request.send(data);
    }
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>
<?php
 include ("../footer1.php");
?>
</body>