<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 6/26/2017
 * Time: 1:33 PM
 */
include('header1.php');
?>
<style>
    .badge {
    background: red none repeat scroll 0 0;
    margin-left: -15px;
    margin-top: -27px;
    }
    </style>
    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 img-responsive back">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 cover">

            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 cover-body">
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="text-align: center"><h3 class="cover-title">A BETTER APPROACH TO CUSTOM TAILORING</h3></div>
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" >
                <div class="col-md-4 index-div">

                </div>
                <div class="col-md-8 index-txt">
                    <ul class=" list-unstyled">
                        <li>OUR FEATURED MIDNIGHT LORO PIANA TUXEDO | $995</li>
                        <li>Benjamins Custom represents a unique fusion of tailor class craftsmanship, world class fabrics and unprecedented scientific precision.</li>
                        <li>Our signature fabrics are Loro Piana and Zegna.</li>
                        <li>Each suit is fully canvassed and hand finished in house here in our Manhattan studio to your exact specifications.</li>
                        <li>Our process uses custom 3D body scanning to ensure scientific precision throughout the fit process.</li>
                        <li>Qualified clients can order with only 10% down and 10 months to pay.</li>
                        <li>Fully Custom Suiting from $595-$995</li>
<!--                        <li><a href="mentrouser.php"><button type="button" class="custom-btn" style="padding: 3%;width: auto">CUSTOM MY TUXEDO</button></a></li>-->
                    </ul>
                </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4" style="color: white;text-align: center;margin-top: 2%">
                    <h4>ABOUT BENJAMINS CUSTOM</h4>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="text-align: center;margin-top: 4%">
                    <a href="customization.php" class="href-txt"><button type="button" class="custom-btn" style="padding: 1%;width: auto">BOOK A FITTING</button></a>
                </div>
                <!--<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="text-align: center"><h3 class="cover-desc">3D PRECISION BODY SCANNING</h3></div>
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="text-align: center"><span class="cover-desc cover-desc0">Our 3D Body Scanner takes 2,844 measurements in 1 second and does so more precisely than human hands can.
 In other words, your suit will fit perfectly.  </span></div>
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="text-align: center"><h3 class="cover-desc">LUXURY STARTING AT $595</h3></div>
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="text-align: center">
                    <span class="cover-desc cover-desc0">All of our suits are fully canvassed and all of our fabrics are top tier., making our suits the best value on the market. </span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="text-align: center"><h3 class="cover-desc">0% FINANCING / 10% DOWN</h3></div>
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="text-align: center">
                    <span class="cover-desc cover-desc0">When ordering, you only need to pay 10%.
This allows our customers to purchase seasonally without significant cash outlay at one time. </span>
                </div>-->
            </div>
        </div>
    </div>
            <script src="js/jquery-3.1.1.min.js" type="application/javascript"></script>

    <script>
        function opencart(){
            window.location="cart.php";
        }

        function getCartCount(){
            var user_id = $("#user_id").val();
            var url = "admin/api/orderProcess.php";
            $.post(url,{"type":"getCartCount","user_id":user_id},function(data){
                var Status = data.Status;
                if(Status == "Success"){
                    showCartCount(data.count);
                }else{
                    // alert("cd"+data.Message);
                }
            }).fail(function(){
                alert("error occured on url");
            });
        }

        function showCartCount(count){
            $("#cartCount").html(count);
        }
        getCartCount();
    </script>
<?php
include('footer1.php');

?>