<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 6/29/2017
 * Time: 6:54 PM
 */
include("header1.php");
?>
    <link rel="stylesheet" href="css/pricing.css">
    <link rel="stylesheet" href="css/financing.css">
    <style>
        .back{
            background-image: url("images/slide2_blur.png") ;
            background-repeat: no-repeat;
            width: 100%;
            height: 1070px;
            background-size:cover ;
            padding: 5%;
        }
    </style>
    <div class="container-fluid pricing-back">
        <div class="row no-gutter custom-div" >
            <div class="col-md-12 process-txt">
                <ul class="list-unstyled">
                    <li><img src="images/customshirts.jpg"></li>
                    <li style="text-align: center;margin-bottom: 10%">CUSTOM SHIRTING</li>
                    <li ><span class="shirt-li" >Our custom shirt offering combines the best craftsmanship with world class fabrics and scientific precision.</span></li>
                    <li ><span class="shirt-li">Each shirt pattern is individually drafted for each client to shape the torso seamlessly and provide ease of movement.</span></li>
                    <li ><span class="shirt-li">Each shirt comes with a complementary custom monogram. </span></li>
                    <li ><span class="shirt-li">Turnaround time is 3-5 weeks </span></li>
                    <li ><span class="shirt-li">Starting at $129</span></li>
                </ul>
            </div>

            <div class="col-md-12 pricing-txt" style="margin-top: 4%">
                <a href="menshirt.php"> <button type="button" class="custom-btn" style="width: auto;padding: 3%">ORDER CUSTOM SHIRTS</button></a>
            </div>
        </div>
    </div>

<?php
 include("footer1.php");
?>