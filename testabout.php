<?php
include("header.php");
?>
<style>
.row_caption {
    background: rgba(0, 0, 0, 0) url("../images/section-title-bg.png") repeat scroll 0 0;
    color: #d32f2f;
    font-family: "Oswald",sans-serif;
    font-size: 40px;
    font-weight: bold;
    line-height: 40px;
    margin: 40px 67px;
    text-align: center;
}
.about4 {
    color: #777;
    font-size: 14px;
    margin-left: 2px;
}
.imgshow {
    border-radius: 10px;
    box-shadow: 0 0 10px;
    margin-top: 106px;
    width: 100%;
}
.thumbaboutus {
    border: 1px solid #ccc;
    box-shadow: 0 0 10px;
    float: left;
    height: 212px;
    margin-left: 23px;
    width: 218px;
}
.span2 {
    font-size: 13px;
    font-weight: bold;
    margin-left: 4px;
    margin-top: 169px;
    position: absolute;
    width: 198px;
}
.aboutlast {
    background: #eee none repeat scroll 0 0;
    box-shadow: 0 0 2px;
    float: left;
    margin-left: 67px;
    width: 26.5%;
}
</style>
    <p class="row_caption">About FinelyFit</p>
    <div class="col-md-12">
        <div class="col-md-7">
            <h1>New Era of FITNESS is Here</h1>
            <hr>
            <h4>Your personal trainer at your finger tips See the real progress by using your scanned measurements!</h4>
            <span class="about4"> Fitness is here.TC2 Labs created FinelyFit as a way to provide people with a quick,</span>
            <br>
            <span class="about4"> easy and customized workout tool for them to exercise ANYWHERE </span>
            <br>
            <span class="about4">(it is like having personal trainer at your fingertips)</span>
            <br>
            <span class="about4">and is perfect for working off those extra weight.</span>
            <br>
            <br>
            <span class="about4">FinelyFit is FREE and is available for download on iOS and Android.</span>
            <br>
            <br>
            <span class="about4">Here are a few quick facts that might help:</span>
            <br>
            <br>
            <li class="about4">FinelyFit is like having a personal trainer at your fingertips.</li>
            <li class="about4">You can customize the workouts as you go along based on what YOU like to do.</li>
            <li class="about4">The followup of the result is amazing.Based on 3D body scans you see your progress is body volume, BMI and lot of body measurements.</li>
            <li class="about4">You will simply LOVE this and its FREE </li>
            <li class="about4">Perfect to have to work off those added Calories! </li>
        </div>
        <div class="col-md-5">
            <img class="imgshow" src="fitness/workout/Images/Double Leg Drop Cable Pullover - Female3.jpg">
        </div>
    </div>
    <div class="col-md-12">
        <hr>
    </div>
    <div class="col-md-12">
        <p class="row_caption">State of the Art Features</p>
        <div class="thumbaboutus" style="background:url(images/about1.jpg) no-repeat scroll 0 0 / 216px 163px;margin-left: 68px">
            <span class="about4 span2"> Personalized, Accurate Avatar</span>
        </div>
        <div class="thumbaboutus" style="background:url(images/about2.png) no-repeat scroll 0 0 / 216px 163px;">
            <span class="about4 span2"> 3D Virtual Try-on and Fitting</span>
        </div>
        <div class="thumbaboutus" style="background:url(images/about3.jpg) no-repeat scroll 0 0 / 216px 163px;">
            <span class="about4 span2"> BMI followup</span>
        </div>
        <div class="thumbaboutus" style="background:url(images/about4.jpg) no-repeat scroll 0 0 / 216px 163px;">
            <span class="about4 span2"> Body volume</span>
        </div>
        <div class="thumbaboutus" style="background: rgba(0, 0, 0, 0) url(images/about5.png) no-repeat scroll 0 -156px / 216px 318px;">
            <span class="about4 span2"> App works for iPhone, Android and though the internet</span>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
    </div>
    <div class="col-md-12">
        <p class="row_caption">FinelyFiT (The Full FITNESS Solution)</p>
        <h3 style="margin:29px 65px">Benefits for Users:</h3>
        <div class="">
            <div class="aboutlast">
                <li class="about4">Your personal trainer at your fingertips</li>
                <li class="about4">Great excerises</li>
            </div>
            <div class="aboutlast">
                <li class="about4">Work plan</li>
                <li class="about4">Clear follow up of your goals</li>
            </div>
            <div class="aboutlast">
                <li class="about4">See in 3D the progress of your body change</li>
                <li class="about4">No equipment required</li>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <br>
        <br>
        <br>
    </div>
    </div>
    <div style="clear:both"></div>
<?php
include("footer.php");
?>